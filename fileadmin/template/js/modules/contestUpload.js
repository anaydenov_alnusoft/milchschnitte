/**
 * contestUpload Plugin
 * Milschnitte | My Break Photo Contest | Upload Widget
 *
 * Copyright brandung GmbH & Co.KG
 * http://www.brandung.de/
 *
 * Date: 22.01.2016
 * MIT License (MIT)
 *
 * Author: Felix Leupold (xiel.de)
 */

;(function ( $, window, document, undefined ) {
	"use strict";

	var pluginName = 'contestUpload';

	//option defaults
	var defaults = {
		acceptDropAnywhere: true,
		selectors: {
			//form
			submitForm: '> form',
			fileSelectInput: '.dropzone__fileinput-wrapper input[type="file"]',

			//dropzone
			dropzoneElement: '.dropzone'
		},
		classes: {
			dropzoneActive: 'dropzone--dragover',
			dropzoneUploading: 'dropzone--uploading',
			dropzonePending: 'dropzone--pending',
			featureDetectionFailed: 'contest-upload--no-fileapi'
		}
	};

	//plugin constructor
	function Plugin( element, options ) {
		this.element = element;
		this.$el = $(this.element);

		//get options from html 
		var optionsFromHTML = this.$el.data();

		//extend defaults with given options and optionsFromHTML
		this.options = $.extend( true, {}, defaults, options, optionsFromHTML ) ;
		
		this._defaults = defaults;
		this._name = pluginName;
		
		this.init();
	}
	
	$.extend(Plugin.prototype, {
		// initialization logic
		init: function () {
			var o = this.options;
			var self = this;
			var $el = this.$el;

			//feature detection
			this.featureDetection = {
				fileApiDragAndDropSupport: window.File && window.FileList && window.FileReader && 'ondragover' in window,
				xhrAdvancedFeatures: window.XMLHttpRequest && window.FormData && 'upload' in XMLHttpRequest.prototype
			};

			if(!( this.featureDetection.fileApiDragAndDropSupport && this.featureDetection.xhrAdvancedFeatures )){
				console.log('contestUpload | modern APIs un-supported', this.featureDetection);
				$el.addClass(o.classes.featureDetectionFailed);
				// return
			} else {
				console.log('contestUpload | all set :) modern APIs are supported', this.featureDetection);
			}

			//instance properties
			this.removeDragActiveClassTimeout = null;

			//main DOM elements
			this.submitForm = $(o.selectors.submitForm, $el).first();
			this.dropzone = $(o.selectors.dropzoneElement, $el);
			this.validDropArea = $(o.acceptDropAnywhere ? 'body' : this.dropzone);
			this.fileSelectInput = $(o.selectors.fileSelectInput, $el).first();

			//inital calls
			this.bindEvents();
		},

		//bind main widget events
		bindEvents: function(){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			//user uses label / file input to select a file
			this.fileSelectInput.on('change', $.proxy(this.handleFileSelect, this) );

			//user drags file into window / dropzone
			this.validDropArea.on('dragover dragleave', $.proxy(this.handleDragHover, this) );
			this.validDropArea.on('drop', $.proxy(this.handleDrop, this));
		},

		//handle dragging (add / remove classes to highlight dropzone)
		handleDragHover: function(e){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			if(e.type === 'dragover'){
				clearTimeout(this.removeActiveClassTimeout);
				self.dropzone.addClass(o.classes.dropzoneActive);
			} else {
				this.removeActiveClassTimeout = setTimeout(function(){
					self.dropzone.removeClass(o.classes.dropzoneActive);
				}, 50);
			}

			e.stopPropagation();
			e.preventDefault();
		},

		//handle drop of files in dropzone
		handleDrop: function(e){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			//handle the drag classes
			this.handleDragHover(e);

			//begin handling the files dropped
			this.handleFileSelect(e);
		},

		//handle files from file input change and dropped files
		handleFileSelect: function(e){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			//if modern file api is not supported, just submit the form
			if(!this.featureDetection.fileApiDragAndDropSupport){
				this.dropzone.addClass(o.classes.dropzoneUploading);
				this.submitForm.submit();
				return;
			}

			var originalEvent = e.originalEvent || e;
			var files = originalEvent.target.files || originalEvent.dataTransfer.files;

			//only accept one/first file
			var fileToSend = files[0];

			if(fileToSend){
				this.uploadFile(fileToSend, this.fileSelectInput.is(originalEvent.target));
			} 
		},

		uploadFile: function(file, alreadyInFileInput){
			var o = this.options;
			var self = this;
			var $el = this.$el;


			//serialize all inputs to the form data (with name of the file input)
			var fd = new FormData(this.submitForm[0]);

			console && console.log('uploadFile via ajax…', this, alreadyInFileInput);

			//add file from drag and drop etc. to the form data (with name of the file input)
			//don't add, if its already in the native file input
			if(!alreadyInFileInput){
				fd.append(this.fileSelectInput.attr('name') || 'file', file);
			}

			this.dropzone.removeClass(o.classes.dropzonePending);
			this.dropzone.addClass(o.classes.dropzoneUploading);

			//send as multipart/form-data via ajax
			$.ajax({
				url: this.submitForm.data('action') || this.submitForm.prop('action'),
				type: this.submitForm.prop('method') || 'POST',
				progress: function(e){
				},
				progressUpload: function(e){
					if (e.lengthComputable) {
						var percentComplete = e.loaded / e.total;
						$('.dropzone__text--uploading h3').text( Math.round(percentComplete * 100) + '%')
						
						if (percentComplete === 1) {
							self.dropzone.removeClass(o.classes.dropzoneUploading);
							self.dropzone.addClass(o.classes.dropzonePending);
						}
					}
				},
				//tell jQuery not to process the data
				data: fd,
				processData: false,
				contentType: false
			}).done(function(data){

				//show that processing is pending, remove loading class
				self.dropzone.addClass(o.classes.dropzonePending);
				self.dropzone.removeClass(o.classes.dropzoneUploading);

				if(typeof data === 'string'){
					console.error('response was a string :) expected json');
				} else {
					console.log('got json', data);

					if(data && data.imageProcessingSuccessfull && data.redirectUrl) {
						location.href = data.redirectUrl;
					} else {
						//TODO: handle fail processings...
						console.error('processing failed', data);
						data.errorMessage && alert(data.errorMessage);
						location.reload();
						self.dropzone.removeClass(o.classes.dropzonePending);
					}
				}

			}).fail( function() {
				console.error( arguments );
				self.dropzone.removeClass(o.classes.dropzoneUploading);
				self.dropzone.removeClass(o.classes.dropzonePending);
			});
		}
	});
	
	//enhance ajax with XHR2 events
	$.ajaxSetup({
		progress: $.noop,
		progressUpload: $.noop,
		xhr: function () {
			var xhr = new window.XMLHttpRequest();
			var that = this;
			xhr.addEventListener("progress", this.progress, false);
			xhr.upload.addEventListener("progress", this.progressUpload, false);
			return xhr;
		}
	});

	// lightweight plugin wrapper around the constructor, 
	// preventing against multiple instantiations
	$.fn[pluginName] = function ( options ) {
		return this.each(function () {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, 
				new Plugin( this, options ));
			}
		});
	}

})( jQuery, window, document );