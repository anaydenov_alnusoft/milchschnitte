/**
 * narrowSelect Plugin
 * Milschnitte | Select Box for navigation on mobile
 *
 * Copyright brandung GmbH & Co.KG
 * http://www.brandung.de/
 *
 * Date: 05.02.2016
 * MIT License (MIT)
 *
 * Author: Felix Leupold (xiel.de)
 */

;(function ( $, window, document, undefined ) {
	"use strict";

	var pluginName = 'narrowSelect';

	//option defaults
	var defaults = {
		navigationLabel: 'Navigation',
		selectors: {
			linkGroup: '> ul',
			optionsAsLinks: 'li a',
			activeOptionLink: 'li.active a'
		},
		classes: {
			labelElement: 'narrowselect__label btn-shadow-wrapper',
			selectElement: 'narrowselect__select',
			openFailed: 'tabs--narrow-select-failed'
		}
	};

	//plugin constructor
	function Plugin( element, options ) {
		this.element = element;
		this.$el = $(this.element);

		//get options from html 
		var optionsFromHTML = this.$el.data();

		//extend defaults with given options and optionsFromHTML
		this.options = $.extend( true, {}, defaults, options, optionsFromHTML ) ;
		
		this._defaults = defaults;
		this._name = pluginName;
		
		this.init();
	}
	
	$.extend(Plugin.prototype, {
		// initialization logic
		init: function () {
			var o = this.options;
			var self = this;
			var $el = this.$el;

			//instance properties
			self.selectId = getUniqueId();

			//main DOM elements
			self.inititalValue = null;
			self.selectLabel = null;
			self.selectBox = null;
			self.selectOptions = null;
			self.linkGroup = $(o.selectors.linkGroup, $el);
			self.optionsAsLinks = $(o.selectors.optionsAsLinks, $el);

			//inital calls
			self.bindEvents();
			self.updateAndRenderOptions();
		},

		updateAndRenderOptions: function(){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			var oldSelectBox = self.selectBox;
			var oldLabel = self.selectLabel;

			//create new selectbox
			self.selectBox = $('<select></select>')
				.addClass(o.classes.selectElement)
				.prop('id', self.selectId)
			;

			//create new label
			self.selectLabel = $('<label></label>')
				.addClass(o.classes.labelElement)
				.html( '<span>'+ o.navigationLabel +'</span>' )
				.prop('for', self.selectId)
			;

			self.optionsAsLinks.each(function(){
				var link = $(this);
				var label = $.trim(link.text());
				var href = link.data('href') || link.attr('href');
				var isActive = link.is(o.selectors.activeOptionLink);
				var optionForLink = $('<option></option>')
					.text( label )
					.prop('selected', isActive)
					.prop('value', href)
				;

				//app option for link to select element
				self.selectBox.append(optionForLink);
			});

			self.selectBox.prependTo($el);
			self.selectLabel.prependTo($el);

			//update collection of options
			self.selectOptions = $('option', self.selectBox);

			//remove old elements
			if(oldSelectBox){
				oldSelectBox.remove();
			}

			if(oldLabel) {
				oldLabel.remove();
			}
		},

		//bind events for plugin
		bindEvents: function(){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			$el.on('click', 'label', function(e){
				if(self.selectBox){
					var e = document.createEvent("MouseEvents");
					var selectElement = self.selectBox[0];
					var worked;

					if (document.createEvent) { // all browsers
						e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
						worked = selectElement.dispatchEvent(e);
					} else if (selectElement.fireEvent) { // ie
						worked = selectElement.fireEvent("onmousedown");
					}

					//check if open select could not be opened, if so show options normally
					if(!worked){
						$el.addClass(o.classes.openFailed);
					}
				}
			});

			$el.on('focus', 'select', function(e){
				if(self.selectBox){
					self.inititalValue = self.selectBox.val();
				}
			})

			$el.on('change', 'select', function(e){
				self.handleSelectEvent(e);
			})
		},

		handleSelectEvent: function(e){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			if(!self.selectBox) {
				return false
			}

			var currentValue = self.selectBox.val();
			var selectedOption = self.selectOptions.filter(':selected').first();
			var selectedIndex = self.selectOptions.index( selectedOption );

			if(currentValue && currentValue !== self.inititalValue && currentValue.length > 1) {
				location.href = currentValue;
			}
		}
	});

	var getUniqueId = (function(){
		var timeId = new Date().getTime();
		return function(){
			timeId++;
			return [pluginName, timeId].join('-')
		}
	}());

	// lightweight plugin wrapper around the constructor, 
	// preventing against multiple instantiations
	$.fn[pluginName] = function ( options ) {
		return this.each(function () {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, 
				new Plugin( this, options ));
			}
		});
	}

})( jQuery, window, document );