/**
 * contestCrop Plugin
 * Milschnitte | My Break Photo Contest | Crop Widget
 *
 * Copyright brandung GmbH & Co.KG
 * http://www.brandung.de/
 *
 * Date: 22.01.2016
 * MIT License (MIT)
 *
 * Author: Felix Leupold (xiel.de)
 */

;(function ( $, window, document, undefined ) {
	"use strict";

	var pluginName = 'contestCrop';
	var sessionStorage = 'sessionStorage' in window && 'setItem' in window.sessionStorage && window.sessionStorage;

	//option defaults
	var defaults = {
		cropWidth: 325,
		cropHeight: 325,
		selectors: {
			cropWrapper: '.dropzone__crop',

			//controls
			controlZoomIn: '.crop-control--plus',
			controlZoomOut: '.crop-control--minus',
			controlFit: '.crop-control--fit',
			controlRotateLeft: '.crop-control--rotate-left',
			controlRotateRight: '.crop-control--rotate-right',

			//form data
			submitForm: '> form',
			dataInputs: '.contest-crop__datainput'
		},
		classes: {
			cropLoading: 'dropzone__crop--loading',
			cropReady: 'dropzone__crop--ready'
		}
	};

	//plugin constructor
	function Plugin( element, options ) {
		this.element = element;
		this.$el = $(this.element);

		//get options from html 
		var optionsFromHTML = this.$el.data();

		//extend defaults with given options and optionsFromHTML
		this.options = $.extend( true, {}, defaults, options, optionsFromHTML ) ;
		
		this._defaults = defaults;
		this._name = pluginName;
		
		this.init();
	}
	
	$.extend(Plugin.prototype, {
		// initialization logic
		init: function () {
			var o = this.options;
			var self = this;
			var $el = this.$el;
			 
			//instance properties
			this.guillotineInstance = null;

			//main DOM elements
			this.cropWrapper = $(o.selectors.cropWrapper, $el);
			this.imageToCrop = $('img', this.cropWrapper).first();
			this.submitForm = $(o.selectors.submitForm, $el).first();
			this.dataInputs = $(o.selectors.dataInputs, this.submitForm);
			this.dataInputs.withKey = {};

			//controls
			this.controls = {
				ZoomIn: $(o.selectors.controlZoomIn, $el),
				ZoomOut: $(o.selectors.controlZoomOut, $el),
				Fit: $(o.selectors.controlFit, $el),
				RotateLeft: $(o.selectors.controlRotateLeft, $el),
				RotateRight: $(o.selectors.controlRotateRight, $el)
			}

			//inital calls
			this.bindEvents();

			//TODO: move to loadImage function -->
			this.cropWrapper.addClass(o.classes.cropLoading);

			this.imageToCrop.on('load', function(){
				self.initCropViewWithImage();
			});

			if(this.imageToCrop.prop('complete')){
				self.initCropViewWithImage();
			}
		},

		//bind main widget events
		bindEvents: function(){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			this.cropWrapper.on('cropChanged', $.proxy(this.cropDidChange, self) );

			this.controls.ZoomIn.on('click', function(e){
				self.guillotineInstance && self.guillotineInstance.zoomIn();
			});

			this.controls.ZoomOut.on('click', function(e){
				self.guillotineInstance && self.guillotineInstance.zoomOut();
			});

			this.controls.Fit.on('click', function(e){
				self.guillotineInstance && self.guillotineInstance.fit();
			});

			this.controls.RotateLeft.on('click', function(e){
				self.guillotineInstance && self.guillotineInstance.rotateLeft();
			});

			this.controls.RotateRight.on('click', function(e){
				self.guillotineInstance && self.guillotineInstance.rotateRight();
			});

			this.submitForm.on('submit', function(e){	
				console.log('submit', e);
			});
		},

		//start Guillotine crop view
		initCropViewWithImage: function(){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			var initialData = false;

			if(sessionStorage) {
				try {
					//try to get restore data from previous crop
					var initialDataJsonString = sessionStorage.getItem('contestCrop') || null;
					initialData = JSON.parse( initialDataJsonString );

					//check if image url is same with the one that the data was saved for
					if(initialData.image !== this.imageToCrop.prop('src')) {
						initialData = false;
					}
				} catch(err){}
			}

			//init guillotine plugin
			this.imageToCrop.guillotine({
				width: o.cropWidth,
				height: o.cropHeight,
				eventOnChange: 'cropChanged',
				init: initialData || {}
			});

			this.guillotineInstance = this.imageToCrop.guillotine('instance');

			//initial fit (centered crop and auto zoom)
			if(!initialData) {
				this.cropWrapper.one('cropChanged', function(){
					//with little timeout to show transition
					setTimeout(function(){
						self.cropWrapper.removeClass(o.classes.cropLoading);
						self.cropWrapper.addClass(o.classes.cropReady);
					}, 50);
				});

				//fit image in
				self.imageToCrop.guillotine('fit');
			} else {
				//with little timeout to show transition
				setTimeout(function(){
					self.cropWrapper.removeClass(o.classes.cropLoading);
					self.cropWrapper.addClass(o.classes.cropReady);
				}, 200);
			}

			//update form data with initial data
			setTimeout(function(){
				self.cropDidChange();
			});
		},

		//gets called when crop was changed by user or by code
		cropDidChange: function(event, _data, eventType){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			var data = _data || this.guillotineInstance.getData();

			if(sessionStorage) {
				try {
					data.image = this.imageToCrop.prop('src');
					sessionStorage.setItem('contestCrop', JSON.stringify(data) );
				} catch(err){}
			}

			//transform data from guillotine to wished format
			var dataToSubmit = {
				scale: data.scale,
				angle: data.angle,
				x: data.x,
				y: data.y,
				width: data.w,
				height: data.h
			};

			this.updateFormData(dataToSubmit);
		},

		//update hidden input fields with crop data
		updateFormData: function(dataToSubmit){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			//update the hidden inputs with the crop data
			for (var dataKey in dataToSubmit) {
				var dataInputForKey = self.dataInputs.withKey[dataKey];

				//look up in DOM and cache element
				if(!dataInputForKey){
					dataInputForKey = self.dataInputs.filter('[name='+ dataKey +']');
					self.dataInputs.withKey[dataKey] = dataInputForKey;
				}

				dataInputForKey.val(dataToSubmit[dataKey]);
			}
		}
	});

	// lightweight plugin wrapper around the constructor, 
	// preventing against multiple instantiations
	$.fn[pluginName] = function ( options ) {
		return this.each(function () {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, 
				new Plugin( this, options ));
			}
		});
	}

})( jQuery, window, document );