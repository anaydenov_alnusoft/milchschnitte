/**
 * contestText Plugin
 * Milschnitte | My Break Photo Contest | Text Widget
 *
 * Copyright brandung GmbH & Co.KG
 * http://www.brandung.de/
 *
 * Date: 22.01.2016
 * MIT License (MIT)
 *
 * Author: Felix Leupold (xiel.de)
 */

;(function ( $, window, document, undefined ) {
	"use strict";

	var pluginName = 'contestText';

	//option defaults
	var defaults = {
		maxlengthDefault: 99,
		minlengthDefault: 0,
		selectors: {
			//form
			submitForm: '> form',
			submitBtn: '.contest-textinput__submit',

			//textinput
			inputContainer: '.contest-textinput',
			inputTextarea: '.contest-textinput textarea',
			remainingContainer: '.contest-textinput__remaining',
			remainingCounter: '.contest-textinput__remaining-counter'
		},
		classes: {
			dropzoneActive: 'dropzone--dragover',
			remainingCounterError: 'contest-textinput__remaining--error'
		}
	};

	//plugin constructor
	function Plugin( element, options ) {
		this.element = element;
		this.$el = $(this.element);

		//get options from html 
		var optionsFromHTML = this.$el.data();

		//extend defaults with given options and optionsFromHTML
		this.options = $.extend( true, {}, defaults, options, optionsFromHTML ) ;
		
		this._defaults = defaults;
		this._name = pluginName;
		
		this.init();
	}
	
	$.extend(Plugin.prototype, {
		// initialization logic
		init: function () {
			var o = this.options;
			var self = this;
			var $el = this.$el;

			//instance properties
			//...

			//main DOM elements
			this.submitForm = $(o.selectors.submitForm, $el).first();
			this.submitBtn = $(o.selectors.submitBtn, $el).first();
			this.inputContainer = $(o.selectors.inputContainer, $el).first();
			this.inputTextarea = $(o.selectors.inputTextarea, $el).first();

			this.remainingContainer = $(o.selectors.remainingContainer, $el).first();
			this.remainingCounter = $(o.selectors.remainingCounter, $el).first();

			//inital calls
			this.bindEvents();
			this.handleUserTextChange();
		},

		//bind main widget events
		bindEvents: function(){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			//user uses label / file input to select a file
			this.inputTextarea.on('change input', $.throttle(100, $.proxy(this.handleUserTextChange, this) ) );
			this.submitForm.on('submit', $.proxy(this.handleUserTextChange, this));
		},

		//react to user input ()
		handleUserTextChange: function(_e){
			var o = this.options;
			var self = this;
			var $el = this.$el;
			var e = _e || {};

			var maxlength = this.inputTextarea.data('maxlength') || this.inputTextarea.attr('maxlength') || o.maxlengthDefault;
			var minlength = this.inputTextarea.data('minlength') || this.inputTextarea.attr('minlength') || o.minlengthDefault;
			var userInput = this.inputTextarea.val();

			//sanitize & normalize user input
			var userInputNormalized = $.trim(userInput);

			//remove double line breaks and whitespaces
			userInputNormalized = userInputNormalized.replace(/\n\s+/g, '\n');
			userInputNormalized = userInputNormalized.replace(/\s+\n/g, '\n');
			userInputNormalized = userInputNormalized.replace(/\s{2,}/g, ' ');

			//remove a set of special characters
			// userInputNormalized = userInputNormalized.replace(/[\_\+\-\.\,\!\@\§\#\$\%\^\&\*\(\)\;\\\/\|\<\>\"\=']/g, '');

			var stringToCount = userInput.replace(/\n/g, '');
			var charactersRemaining = maxlength - stringToCount.length;

			//update remaining counter and set error class
			this.remainingCounter.text(charactersRemaining);

			if(charactersRemaining >= 0){
				this.remainingContainer.removeClass(o.classes.remainingCounterError);

				if(stringToCount.length >= minlength) {
					this.submitBtn.prop('disabled', false);
				} else {
					this.submitBtn.prop('disabled', true);
				}
			} else {
				this.remainingContainer.addClass(o.classes.remainingCounterError);
				this.submitBtn.prop('disabled', true);
			}

			if(e && e.type === 'change'){
				//cut off overflow
				//userInputNormalized = userInputNormalized.substr(0, maxlength);

				//set normalized input
				this.inputTextarea.val(userInputNormalized);

				//re-check and update counter
				this.handleUserTextChange();
			}
		}
	});

	// lightweight plugin wrapper around the constructor, 
	// preventing against multiple instantiations
	$.fn[pluginName] = function ( options ) {
		return this.each(function () {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, 
				new Plugin( this, options ));
			}
		});
	}

})( jQuery, window, document );