/**
 * infiniteScroll Plugin
 * Milschnitte | My Break Photo Contest | Infinite Scroll for Daily Gallery
 *
 * Copyright brandung GmbH & Co.KG
 * http://www.brandung.de/
 *
 * Date: 03.02.2016
 * MIT License (MIT)
 *
 * Author: Felix Leupold (xiel.de)
 */

;(function ( $, window, document, undefined ) {
	"use strict";

	var pluginName = 'infiniteScroll';
	var rAF = window.requestAnimationFrame || setTimeout;

	//option defaults
	var defaults = {
		itemsApi: '', //json api
		itemsKey: 'items',
		nextItemsApiKey: 'nextItems',
		endPosTolerance: 50,
		itemsPerShow: 4,
		reCheckThrottleInterval: 250,
		selectors: {
			itemsContainer: '> ul'
		},
		template: function(item){
			console.warn('infiniteScroll | please add a proper item template');
			return '<li>'+ JSON.stringify( item ) +'</li>'
		},
		callbacks: {
			//gets called before adding of elements (return false prevents adding)
			willAdd: function(elements){
				console.log('willAdd callback', this, elements);
				return
			},
			//callback after the elements were added to the itemsContainer
			didAdd: function(elements){
				console.log('didAdd callback', this, elements);
				return
			}
		}
	};

	//plugin constructor
	function Plugin( element, options ) {
		this.element = element;
		this.$el = $(this.element);

		//get options from html 
		var optionsFromHTML = this.$el.data();

		//extend defaults with given options and optionsFromHTML
		this.options = $.extend( true, {}, defaults, options, optionsFromHTML ) ;
		
		this._defaults = defaults;
		this._name = pluginName;
		
		this.init();
	}
	
	$.extend(Plugin.prototype, {
		// initialization logic
		init: function () {
			var o = this.options;
			var self = this;
			var $el = this.$el;

			//check if items api given
			if(!o.itemsApi) {
				return console.error('infiniteScroll | need itemsApi url to initialize');
			}

			//instance properties
			self.loadedItems = [];
			self.itemsFailedToLoad = [];
			self.newItems = [];
			self.loadNextItemsFrom = o.itemsApi;
			self.itemsLoadingInProgress = false;
			self.lastCheckIfNewItemsNeeded = 0;
			self.afterCheck = null;
			self.scrollTop = window.scrollY || $(window).scrollTop();
			self.scrollBottomPos = self.scrollTop + window.innerHeight;

			//main DOM elements
			self.itemsContainer = $(o.selectors.itemsContainer, $el).first();

			//inital calls
			this.bindEvents();

			//load first item set initially
			this.loadMoreItems( self.loadNextItemsFrom );
		},

		//bind main widget events
		bindEvents: function(){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			//bind for newItemsAvailable event, which gets fired after items were loaded from itemsApi
			$(self).on('newItemsAvailable', function(){
				self.checkIfNewItemsShouldBeLoaded();
			});

			//recheck after scroll/resize.... debounce this later
			$(window).on('scroll resize', function(){
				self.checkIfNewItemsShouldBeLoaded();
			});
		},

		checkIfNewItemsShouldBeLoaded: function(){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			if(self.itemsLoadingInProgress){
				return
			}

			var now = +new Date();

			//throttle checks…
			if(now - self.lastCheckIfNewItemsNeeded < o.reCheckThrottleInterval ) {
				//recheck at the very end of throttle
				clearTimeout(self.afterCheck);
				self.afterCheck = setTimeout(function(){
					self.checkIfNewItemsShouldBeLoaded();
				}, o.reCheckThrottleInterval * 2 );
				return;
			}

			self.lastCheckIfNewItemsNeeded = +new Date();

			//container dims
			var itemsContainerOffset = self.itemsContainer.offset(); //cache this later
			var containerHeight = self.itemsContainer.height();
			var containerScrollEndPos = itemsContainerOffset.top + containerHeight - (o.endPosTolerance || 0);

			//scroll dims
			var windowHeight = window.innerHeight || $(window).height();
			self.scrollTop = window.scrollY || $(window).scrollTop();
			self.scrollBottomPos = self.scrollTop + window.innerHeight;

			//check if 
			if( (self.scrollTop > itemsContainerOffset.top || containerHeight < windowHeight) && self.scrollBottomPos >= containerScrollEndPos) {
				self.loadAddNextItems();
			}
		},

		loadMoreItems: function(url){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			if(self.newItemsRequested){
				return console.log('infiniteScroll | already loading new items');
			}

			if(!url) {
				return console.error('infiniteScroll | loadMoreItems needs an url to load more items');
			}

			self.newItemsRequested = true;

			$.ajax({
				url: url
			})
			.done(function(data){
				var newItems = data[o.itemsKey];

				//loadNextItemsFrom url was used to load new items, remove it, so items won't be loaded again
				if(url === self.loadNextItemsFrom){
					self.loadNextItemsFrom 
				}

				if(!newItems){
					return console.error('infiniteScroll | no items found for itemsKey in result', o.itemsKey, data);
				}

				//add load items to the array from which items will be loaded
				$.each(newItems, function(i, newItem){
					self.newItems.push(newItem)
				});

				if(o.nextItemsApiKey && o.nextItemsApiKey in data) {
					self.loadNextItemsFrom = data[o.nextItemsApiKey]
				} else {
					self.loadNextItemsFrom = false;
				}

				$(self).trigger('newItemsAvailable');
			})
			.fail(function(){
				//...
				console.error('loading failed', arguments);
			})
			.always(function(){
				//request for new items is done
				self.newItemsRequested = false;
			});
		},

		loadAddNextItems: function(){
			var o = this.options;
			var self = this;
			var $el = this.$el;

			var itemLoadPromises = [];
			var itemsPerShow = o.itemsPerShow || 1;

			if(typeof itemsPerShow === 'function'){
				itemsPerShow = parseInt(itemsPerShow.call(self) || self._defaults.itemsPerShow);
			}

			//get number of items to load (limited by available items)
			var itemCountToLoad = Math.min(itemsPerShow || 1, self.newItems.length);

			//get array of items to load and remove those items from the set
			var itemsToLoad = self.newItems.splice(0, itemCountToLoad);

			//return if there are no items to load
			if(!itemsToLoad || itemsToLoad.length === 0) {
				//load more items from new api url, or increase debounce
				if(self.loadNextItemsFrom){
					self.loadMoreItems(self.loadNextItemsFrom);
				} else {
					o.reCheckThrottleInterval += 10;
				}
				return;
			}

			//stop more calls to load func
			self.itemsLoadingInProgress = true;

			//get loading promises for items with templates
			$.each(itemsToLoad, function(i, itemToLoad){
				itemLoadPromises.push( self.loadItemAndGetTemplateAsPromise(itemToLoad) );
			});

			//wait for all loadings to finish, then add them to item container
			$.when.apply(self, itemLoadPromises)
				//when all itemLoadPromises were resolved
				.done(function(){
					var loadedElements = Array.prototype.slice.call(arguments, 0);
					var $loadedElements = $(loadedElements);

					//stop added elements, when willAdd returns false
					if(typeof o.callbacks.willAdd === 'function' && o.callbacks.willAdd.call(self, $loadedElements) === false) {
						return
					}

					//append elements
					self.itemsContainer.append( loadedElements )

					//call callback from options
					if(typeof o.callbacks.didAdd === 'function') {
						rAF(function(){
							o.callbacks.didAdd.call(self, $loadedElements);
						}, 16);
					}
				})
				//called immediately when one of the items was rejected
				.fail(function(){
					//readd items that were able to load, but other items in the group did not
					$.each(itemLoadPromises, function(i, itemLoadPromise){
						if(itemLoadPromise.state() !== 'rejected' ){
							self.newItems.unshift(itemsToLoad[i]);
						} else {
							self.itemsFailedToLoad.push(itemsToLoad[i]);
						}
					})
				})
				.always(function(){
					self.itemsLoadingInProgress = false;

					setTimeout(function(){
						self.checkIfNewItemsShouldBeLoaded();
					}, o.reCheckThrottleInterval );
				})
			;
		},

		//
		loadItemAndGetTemplateAsPromise: function(item) {
			var o = this.options;
			var self = this;
			var $el = this.$el;

			if(!item) {
				return console.error('infiniteScroll | loadItemAndGetTemplateAsPromise() called without item to load');
			}

			var itemLoadPromise = $.Deferred();
			var allItemMediaSuccessfullyLoaded = true;
			var itemTemplate = o.template( item );
			var itemElement = $( itemTemplate );
			var mediaElementsToLoad = $('img', itemElement);
			var mediaElementsToLoadCount = mediaElementsToLoad.length;

			//bind load listeners and check if item was already loaded before / cached
			$.each(mediaElementsToLoad, function(i, _mediaElementToLoad){
				var mediaElementToLoad = $(_mediaElementToLoad);
				var completeCheck = setTimeout(function(){
					if(mediaElementToLoad.prop('complete')){
						mediaElementToLoad.off('load.'+pluginName + ' error.'+pluginName);
						mediaLoaded();
					}
				});

				//bind load listener
				mediaElementToLoad.on('load error', function(e){
					clearTimeout(completeCheck);
					mediaLoaded(e.type === 'error');

					if(e.type === 'error'){
						item.errorEvent = e;
					}
				});
			});

			//if there is no media at all
			if(mediaElementsToLoadCount === 0){
				allMediaLoaded();
			}

			//count down mediaElementsToLoadCount
			function mediaLoaded(didFail){
				allItemMediaSuccessfullyLoaded = allItemMediaSuccessfullyLoaded && !didFail;
				mediaElementsToLoadCount--;

				if(mediaElementsToLoadCount === 0){
					allMediaLoaded();
				}
			}

			//resolve or reject load promise after all media were loaded
			function allMediaLoaded(){
				if(allItemMediaSuccessfullyLoaded){
					setTimeout(function(){
						itemLoadPromise.resolve(itemElement[0]);
					}, 50 + Math.random() * 100 )
				} else {
					itemLoadPromise.reject();
				}
			}

			return itemLoadPromise;
		}
	});

	// lightweight plugin wrapper around the constructor, 
	// preventing against multiple instantiations
	$.fn[pluginName] = function ( options ) {
		return this.each(function () {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, 
				new Plugin( this, options ));
			}
		});
	};

		// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
	// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
	// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
	// MIT license
	;(function() {
		var lastTime = 0;
		var vendors = ['ms', 'moz', 'webkit', 'o'];
		for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
			window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
			window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
									   || window[vendors[x]+'CancelRequestAnimationFrame'];
		}
	 
		if (!window.requestAnimationFrame)
			window.requestAnimationFrame = function(callback, element) {
				var currTime = new Date().getTime();
				var timeToCall = Math.max(0, 16 - (currTime - lastTime));
				var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
				  timeToCall);
				lastTime = currTime + timeToCall;
				return id;
			};
	 
		if (!window.cancelAnimationFrame)
			window.cancelAnimationFrame = function(id) {
				clearTimeout(id);
			};
	}());


})( jQuery, window, document );