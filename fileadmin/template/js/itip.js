$(function () {



    // functions
    // **********************************
    // **********************************

    // get url with params
    // return catname, name of tip
    // example url: http://www.milchschnitte.de/infotainment/inseltipps/TabsNextPrev/index.html?cat=fantasie&name=hallo
    // **********************************
    var getUrlParams = function (url) {

        var params = [], hash;
        var finalUrl = url || document.URL;
        var hashes = finalUrl.slice(finalUrl.indexOf('?') + 1).split('&');

        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            params.push(hash[0]);
            params[hash[0]] = hash[1];
        }

        return params;
    }


    // init Tabfunction jQuery
    // **********************************
    var initTabs = function (el) {

        // init tab
        $tabs = $(el).tabs({
            active: 0,
            show: { effect: effectStyle, duration: effectDuration }
        });

        return $tabs;
    }


    // init prev next navigation in inset tab
    // **********************************
    var initTabNavigation = function (el, templateNav) {
        var currentCatItems = '#' + el.attr('id') + ' .ui-tabs-panel';

        // init next, prev tab for Items in categoryList
        $(currentCatItems).each(function (i) {
            var totalSize = $(currentCatItems).size() - 1;
            var current = '#' + $(this).attr('id');
            var prev = '#' + $(this).prev().attr('id');
            var next = '#' + $(this).next().attr('id');
            var number = i + 1;
            var numberTotal = $(currentCatItems).size();

            var currentCatId = '#' + $(this).closest('div.category-panel').attr('id');

            var currentCatString = $('#category-tab > ul li a[href=' + currentCatId + ']').html();
            $(this).prepend(templateNav);

            $(this).find('.number').html(number);
            $(this).find('.number-total').html(numberTotal);
            $(this).find('.active-cat').html(currentCatString);

            if (i != totalSize) {
                var targetTip = number + 1;
                $(this).find('.flip-nav-right').prepend('<a class="next-tab" targetTip="' + targetTip + '" href="' + next + '"><span class="arrow left"></span>Nächster Tipp</a>');
                $(this).find('.cta-field').prepend('<a class="cta-btn goto-next" targetTip="' + targetTip + '" href="' + next + '">N&auml;chster Tipp<span class="arrow"></span></a>');
            }
            if (i == totalSize) {
                $(this).find('.cta-field').prepend('<a class="cta-btn goto-send-ip" href="#inseltipp-einsenden">Tipp einreichen<span class="arrow"></span></a>');
            };

            if (i != 0) {
                var targetTip = number - 1;
                $(this).find('.flip-nav-left').prepend('<a class="prev-tab" targetTip="' + targetTip + '" href="' + prev + '"><span class="arrow"></span>Vorheriger Tipp</a>');
            }
        });
    }



    // initCatItems
    // **********************************
    var initCatItems = function (templateNav) {

        // run each category
        $('#category-tab > .ui-tabs-panel').each(function (i) {

            // first and last element is ignored, because they have not an inset tab-element
            var size = $('#category-tab > .ui-tabs-panel').size();

            if (i > 0 && i < size) {

                var currentCat = $(this);

                // init tabfunction jQuery
                initTabs(currentCat);


                // init prev next navigation in categoryItems
                initTabNavigation(currentCat, templateNav);
            }
        });

    }

    // initAccordions
    // **********************************
    var initAccordions = function (classEl) {
        $(classEl).each(function (i) {

            $(this).attr('id', 'accordion-' + i).accordion({
                collapsible: true,
                active: false,
                heightStyle: "content"
            });

        });
    }


    // global Vars
    // **********************************
    // **********************************
    var effectDuration = 500;
    var effectStyle = 'fadeIn';

    var allParams = getUrlParams();
    var paramsByCat = getUrlParams()['cat'];
    var paramsByName = getUrlParams()['name'];

    var templateNav = '<div class="row-fluid flip">'
                        + '<div class="span4 flip-nav-left">'
                        + '</div>'
                        + '<div class="span4">'
                        + '<span class="active-cat"></span> <span class="number"></span> von <span class="number-total"></span>'
                        + '</div>'
                        + '<div class="span4 flip-nav-right">'
                        + '</div>'
                        + '</div>';


    // inits
    // **********************************
    // **********************************

    // 1. init CategoryList
    $('#category-tab > ul li a').each(function (i) {
        var url = $(this).attr('href');
        $(this).attr('href', '#' + getUrlParams(url)['cat']);
    });
    $categoryList = initTabs('#category-tab');


    // 2. init CategoryListItems of Fantasie, Kreativ ...
    $('#category-tab > .ui-tabs-panel ul li a').each(function (i) {
        var url = $(this).attr('href');
        $(this).attr('href', '#' + getUrlParams(url)['name']);
    });
    initCatItems(templateNav);

    // 3. if deeplink
    //    Parameter cat, set active tab by url
    if ($('ul li a[href="' + paramsByCat + '"]')) {
        $categoryList.tabs('select', '#' + paramsByCat);

        // if Parameter name, set active tab by url
        if ($('#' + paramsByCat + ' ul ul li a[href="' + paramsByName + '"]')) {
            $('#' + paramsByCat).tabs('select', '#' + paramsByName);
        }
    }

    // clickEvent on next, prev tab for Items in categoryList
    $('.next-tab, .prev-tab, .goto-next').click(function (e) {

        e.preventDefault();
        var currentCat = '#' + $(this).closest('div.category-panel').attr('id');
        $(currentCat).tabs('select', $(this).attr("href"));
        $('html, body').animate({ scrollTop: $('#category-tab').offset().top });
        navpos = $('.flip').offset();

        var targetTip = $(e.currentTarget).attr('targetTip');
        ms.trackEvent('Inseltipps - ' + currentCat.substr(1) + ' - Tipp' + targetTip, '/inseltipps/' + currentCat.substr(1) + '/tipp' + targetTip);
    });

    // clickEvent on next, prev tab for Items in categoryList
    $('.goto-ip, .goto-send-ip').click(function (e) {

        e.preventDefault();
        var currentCat = $(this).attr("href");
        $categoryList.tabs('select', currentCat);
        $('html, body').animate({ scrollTop: $('#category-tab').offset().top });
        navpos = $('.flip').offset();
        ms.trackEvent('Inseltipps - ' + currentCat.substr(1) + ' - Tipp1', '/inseltipps/' + currentCat.substr(1) + '/tipp1');
    });

    // click handler on tabs for tracking
    $('#category-tab > ul li a').each(function (idx, el) {
        $(el).click(
            function (e) {
                var currentCat = $(this).attr('href');
                ms.trackEvent('Inseltipps - ' + currentCat.substr(1) + ' - Tipp1', '/inseltipps/' + currentCat.substr(1) + '/tipp1');
            }
        );
    });

    // Dropdown Select
    var dropdowns = $(".dropdown");

    // Onclick on a dropdown, toggle visibility
    dropdowns.find("dt").click(function () {
        dropdowns.find("dd ul").hide();
        $(this).next().children().toggle();
    });


    if ($('#category').val() != "") {
        $('#category-dropdown dt a span').text($('#category').val());
    }
    // Clic handler for dropdown
    dropdowns.find("dd ul li a").click(function () {
        var leSpan = $(this).parents(".dropdown").find("dt a span");

        // Remove selected class
        $(this).parents(".dropdown").find('dd a').each(function () {
            $(this).removeClass('selected');
        });

        // Update selected value
        leSpan.text($(this).text());
        if ($(this).text() != "") {
            $('input#category').val($(this).text());
            $('input#category').removeClass('error');
            $('input#category + label.error').remove();
        }



        // If back to default, remove selected class else addclass on right element
        if ($(this).hasClass('default')) {
            leSpan.removeClass('selected')
        }
        else {
            leSpan.addClass('selected');
            $(this).addClass('selected');
        }

        // Close dropdown
        $(this).parents("ul").hide();
    });

    // Accordion Spielanleitung
    initAccordions('.ui-accordion');


    var navPos = $('header').height() + $('.header-tipps').height() + $('#category-tab > ul').height() + $('.flip').height() + 80;


    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > navPos) {
            $('.flip').addClass('fixed');
            $('.flip').parent('div').addClass('fixed-nav');
        }
        else {
            $('.flip').removeClass('fixed');
            $('.flip').parent('div').removeClass('fixed-nav');
        }
    });

    // Close all dropdown onclick on another element
    $(document).bind('click', function (e) {
        if (!$(e.target).parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
    });


    var form = "#form-send-tip";
    var validForm = false;
    $('#thank-you').hide();
    $('#form-send-tip-error').hide();
    $('.status.send-tip').hide();

    submittingForm = function (form) {
        $('.status.send-tip').show();

        sData = {
            category: '', // $('select#category').val(),
            tipTitle: $('#tiptitle').val(),
            tipDescription: $('#tipdesc').val(),
            firstName: $('#firstname').val(),
            lastName: $('#lastname').val(),
            city: $('#city').val(),
            email: $('#email').val(),
            textChildren: $('#text-child').val(),
        };

        var serviceUrl = rootPath + "/WebService.asmx/InselTipp";

        $.ajax({
            'type': 'POST',
            'contentType': 'application/json; charset=utf-8',
            'url': serviceUrl,
            'data': JSON.stringify(sData, null, 2),
            'dataType': 'json',
            'success': function (rData) {
                if (rData.d.Status == 1) {
                    $('#form-send-tip').hide();
                    $('#form-send-tip-error').hide();
                    $('.status.send-tip').hide();
                    $('#thank-you').show();
               } else {
                    $('#form-send-tip-error').show();
                    $('.status.send-tip').hide();
                }
            },
            'error': function (request, status, error) {
                $('#form-send-tip-error').show();
                $('.status.send-tip').hide();
            }
        });

        return false;

    }

    $('input#send-tip').click(function (e) {
        $('#form-send-tip').validate({
            submitHandler: function (form) {
                submittingForm('#form-send-tip');
            },
            rules: {
                /*category: {
                    required: true,
                },*/
                tiptitle: {
                    required: true,
                    minlength: 2
                },
                tipdesc: {
                    required: true,
                    minlength: 2
                },
                firstname: {
                    required: true,
                    minlength: 2
                },
                lastname: {
                    required: true,
                    minlength: 2
                },
                email: {
                    required: true,
                    email: true
                },
                agree: "required"
            },
            messages: {
                //category: "Wählen Sie eine Kategorie",
                tiptitle: "Gib den Namen deines Inseltipps ein.",
                tipdesc: "Gib die Beschreibung deines Inseltipps ein.",
                firstname: "Gib deinen Vornamen ein.",
                lastname: "Gib deinen Nachnamen ein.",
                city: "Gib deine Stadt ein.",
                email: {
                    required: "Gib deine E-Mail-Adresse ein.",
                    email: "Deine E-Mail-Adresse muss in folgenden Format vorliegen: muster@domain.de"
                },
                agree: "Bitte akzeptiere die Datenschutzrichtlinien."
            }

        });

    });


});