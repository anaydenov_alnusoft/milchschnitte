
//-- http://stackoverflow.com/questions/3326650/console-is-undefined-error-for-internet-explorer
if (!window.console) console = { log: function () { } };


// jquery easing

// t: current time, b: begInnIng value, c: change In value, d: duration
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
    {
        def: 'easeOutQuad',
        swing: function (x, t, b, c, d) {
            //alert(jQuery.easing.default);
            return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
        },
        easeInQuad: function (x, t, b, c, d) {
            return c*(t/=d)*t + b;
        },
        easeOutQuad: function (x, t, b, c, d) {
            return -c *(t/=d)*(t-2) + b;
        },
        easeInOutQuad: function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return c/2*t*t + b;
            return -c/2 * ((--t)*(t-2) - 1) + b;
        },
        easeInCubic: function (x, t, b, c, d) {
            return c*(t/=d)*t*t + b;
        },
        easeOutCubic: function (x, t, b, c, d) {
            return c*((t=t/d-1)*t*t + 1) + b;
        },
        easeInOutCubic: function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return c/2*t*t*t + b;
            return c/2*((t-=2)*t*t + 2) + b;
        },
        easeInQuart: function (x, t, b, c, d) {
            return c*(t/=d)*t*t*t + b;
        },
        easeOutQuart: function (x, t, b, c, d) {
            return -c * ((t=t/d-1)*t*t*t - 1) + b;
        },
        easeInOutQuart: function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
            return -c/2 * ((t-=2)*t*t*t - 2) + b;
        },
        easeInQuint: function (x, t, b, c, d) {
            return c*(t/=d)*t*t*t*t + b;
        },
        easeOutQuint: function (x, t, b, c, d) {
            return c*((t=t/d-1)*t*t*t*t + 1) + b;
        },
        easeInOutQuint: function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
            return c/2*((t-=2)*t*t*t*t + 2) + b;
        },
        easeInSine: function (x, t, b, c, d) {
            return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
        },
        easeOutSine: function (x, t, b, c, d) {
            return c * Math.sin(t/d * (Math.PI/2)) + b;
        },
        easeInOutSine: function (x, t, b, c, d) {
            return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
        },
        easeInExpo: function (x, t, b, c, d) {
            return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
        },
        easeOutExpo: function (x, t, b, c, d) {
            return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
        },
        easeInOutExpo: function (x, t, b, c, d) {
            if (t==0) return b;
            if (t==d) return b+c;
            if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
            return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
        },
        easeInCirc: function (x, t, b, c, d) {
            return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
        },
        easeOutCirc: function (x, t, b, c, d) {
            return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
        },
        easeInOutCirc: function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
            return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
        },
        easeInElastic: function (x, t, b, c, d) {
            var s=1.70158;var p=0;var a=c;
            if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
            if (a < Math.abs(c)) { a=c; var s=p/4; }
            else var s = p/(2*Math.PI) * Math.asin (c/a);
            return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
        },
        easeOutElastic: function (x, t, b, c, d) {
            var s=1.70158;var p=0;var a=c;
            if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
            if (a < Math.abs(c)) { a=c; var s=p/4; }
            else var s = p/(2*Math.PI) * Math.asin (c/a);
            return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
        },
        easeInOutElastic: function (x, t, b, c, d) {
            var s=1.70158;var p=0;var a=c;
            if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
            if (a < Math.abs(c)) { a=c; var s=p/4; }
            else var s = p/(2*Math.PI) * Math.asin (c/a);
            if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
            return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
        },
        easeInBack: function (x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            return c*(t/=d)*t*((s+1)*t - s) + b;
        },
        easeOutBack: function (x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
        },
        easeInOutBack: function (x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
            return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
        },
        easeInBounce: function (x, t, b, c, d) {
            return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
        },
        easeOutBounce: function (x, t, b, c, d) {
            if ((t/=d) < (1/2.75)) {
                return c*(7.5625*t*t) + b;
            } else if (t < (2/2.75)) {
                return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
            } else if (t < (2.5/2.75)) {
                return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
            } else {
                return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
            }
        },
        easeInOutBounce: function (x, t, b, c, d) {
            if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
            return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
        }
    });

var tabs = function () {
    var $filter = $('.filter'),
        $filters = $('.filter a'),
        $targets = $('.filter--target');

    return {
        init: function (prefix) {
            $filters.on('click', function (event) {
                event.preventDefault();

                var $this = $(this);

                if (window.innerWidth <= 639) {
                    $filter.toggleClass('expanded');

                    if ($this.parent().hasClass('active')) {
                        return
                    };
                };

                var filter = this.hash.replace('#', '');
                $filters.parent().removeClass('active');
                $this.parent().addClass('active');

                $targets.parent().removeClass('hide');

                if (filter.length) {
                    $targets.not('.filter--' + filter).parent().addClass('hide');
                };
            });
        }
    }
}();

var shariff = function() {
    var $el = $('.shariff');

    return {
        init: function(element) {
            $el = element || $el;
            new Shariff($el, {
                services: ['facebook', 'twitter', 'googleplus', 'mail', 'whatsapp'],
                orientation: 'vertical',

                mailUrl: 'mailto:',
                mailSubject: $('meta[property="og:title"]').attr('content'),
                mailBody: $('meta[property="og:description"]').attr('content') + '\n' + location.href,
            });
        }
    }
}();

var msHome = function () {
    var self,
        description;

    return {
        init: function () {
            if (!$('body').is('.home')) return;

            self = this;

            description = $('<div class="slider-description-ext"></div>');

            $('.focuspoint').focusPoint();

            $('.flexslider').parent().append(description);

            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: false,
                directionNav: false,
                pauseOnHover: true,
                slideshowSpeed: 5000,
                start: self.updateDescription,
                before: self.updateDescription,
                touch: false
            });
        },

        updateDescription: function (fslider) {
            var slide = fslider.container.children(':not(.clone)').eq(fslider.animatingTo),
                html = slide.children('div.description').html();

            if (ms.inMobileView()) {
                description.fadeTo(100, 0, function () {
                    description.html(html).fadeTo(500, 1);
                });
            } else {
                description.html(html);
            }
        }
    }
}();

var msBaking = function () {
    var self;

    return {
        init: function () {
            self = this;
            if ($('body').is('.backen')) {
                tabs.init();

                // When clicking on video button display 2nd tab
                $('.cta-btn.video-cta').on('click', function() {
                    $('.tabs').find('li').eq(1).find('a').trigger('click');

                    $('html, body').animate({
                        scrollTop: $(".video-yt-wrapper").first().offset().top
                    }, 1000);
                });

                /*
                * Implements 'hover' function for touch devices
                 */
                $('.product-teaser__content-wrapper--hover').on('click', function (event) {
                    if ($(this).css('opacity') == 0)
                        event.preventDefault();
                });
            }

            if ($('body').is('.backen--detail') || $('body').is('.spielefinder--detail')) {
                var likedSites = [];
                shariff.init();

                if (typeof localStorage !== 'undefined' && typeof localStorage.MSlikedSites !== 'undefined') {
                    likedSites = JSON.parse(localStorage.MSlikedSites);
                }

                if (likedSites.indexOf(location.pathname) >= 0) {
                    $('.cta-btn.social-like').addClass('disabled');
                } else {
                    $('.cta-btn.social-like').on('click', function() {
                        $(this)
                            .addClass('disabled')
                            .off('click');

                        if (typeof localStorage !== 'undefined') {
                            likedSites.push(location.pathname);
                            localStorage.MSlikedSites = JSON.stringify(likedSites);
                        }
                        dataLayer.push({
                        // console.log({
                            'event' : 'recipeLike',
                            'status' : 'click',
                            'recipeURL' : location.pathname
                        });
                    })
                }

                $('a[download]').on('click', function () {
                    ms.trackDownload('document', $(this).attr('href'));
                });
            }

        },
    }
}();


var msTvSpots = function () {

    return {
        init: function () {
            var $videos = $('#ms-video, .ms-video');
            var $packshot = $('.packshot');

            $videos.each(function(){
                var $video = $(this);
                var videoWrapper = $video.closest('.video-wrapper');
                var isMobile = navigator.userAgent.match(/(iPhone)/g) || navigator.userAgent.match(/(Android)/g)

                var p = new MediaElementPlayer($video, {
                    defaultVideoWidth: 960,
                    defaultVideoHeight: 400,

                    videoWidth: 960,
                    videoHeight: 400,

                    iPhoneUseNativeControls: true,
                    AndroidUseNativeControls: true,

                    features:
                        navigator.userAgent.toLowerCase().indexOf('msie') >= 0 ?
                        ['playpause','progress','current','duration','tracks','volume'] :
                        ['playpause','progress','current','duration','tracks','volume','fullscreen']
                    ,

                    success: function (mediaEl, node, player) {
                        var $poster = $('.mejs-poster');
                        var $controls = $('.mejs-controls');
                        $controls.addClass('transparent');

                        if (!isMobile) {
                            $video.addClass('transparent');
                        };

                        mediaEl.addEventListener('ended', function (e) {
                            $poster.show();
                            $packshot.removeClass('hide');
                            $controls.addClass('transparent');
                            if (!isMobile) {
                                $video.addClass('transparent');
                            };
                        });

                        mediaEl.addEventListener('play', function (e) {
                            $packshot.addClass('hide');
                            $controls.removeClass('transparent');
                            $video.removeClass('transparent');
                        });


                        // Video started
                        var playHandler = function (e) {
                            ms.trackVideo('0');
                            mediaEl.removeEventListener('play', playHandler, false);
                            $packshot.addClass('hide');
                            $controls.removeClass('transparent');
                        }
                        mediaEl.addEventListener('play', playHandler, false);

                        // Video played half through
                        var timeupdateHandler = function (e) {
                            var e = e.currentTarget ? e.currentTarget : e;

                            // Played half through
                            if (e.currentTime > (e.duration / 2)) {
                                ms.trackVideo('50');
                                mediaEl.removeEventListener('timeupdate', timeupdateHandler, false);
                            }
                        }
                        mediaEl.addEventListener('timeupdate', timeupdateHandler, false);

                        // Video ended
                        var endedHandler = function (e) {
                            ms.trackVideo('100');
                            mediaEl.removeEventListener('ended', endedHandler, false);
                        }
                        mediaEl.addEventListener('ended', endedHandler, false);
                    }


                });

                if (isMobile) {
                    $video.parent().parent().addClass('native-video')
                }
            });

        }
    }
}();

msTvSpots.init();

var msSurvey = function () {
    var types = ['macher', 'skeptiker', 'netzwerker', 'improvisationstalent'],
        scores = {
            'macher': [
                {"positive":10,"neutral":-4,"negative":-8},
                {"positive":10,"neutral":-4,"negative":-8},
                {"positive":6,"neutral":-2,"negative":-4},
                {"positive":-4,"neutral":0,"negative":4},
                {"positive":8,"neutral":-4,"negative":-8},
                {"positive":8,"neutral":-4,"negative":-8},
                {"positive":4,"neutral":0,"negative":-4},
                {"positive":8,"neutral":0,"negative":-8}
            ],

            'skeptiker': [
                {"positive":-8,"neutral":0,"negative":8},
                {"positive":-8,"neutral":0,"negative":8},
                {"positive":-8,"neutral":-4,"negative":4},
                {"positive":4,"neutral":0,"negative":-4},
                {"positive":-6,"neutral":2,"negative":6},
                {"positive":-6,"neutral":2,"negative":6},
                {"positive":-4,"neutral":0,"negative":4},
                {"positive":-8,"neutral":0,"negative":8}
            ],

            'netzwerker': [
                {"positive":6,"neutral":-2,"negative":-6},
                {"positive":6,"neutral":-2,"negative":-6},
                {"positive":2,"neutral":2,"negative":-4},
                {"positive":16,"neutral":0,"negative":-16},
                {"positive":-2,"neutral":0,"negative":2},
                {"positive":-2,"neutral":0,"negative":2},
                {"positive":2,"neutral":0,"negative":-2},
                {"positive":4,"neutral":0,"negative":-4}
            ],

            'improvisationstalent': [
                {"positive":-6,"neutral":2,"negative":6},
                {"positive":-6,"neutral":2,"negative":6},
                {"positive":2,"neutral":2,"negative":-4},
                {"positive":-12,"neutral":0,"negative":12},
                {"positive":2,"neutral":0,"negative":-2},
                {"positive":2,"neutral":0,"negative":-2},
                {"positive":-2,"neutral":0,"negative":2},
                {"positive":-4,"neutral":0,"negative":4}
            ]
        };

    var $dom = $('#survey'),
        self;

    return {
        init: function () {
            self = this;

            this.completeMarkup();
            this.shuffleOptions();

            $dom.find('input.choice').change(function () {
                $(this).closest('li.question').find('a.cta-btn').removeClass('disabled');
                $(this).closest('ol').siblings('p.error').remove();
            });

            $dom.find('a.cta-btn').click(function (e) {
                e.preventDefault();

                if ($(this).is('.disabled')) {
                    var $this = $(this);

                    if (!$this.siblings('p.error').length) {
                        $this.before('<p class="error">Bitte wählen Sie eine Antwort!</p>');
                        $this.siblings('p.error').show();
                    }
                } else {
                    self.nextScreen();
                }
            });
        },

        evaluate: function () {
            var answers = [],
                gender = '',
                results = {
                    'macher': 0,
                    'skeptiker': 0,
                    'netzwerker': 0,
                    'improvisationstalent': 0
                };

            // collect answers
            $dom.find('input.choice:checked').each(function (i, inp) {
                answers.push($(this).parent().data('value'));
            });
            gender = answers.pop();

            // calculate scores
            $.each(answers, function (i, value) {
                results['macher'] += scores['macher'][i][value];
                results['skeptiker'] += scores['skeptiker'][i][value];
                results['netzwerker'] += scores['netzwerker'][i][value];
                results['improvisationstalent'] += scores['improvisationstalent'][i][value];
            });

            // tiebreak
            if (results['macher'] == results['netzwerker']) {
                if (answers[3] == 'positive') {
                    results['macher'] = -100;
                } else {
                    results['netzwerker'] = -100;
                }
            } else if (results['skeptiker'] == results['improvisationstalent']) {
                if (answers[3] == 'positive') {
                    results['skeptiker'] = -100;
                } else {
                    results['improvisationstalent'] = -100;
                }
            }

            // find highest score
            var max = Math.max(results['macher'], results['skeptiker'], results['netzwerker'], results['improvisationstalent']),
                type = '';

            $.each(types, function (i, val) {
                if (results[val] == max) {
                    type = val;
                    return false;
                }
            });

            var resultScreen = $dom.children('.result.' + type);
            resultScreen.addClass(gender);
            self.setScreen(resultScreen);
        },

        nextScreen: function () {
            var act = $dom.children('.active'),
                next = act.next();

            if (next.is('.question')) {
                self.setScreen(next);

                dcsMultiTrack(
                    'DCS.dcsuri', 'milch-schnitte/alltagstypentest/frage' + next.index(),
                    'WT.ti', 'Milch-Schnitte - Alltagstypentest - Frage ' + next.index(),
                    'WT.cg_n', 'Milch-Schnitte-Alltagstypentest'
                );
            } else {
                self.evaluate();

                dcsMultiTrack(
                    'DCS.dcsuri', 'milch-schnitte/alltagstypentest/auswertung',
                    'WT.ti', 'Milch-Schnitte - Alltagstypentest - Auswertung',
                    'WT.cg_n', 'Milch-Schnitte-Alltagstypentest'
                );
            }
        },

        setScreen: function (scr) {
            var act = $dom.children('.active');

            act.fadeOut(100, function () {
                scr.fadeIn(200, function () {
                    act.removeClass('active');
                    scr.addClass('active');
                });
            });

            $('html, body').animate({ scrollTop: 0 /* $('ul#survey').offset().top */ }, 100);
        },

        shuffleOptions: function () {
            $dom.find('li.question:not(.gender) ol').each(function (index, value) {
                var children = $(this).children().toArray();
                var len, random;
                while (children.length > 0) {
                    len = children.length;
                    random = parseInt(Math.random()*len);
                    $(children.splice(random, 1)).prependTo($(this));
                }
            });
        },

        completeMarkup: function () {
            $dom.children('.question').each(function (qi, el) {
                var $el = $(el);

                $el.find('ol > li').each(function (i, li) {
                    var $li = $(li),
                        val = li.className,
                        question = $li.html(),
                        id = 'q_' + qi + '_' + val;

                    $li
                        .data('value', val)
                        .html(
                            '<input type="radio" class="choice" name="answer_' + qi + '" id="' + id + '">' +
                            '<label for="' + id + '">' + question + '</label>'
                        );
                });

                $el.find('ol').after('<a class="cta-btn disabled" href="#">Nächste Frage<span class="arrow"></span></a>');
            });

            $dom.find('.question.gender a.cta-btn').html('Zur Auswertung<span class="arrow"></span>');
        }
    }
}();


var rootPath = (function () {
    if (window.location.host.indexOf('10.172.30.25') > -1 || window.location.host.indexOf('localhost') > -1)
        return 'http://10.172.30.25/milchschnitte';
    else return "http://" + window.location.host;
}());


var msOrderForm = function () {

    if(window.location.host == '192.168.42.200'){
        var config = {
            webservices: {
                price_url: '/milchschnitte/WebService.asmx/SimplePostageList',
                order_url: '/milchschnitte/WebService.asmx/Order'
            }
        };
    } else if(window.location.host == 'ms13.demo.gosub.de'){
        var config = {
            webservices: {
                price_url: 'http://' + window.location.host + '/SimplePostageList.json',
                order_url: 'http://' + window.location.host + '/Order.json'
            }
        };

    } else {
        var config = {
            webservices: {
                price_url: '/WebService.asmx/SimplePostageList',
                order_url: '/WebService.asmx/Order'
            }
        };
    }

    var self,
        container,
        step2,
        form;

    return {
        init: function () {
            self = this;
            container = $('div.row.order-step-1');
            step2 = $('div.row.order-step-2');
            form = $('#study-orderform');

            if (!form.length) return;

            form.submit(self.onSubmit).find('#uid8').change(self.updatePrice);
            self.loadPrices();
        },

        loadPrices: function () {
            if (form.data('study_price')) return;

            self.showLoader();

            $.ajax({
                'type': 'POST',
                'contentType': 'application/json; charset=utf-8',
                'url': config.webservices.price_url,
                'data': '',
                'dataType': 'json',
                'success': function (data) {
                    self.hideLoader();

                    if (data.d.Status == 1) {
                        form.data('study_price',data.d);
                        self.updatePrice();
                    } else {
                        container.find('.no-data').show();
                    }
                },
                'error': function () {
                    self.hideLoader();
                    container.find('.no-data').show();
                }
            });
        },

        updatePrice: function () {
            var qty = form.find('#uid8').val(),
                priceData = form.data('study_price'),
                calc = form.find('.price-calc');

            calc.find('.items').text(self.formatPrice(qty * priceData.ValueOfItem));
            calc.find('.postage').text(self.formatPrice(priceData.Postages[qty]));
            calc.find('.total').text(self.formatPrice(qty * priceData.ValueOfItem + priceData.Postages[qty]));
        },

        validateForm: function () {
            var success = true,
                email;

            $.each(['firstname', 'lastname', 'street', 'postalCode', 'city', 'email', 'emailConfirm'], function (i, field) {
                var inp = form.find('input[name="' + field + '"]'),
                    value = inp.val(),
                    isValid = value.length > 0;

                if (field == 'email') {
                    isValid = !!value.match(/.+@.+\..+/);
                    email = value;
                } else if (field == 'emailConfirm') {
                    isValid = isValid && (value == email);
                }

                inp.add(inp.prev()).toggleClass('error',!isValid);
                success = success && isValid;
            });

            var terms = form.find('input[name="agreesToTerms"]'),
                termsChecked = terms.is(':checked');

            terms.add(terms.next()).toggleClass('error',!termsChecked);
            success = success && termsChecked;

            var wider = form.find('input[name="agreesToWider"]'),
                widerChecked = wider.is(':checked');

            wider.add(wider.next()).toggleClass('error',!widerChecked);
            success = success && widerChecked;

            return success;
        },

        onSubmit: function (e) {
            e.preventDefault();

            if (!self.validateForm()) {
                window.location.hash = 'uid1';
                return;
            }

            var data = {};
            form.find('input[type="text"], select').each(function (i, el) {
                var inp = $(el);
                data[inp.attr('name')] = inp.val();
            });

            $.ajax({
                'type': 'POST',
                'contentType': 'application/json; charset=utf-8',
                'url': config.webservices.order_url,
                'data': JSON.stringify(data, null, 2),
                'dataType': 'json',
                'success': function (result) {
                    self.hideLoader();

                    if (result.d.Status == 1) {
                        var calc = form.find('.price-calc');
                        step2.find('.total').text(calc.find('.total').text());
                        step2.find('.order-number').text(result.d.OrderNumber);

                        var p = container.parent(),
                            ph = p.height(),
                            ch = container.height();

                        p.height(ph);
                        container.fadeOut(100, function () {
                            step2.fadeIn(300);
                        });

                        setTimeout(function () {
                            p.animate({ height: (step2.height() + (ph - ch)) }, 200, function () {
                                p.css('height', '');
                            });
                        }, 100);

                        dcsMultiTrack(
                            'DCS.dcsuri', 'milch-schnitte/alltagsstudie/service/buch-bestellung',
                            'WT.ti', 'Milch-Schnitte - Alltagsstudie - Service - Buch-Bestellung',
                            'WT.cg_n', 'Milch-Schnitte-Alltagsstudie'
                        );
                    } else {
                        container.find('.no-data').show();
                    }
                },
                'error': function (request, status, error) {
                    self.hideLoader();
                    container.find('.no-data').show();
                }

            });
        },

        formatPrice: function (p) {
            return String((p / 100).toFixed(2)).split('.').join(',');
        },

        showLoader: function () {
            container.addClass('loader');
        },

        hideLoader: function () {
            container.removeClass('loader');
        }
    }
}();

var ms = function () {

    var cfg = {
        tinysliderInterval: 4000
    };

    var self,
        lastHash = '';

    // Google Tag Manager Data Layer for Custom Tracking Events
    // https://developers.google.com/tag-manager/devguide#events
    var dataLayer = window.dataLayer;

    if(!dataLayer){
        console.error('Google Tag Manager | dataLayer is not available for custom tracking');
    }

    return {
        init: function () {
            self = this;

            msHome.init();
            msBaking.init();
            msSurvey.init();
            msOrderForm.init();

            self.initAccordions();
            self.initTinySliders();
            self.initCollapsible();
            self.initPopups();
            self.initAgeCheck();
            self.initPrint();
        },

        initPopups: function () {
            $('a.popup').fancybox({
                type: 'iframe',
                maxWidth: 600,
                maxHeight: 595,
                minHeight: 400,
                padding: 5,
                margin: 60,
                helpers : {
                    overlay : {
                        opacity : 0.8
                    }
                },
                beforeLoad: function () {
                    if (self.inMobileView()) {
                        window.open($(this).attr('href'), 'popup');
                        return false;
                    }
                }
            });
        },

        initCollapsible: function () {
            $('p.toggle-btn a').click(function (e) {
                var a = $(this),
                    content,
                    contentTop;

                if (a.attr('href').length > 1) {
                    content = a.parent().prev('.collapsible.' + a.attr('href'));
                    contentTop = a.parent().nextAll('.collapsible.' + a.attr('href'));
                } else {
                    content = a.parent().prev('.collapsible');
                    contentTop = a.parent().prev('.collapsible');
                }

                if (!content.length && !contentTop.length) return;
                e.preventDefault();

                $('.collapsible').slideUp();
                $('.cta-btn.toggle').not($(this)).removeClass('active');

                content[a.is('.active') ? 'slideUp' : 'slideDown']({
                    duration: 300,
                    easing: 'easeOutQuart'
                });
                contentTop[a.is('.active') ? 'slideUp' : 'slideDown']({
                    duration: 300,
                    easing: 'easeOutQuart'
                });
                a.toggleClass('active');
            });
        },

        initTinySliders: function () {
            $('.tiny-slider').each(self.initSlider);

            var _onResize = function () {
                $('.tiny-slider .slider-wrapper').each(function () {
                    var sw = $(this),
                        w = sw.width();

                    if (w) sw.find('li').width(w);
                });

                $('.tiny-slider .slider-wrapper li.active').each(function () {
                    var target = $(this),
                        w = target.width();

                    if (w) target.closest('.slider-wrapper').scrollLeft(target.index() * target.width());
                });
            };

            var onResize = function () {
                if ($('.tiny-slider .slider-wrapper').eq(0).width() > 0) {
                    _onResize();
                } else {
                    setTimeout(onResize, 50);
                }
            };

            $(window).resize(onResize);
            onResize();

            setInterval(function () { $('.tiny-slider .controls.next').click() }, cfg.tinysliderInterval);
        },

        initSlider: function (i, el) {
            var s = $(el),
                ul = s.children('ul');

            var gotoPrev = function (e) {
                    e.preventDefault();

                    var ul = $(this).siblings('.slider-wrapper').children();
                    animateTo(ul.children('.active').prev());
                },
                gotoNext = function (e) {
                    e.preventDefault();

                    var ul = $(this).siblings('.slider-wrapper').children();
                    animateTo(ul.children('.active').next());
                },
                goto = function (e) {
                    e.preventDefault();

                    var ul = $(this).parent().siblings('.slider-wrapper').children();
                    var index = parseInt($(this).html(), 10);
                    animateTo($(ul.children()[index]));
                },
                animateTo = function (target) {
                    var ul = target.parent(),
                        len = ul.children().length,
                        pages = ul.parent().siblings('.controls.pagination').children();

                    if (target.is('.clone'))
                        target = ul.children().eq(target.index() == 0 ? len - 2 : 1 );

                    pages.removeClass('active');
                    $(pages[ul.children().index(target) - 1]).addClass('active');

                    ul.parent().animate({ scrollLeft: target.index() * target.width() }, 350, 'easeOutQuart', function () {
                        target.siblings().removeClass('active');

                        target.addClass('active');
                    });
                };

            ul.wrap('<div class="slider-wrapper" style="width: 100%; overflow: hidden"></div>');

            ul.children(':last').clone().addClass('clone').insertBefore(ul.children(':first'));
            ul.children(':first').next().clone().removeClass('active').addClass('clone').insertAfter(ul.children(':last'));
            ul.parent().scrollLeft(ul.children('.active').position().left);

            s.children('.controls.pagination').append(function () {
                var pages = [];
                var len = ul.children().length - 2;
                for (var i = 0; i < len; i++) {
                    var className = (i === 0) ? ' class="active"' : '';
                    var page = $('<span' + className + '>' + ( i + 1 ) + '</span>');
                    pages.push(page.on('click', goto));
                };
                return pages;
            });

            s.children('.controls.back').click(gotoPrev);
            s.children('.controls.next').click(gotoNext);
        },

        initAccordions: function () {
            $('div.accordeon > h1').click(function (e) { e.preventDefault(); self.toggleAccordion($(this).parent()) });
            setInterval(self.checkHash, 250);
        },


        initAgeCheck: function () {
            if ($('div.popup-altersschranke').length > 0) {
                $('footer').remove();

                if (!navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
                    $('div.popup-altersschranke div.birthday input[type=text], div.popup-altersschranke div.birthday input[type=number]').keyup(function () {
                        if (this.value.length == this.maxLength) {
                        //console.log($(this).nextAll('input[type=text]').first());
                            // $(this).blur();
                            $(this).nextAll('input[type=text], input[type=number]').first().focus();
                        }
                    });
                }
            }
        },


        toggleAccordion: function (el, cb) {
            var cnt = el.children('div.acc-content');

            if (el.is('.busy')) return;
            el.toggleClass('busy active');

            cnt[el.is('.active') ? 'slideDown' : 'slideUp'](350, function () {
                el.removeClass('busy');
                cb && cb();
            });
        },

        trackEvent: function (description, path) {
            console && console.log('ms.trackEvent | ' + path + " | " + description);
            try {
                dataLayer.push({ 'event': description, 'vpv': path });
            } catch(err){ self.trackError(err) }
        },

        trackDownload: function (type, name) {
            console && console.log('ms.trackDownload | ' + type + " | " + name);
            try {
                dataLayer.push({ 'event': 'download', 'downloadtype': type, 'downloadname': name });
            } catch(err){ self.trackError(err) }
        },

        trackVideo: function (status) {
            console && console.log('ms.trackVideo | ' + status );
            try {
                dataLayer.push({ 'event': 'video', 'videostatus': '0' });
            } catch(err){ self.trackError(err) }
        },

        trackError: function(err){
            console && console.error('trackError |', err);
        },

        checkHash: function () {
            var newHash = window.location.hash.substr(1);

            if (newHash != lastHash) {
                var el = $('#' + newHash);

                if (el.is('.accordeon:not(.active)')) {
                    self.toggleAccordion(el, function () {
                        $('body, html').animate({ 'scrollTop': el.position().top}, 200);
                    });
                }

                lastHash = newHash;
            }
        },

        inMobileView: function () {
            return $('#nav-toggle').is(':visible');
        },

        initPrint: function() {
            $('.social-print').on('click', function() {
                var $target = $(this).closest('.print-wrapper');
                $target.printThis();
            });
        }
    }

}();

var $flexSliderFacts = $('.schon-gewusst-slider'),
    $flexSliderIngredients = $('.ingredients__slider');

$(document).ready(function() {

    ms.init();

    var navigation = responsiveNav("#nav", {
        insert: "before",
        openPos: "relative",
        animate: true,
        transition: 200,
        label: "<span>schließen</span>",
        // callback functions
        open: function(){
            $('#nav-toggle').addClass("opened");
        },
        close: function(){
            $('#nav-toggle').removeClass("opened");
        }
    });

    $('#nav > ul > li > a').on('click', function ( event ) {
        if ( window.innerWidth <= 768 ) {
            event.preventDefault();
            var $li = $(this).parent();
            if ($li.hasClass('active')) {
                $li.removeClass('active');
            }
            else {
                $('#nav > ul > li.active').removeClass('active');
                $li.addClass('active');
            }

            setTimeout(function() {
                $('html, body').animate({ scrollTop: 0 }, 250);
            }, 500);
        }
    });

    $( ".spielefinder .toggle-filter-menu" ).on( "click", function(event) {
        event.preventDefault();
        // hier die filter anzeigen
        if ($(".spielefinder .toggle-filter-menu").hasClass('open')) {
            $(this).removeClass('open');
            console.log('open');
        } else {
            $(this).addClass('open');
            console.log('close');
        }
        $('.spielefinder .filter-menu').slideToggle("fast");

    });

    var $diyItems = $( ".diy-item" );

    $diyItems.on( "click", function (e) {
        e.preventDefault();

        var index = $diyItems.index(this);
        var $instructions = $(".instruction");
        var $instruction = $($instructions[index]);

        if( $instruction.length ) {

            if( ! $instruction.hasClass("active") ) {
                dataLayer.push({
                // console.log({
                    'event' : 'instructionItem',
                    'instructionItemStatus' : 'click',
                    'instruction' : getInstructionName($instruction.attr('class'))
                });
            }

            $instructions.removeClass("active");
            $instruction.addClass("active");

            $diyItems.removeClass('active');
            $( this ).addClass('active');

            var offset = $instruction.offset();
            if( offset ) {
                $('html, body').animate({ scrollTop: offset.top }, 250);
            }

            var $step0 = $($instruction.find(".instruction__steps").children()[1]);
            $step0.children().click();

        }
    });

    $( ".instruction__content .cta-btn.next" ).on( "click", function (e) {
        e.preventDefault();

        var $instruction = $( this ).parents( ".instruction" );
        $instruction.find( ".page-link.next" ).click();
    });
    $( ".instruction__content .cta-btn.prev" ).on( "click", function (e) {
        e.preventDefault();

        var $instruction = $( this ).parents( ".instruction" );
        $instruction.find( ".page-link.prev" ).click();
    });

    var createPagination;
    ( createPagination = function(displayedPages) {
        var $steps = $(".instruction .instruction__steps");
        for (var i = 0; i < $steps.length; i++) {
            var $step = $($steps[i]);
            var length = parseInt($step.attr("number-of-steps"));
            var labelMap = [];

            for (var k = 0; k < length; k++) {
                labelMap[k] = k.toString();
            };

            (function ( $step ) {
                $step.pagination({
                    items : length,
                    displayedPages : displayedPages || length,
                    edges : 1,
                    labelMap : labelMap,
                    nextText : 'Weiter >',
                    onPageClick : function (pageNumber, e) {
                        e.preventDefault();

                        var target = $step.children()[pageNumber];
                        if( typeof target === "undefined" ) {
                            target = $step;
                        }
                        setStepActive(--pageNumber, $(target), $(e.target));
                    }
                })
            })($step)
        };
    })()

    var $window = $(window);
    var lastWidth = 0;
    $window.resize( resizeHandler );
    function resizeHandler () {
        var width = $window.width();

        if(Math.abs(lastWidth - width) < 50 ) {
            return;
        }

        lastWidth = width;
        var displayedPages;

        if( width > 480 && width < 767 ) {
            displayedPages = 5;
        } else if ( width < 480 ) {
            displayedPages = 3;
        }

        createPagination(displayedPages);
    }
    resizeHandler()

    function setStepActive(pageNumber, $element, eventTarget) {
        var $instruction = $element.parents(".instruction");

        var type = '';

        if (eventTarget.hasClass('next')) {
            type = 'next';
        } else if (eventTarget.hasClass('prev')) {
            type = 'previous';
        } else {
            type = 'page';
        }

        dataLayer.push({
        // console.log({
            'event' : 'instructionSlide',
            'instructionSlideStatus' : 'change',
            'instruction' : getInstructionName($instruction.attr('class')),
            'pageNumber' : pageNumber,
            'type' : type
        });

        if (pageNumber > 0 || isNaN(pageNumber)) {
            $instruction.children("img").addClass("hide");
        } else {
            $instruction.children("img").removeClass("hide");
        }
        var $step = $instruction.find(".instruction__content .step.active");
        var $go_step = $instruction.find(" .instruction__content .step.step" + pageNumber).addClass("active");
        $step.removeClass("active");
        $go_step.addClass("active");

        var offset = $instruction.offset();
        if (offset) {
            $('html, body').animate({ scrollTop: offset.top }, 250);
        }
    }

    var getInstructionName = function ( className ) {
        return className.replace('instruction', '')
                        .replace('instruction--', '')
                        .replace('active', '')
                        .trim()
    };

    $('#spielefinder-btn').on('click', function( event ) {
        event.preventDefault();

        dataLayer.push({
        // console.log({
            'event' : 'zumSpielefinder',
            'zumSpielefinderStatus' : 'click'
        });

        setTimeout(function () {
            location.href = '/mein-familienalltag/spielefinder';
        }, 100);
    });

    $('.instruction .download').on('click', function () {
        ms.trackDownload('document', $(this).attr('href'));
    });

    $('.product-teaser__arrow.left.first').on('click', function (event) {
        event.preventDefault();
        toggleFirstStageProductPage();
    });

    $('.product-teaser__arrow.right.first').on('click', function (event) {
        event.preventDefault();
        toggleFirstStageProductPage();
    });

    $('.product-teaser__arrow.left.zutaten').on('click', function (event) {
        event.preventDefault();
        swipeIngredients('left');
    });

    $('.product-teaser__arrow.right.zutaten').on('click', function (event) {
        event.preventDefault();
        swipeIngredients('right');
    });

    $('.product-teaser.first a').on('click', function (event) {
        event.preventDefault();
        toggleFirstStageProductPage();
    });

    $('.product-teaser.second a').on('click', function (event) {
        event.preventDefault();
        toggleFirstStageProductPage();
    });

    $('.schon-gewusst .next').on('click', function (event) {
        event.preventDefault();
        var flexSliderObject = $flexSliderFacts.data('flexslider');
        flexSliderObject.flexAnimate((flexSliderObject.currentSlide + 1) % flexSliderObject.pagingCount,true);
    });

    $('.product-teaser__ingredients img').on('click', function () {
        var $this = $(this);
        animateIngredients($this);
    });

    $('.product-teaser__ingredients img').on('mouseover', function () {
        var $this = $(this);
        toggleHoverButton('show', $this);
    });

    $('.product-teaser__ingredients img').on('mouseout', function () {
        var $this = $(this);
        toggleHoverButton('hide', $this);
    });

    $('.cta-btn-ingredient').on('mouseover', function () {
        toggleHoverButton('show');
    });

    $('.cta-btn-ingredient').on('mouseout', function () {
        toggleHoverButton('hide');
    });

    $('.cta-btn-ingredient a').on('click', function (event) {
        event.preventDefault();
    });

    $('.product-teaser__arrow.bottom').on('click', function (event) {
        event.preventDefault();
        $('html, body').animate({ scrollTop: ($('.' + $(this).attr('data-scrollTo')).offset().top)}, 1000);
    });

    $('.zutaten .btn-close a').on('click', function (event) {
        event.preventDefault();


        toggleSingleIngredient();
        resetIngredients();
    });


    $(window).on("orientationchange", function(event) {
        initCarousel();
    });

    var $swipeButton = $('.swipe__button-shadow');
    if ($swipeButton.length) {
        animateSwipeButton($swipeButton);
    }

    if ($flexSliderIngredients.length) {
        $flexSliderIngredients.flexslider({
            slideshow: false,
            animation: "slide",
            controlNav: false,
            directionNav: false,
            pauseOnHover: true,
            touch: true
        });
    }

    if ($('.contest-infoform').length) {
        $(".contest-infoform form").submit(function(event) {
            $('.contest-infoform button').attr('disabled',true);
        });
    }
});

var initFactsSlider = false;
/*
$(window).scroll(function() {
    if ($flexSliderFacts.length && !initFactsSlider) {
        $(".schon-gewusst-slider:in-viewport").each(function () {
            console.log('inviewport');
            $flexSliderFacts.flexslider("destroy");
            initFactsSlider = true;
            $flexSliderFacts.flexslider({
                slideshow: true,
                animation: "slide",
                controlNav: false,
                directionNav: false,
                pauseOnHover: true,
                slideshowSpeed: 5000,
                touch: true
            });
        });
    }
});
*/
$(window).scroll(function() {
    if ($flexSliderFacts.length && !initFactsSlider) {
        $(".schon-gewusst-slider:in-viewport").each(function () {
            var flexSliderObject = $flexSliderFacts.data('flexslider');
            initFactsSlider = true;
            flexSliderObject.flexAnimate(0,true);
            flexSliderObject.play();
        });
    }
});

$(window).load(function() {
    initCarousel();
});


function initCarousel() {
    if ($("#product-carousel").length) {

        $("#product-carousel").Cloud9Carousel( {
            autoPlay: false,
            bringToFront: true,
            speed: 3,
            farScale: 0.6,
            smooth: true,
            yRadius: 40,
            buttonLeft: $(".sortiment .button.left"),
            buttonRight: $(".sortiment .button.right"),
            onRendered: function( carousel ) {
                $('#carousel-caption').text(carousel.items[carousel.nearestIndex()].element.alt);
            }
        });
    }

    if ($flexSliderFacts.length) {
        $flexSliderFacts.flexslider({
            slideshow: true,
            animation: "slide",
            controlNav: false,
            directionNav: false,
            pauseOnHover: true,
            slideshowSpeed: 5000,
            touch: true
        });
    }
}

function animateSwipeButton($obj) {
    var startPosition = -150,
        stopPosition = $('.swipe__button').outerWidth() + 50,
        speed = 2500;

    $obj.animate({
        right: stopPosition
    }, speed, function() {})
        .promise()
        .done(
            function () {
                $obj.css('right', startPosition + 'px');
                animateSwipeButton($obj);
            }
        );
}

function toggleFirstStageProductPage() {

    var $arrowLeft = $('.product .mainteaser .product-teaser__arrow.left'),
        $arrowRight = $('.product .mainteaser .product-teaser__arrow.right'),
        $secondStage = $('.product .mainteaser .second');

    $secondStage.fadeToggle(600);

    $arrowLeft.fadeToggle();
    $arrowRight.fadeToggle();

    $('.mainteaser .flying-element').fadeToggle();
}

var ingredients = {
    honey: {
        position: {
            x: 160,
            y: 160,
            w: 270,
            i: 5
        },
        slideDirection: 'right'
    },
    sugar: {
        position: {
            x: 80,
            y: 230,
            w: 400,
            i: 3


        },
        slideDirection: 'right'
    },
    oil: {
        position: {
            x: 220,
            y: 70,
            w: 160,
            i: 4
        },
        slideDirection: 'right'
    },
    milk: {
        position: {
            x: 100,
            y: 60,
            w: 280,
            i: 2
        },
        slideDirection: 'left'
    },
    powder: {
        position: {
            x: 100,
            y: 200,
            w: 360,
            i: 0
        },
        slideDirection: 'left'
    },
    egg: {
        position: {
            x: 210,
            y: 250,
            w: 180,
            i: 1

        },
        slideDirection: 'left'
    }
},
singleIngredientStatus = false,
currentActiveIngredient = 0;


function swipeIngredients(direction) {

    $('[data-type]').each(function() {
        var $thisObj = $(this);
        if (ingredients[$thisObj.data("type")].position.i == currentActiveIngredient) {

            var newPosition;

            if (direction == "right") {
                newPosition = -500;
            } else {
                newPosition = 1000;
            }

            $thisObj.animate({
                left: newPosition
            }, 300);

            if (direction == "right") {
                currentActiveIngredient++;
                if (currentActiveIngredient > 5) {
                    currentActiveIngredient = 0;
                }
            } else {
                currentActiveIngredient--;
                if (currentActiveIngredient < 0) {
                    currentActiveIngredient = 5;
                }
            }

            $('[data-type]').each(function() {
                var $thisObj = $(this),
                    $ingredientsDetailContainer = $('.zutaten__detail');

                if (ingredients[$thisObj.data("type")].position.i == currentActiveIngredient) {

                    $ingredientsDetailContainer.fadeOut()
                        .promise().done(function() {
                        $('.detail__headline').html($('*[data-text="' + $thisObj.data('type') + '"] > h2').html());
                        $('.detail__text').html($('*[data-text="' + $thisObj.data('type') + '"] > p').html());

                        if (direction == "right") {
                            $thisObj.css('left', 1000);
                        } else {
                            $thisObj.css('left', -500);
                        }

                        $thisObj.css('top', ingredients[$thisObj.data("type")].position.y);
                        $thisObj.css('width', ingredients[$thisObj.data("type")].position.w);

                        $thisObj.animate({
                            left: ingredients[$thisObj.data("type")].position.x,

                        }, 300, function() {
                            $ingredientsDetailContainer.fadeIn();
                        });


                    });
                    return false;
                }
            });

            return false;
        }
    });


}

function resetIngredients() {
    $('[data-type]').each(function() {
        var $thisObj = $(this);
        $thisObj.animate({
            left: $thisObj.data('left'),
            top: $thisObj.data('top'),
            width: $thisObj.data('width')
        }, 1000, function () {
            $('.zutaten .flying-element').fadeIn();

        });
    });
    singleIngredientStatus = false;
}

function toggleSingleIngredient() {
    $('.zutaten .product-teaser__content-wrapper.zutaten__detail').fadeToggle();
    $('.zutaten .product-teaser__content-wrapper.headline').fadeToggle();
    $('.zutaten .btn-close').fadeToggle();

    $('.product .product-teaser__arrow.zutaten.left').fadeToggle();
    $('.product .product-teaser__arrow.zutaten.right').fadeToggle();



    singleIngredientStatus = true;
}

function animateIngredients(obj) {
    var $obj = $(obj),
        type = $obj.data('type'),
        newPositionLeft = 0,
        newPositionTop = 0,
        newWidth = 0,
        currentPositionLeft = Math.round($obj.position().left),
        currentPositionTop = Math.round($obj.position().top),
        currentWidth = Math.round($obj.width());

    toggleSingleIngredient();

    if (currentPositionLeft == ingredients[type].position.x) {
        newPositionLeft = $obj.data('left');
        newPositionTop = $obj.data('top');
        newWidth = $obj.data('width');

        resetIngredients();
    } else {

        $('.zutaten .flying-element').fadeOut();

        $obj.data('left',currentPositionLeft);
        $obj.data('top',currentPositionTop);
        $obj.data('width',currentWidth);

        newPositionLeft = ingredients[type].position.x;
        newPositionTop = ingredients[type].position.y;
        newWidth = ingredients[type].position.w;

        currentActiveIngredient = ingredients[type].position.i;


        $('.detail__headline').html($('*[data-text="' + type + '"] > h2').html());
        $('.detail__text').html($('*[data-text="' + type + '"] > p').html());
        $('.cta-btn-ingredient').hide();
    }

    $obj.animate({
        left: newPositionLeft,
        top: newPositionTop,
        width: newWidth
    }, 600);



    if (currentPositionLeft != ingredients[type].position.x) {
        $('[data-type]').not($obj).each(function() {
            var $thisObj = $(this),
                currentPositionLeft = Math.round($thisObj.position().left),
                currentPositionTop = Math.round($thisObj.position().top),
                currentWidth = Math.round($thisObj.width());

            $thisObj.data('left', currentPositionLeft);
            $thisObj.data('top', currentPositionTop);
            $thisObj.data('width', currentWidth);


            if (ingredients[$thisObj.data('type')].slideDirection == 'left') {
                newPositionLeft = -300;
            } else {
                newPositionLeft = 1000;
            }

            $thisObj.animate({
                left: newPositionLeft,
                top: currentPositionTop,
                width: currentWidth
            }, 1000);
        });
    }
}

function toggleHoverButton(state, $obj) {
    if (singleIngredientStatus == false) {
        var $hoverButton = $('.product-teaser__ingredients .cta-btn-ingredient');


        if (typeof $obj === 'undefined') {
            return;
        }

        $hoverButton.find('a').text($('*[data-text="' + $obj.data('type') + '"] > h2').html());
        $hoverButton.css('left', Math.round($obj.position().left + ($obj.width() / 2) - ($hoverButton.width() / 2)));
        $hoverButton.css('top', Math.round($obj.position().top + ($obj.height() / 2) - ($hoverButton.width() / 2)));

        if (state == 'show') {
            $hoverButton.show();
        } else {


            $hoverButton.hide();
        }
    }
}

!function(t){function e(){var e,i,n={height:a.innerHeight,width:a.innerWidth};return n.height||(e=r.compatMode,(e||!t.support.boxModel)&&(i="CSS1Compat"===e?f:r.body,n={height:i.clientHeight,width:i.clientWidth})),n}function i(){return{top:a.pageYOffset||f.scrollTop||r.body.scrollTop,left:a.pageXOffset||f.scrollLeft||r.body.scrollLeft}}function n(){var n,l=t(),r=0;if(t.each(d,function(t,e){var i=e.data.selector,n=e.$element;l=l.add(i?n.find(i):n)}),n=l.length)for(o=o||e(),h=h||i();n>r;r++)if(t.contains(f,l[r])){var a,c,p,s=t(l[r]),u={height:s.height(),width:s.width()},g=s.offset(),v=s.data("inview");if(!h||!o)return;g.top+u.height>h.top&&g.top<h.top+o.height&&g.left+u.width>h.left&&g.left<h.left+o.width?(a=h.left>g.left?"right":h.left+o.width<g.left+u.width?"left":"both",c=h.top>g.top?"bottom":h.top+o.height<g.top+u.height?"top":"both",p=a+"-"+c,v&&v===p||s.data("inview",p).trigger("inview",[!0,a,c])):v&&s.data("inview",!1).trigger("inview",[!1])}}var o,h,l,d={},r=document,a=window,f=r.documentElement,c=t.expando;t.event.special.inview={add:function(e){d[e.guid+"-"+this[c]]={data:e,$element:t(this)},l||t.isEmptyObject(d)||(l=setInterval(n,250))},remove:function(e){try{delete d[e.guid+"-"+this[c]]}catch(i){}t.isEmptyObject(d)&&(clearInterval(l),l=null)}},t(a).bind("scroll resize scrollstop",function(){o=h=null}),!f.addEventListener&&f.attachEvent&&f.attachEvent("onfocusin",function(){h=null})}(jQuery);