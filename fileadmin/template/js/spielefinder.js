var spiel = GetURLParameter('spiel');

function pageLoad(sender, args) {
    //debugger;
    // console.log("hf: " + $('#hf_spielId').val());
    if ($('#hf_spielId').val() != "" || spiel != undefined) {
        renderFancy();
    }
    $('#like').text($('#lit_like').text());
    //console.log("like" + $('#like').text());
}

function share_track(network,spiel) {
    var spiel_url = window.location + '?spiel=' + spiel
    dataLayer.push({'event':'social',' network': network,' socialAction': 'Share',' opt_target': spiel_url});
}

function showSpiel(spielslug) {
    $('#hf_spielId').val(spielslug);
    __doPostBack('up_spiel', '');
}

function triggerLike() {
    // console.log("hf: " + $('#hf_spielId').val());
    $('#btn_like').trigger('click');
    $('#triggerLink').attr("disabled", "true");
}

function renderFancy() {
    $.fancybox.open("#up_spiel", {
        padding: 0,
        fitToView: true,
        width: '960',
        height: '600',
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        scrolling: 'no',
        afterShow: function () {
             dataLayer.push({'event':'spielefinder','spielname': $('#up_spiel #div_fancyTitle').text()});
             $('#lnk_twitter').attr("onclick", 'share_track("Twitter", "' + $('#hf_spielId').val() + '");');
             $('#lnk_facebook').attr("onclick", 'share_track("Facebook", "' + $('#hf_spielId').val() + '");');
             $('#lnk_google').attr("onclick", 'share_track("Google", "' + $('#hf_spielId').val() + '");');
             $('#lnk_pinterest').attr("onclick", 'share_track("Pinterest", "' + $('#hf_spielId').val() + '");');
        },
        afterClose: function () {
            $('#hf_spielId').removeAttr('value');
        }
    });
}


var thisHash = window.location.hash;
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

$(document).ready(function () {

    var kind_alter = '-';
    var anzahl_spieler = '-';
    var spiel_ort = '-';
    var spiel_material = '-';

    var filterAble;
    setFilterButtonState(false);

    function setFilterButtonState(bool) {
        filterAble = bool;
        if (filterAble) {
            $(".clear-filter").css({
                'opacity': '1',
                'cursor': 'pointer'
            });
        } else {
            $(".clear-filter").css({
                'opacity': '0.5',
                'cursor': 'not-allowed'
            });
        }
    }

    $("#select_kindesalter").selectbox({
        onChange: function (val, inst) {
            $(this).find('option').removeAttr("selected");
            if (val === '-') {
                $("#select_kindesalter").find('option').removeAttr("selected");
                $("#select_kindesalter option:eq(0)").attr("selected", "selected");
                $("#select_kindesalter").parent().find('a.sbSelector').text('Kindesalter');
            } else {
                $("#select_kindesalter option[value='" + val + "']").attr("selected", "selected");
                setFilterButtonState(true);
            }
            kind_alter = val.toString();
            $(this).val(val);
            initSelectFilter();
        }
    });

    $("#select_spieleranzahl").selectbox({
        onChange: function (val, inst) {
            $(this).find('option').removeAttr("selected");
            if (val === '-') {
                $("#select_spieleranzahl").find('option').removeAttr("selected");
                $("#select_spieleranzahl option:eq(0)").attr("selected", "selected");
                $("#select_spieleranzahl").parent().find('a.sbSelector').text('Spieleranzahl');
            } else {
                $("#select_spieleranzahl option[value='" + val + "']").attr("selected", "selected");
                setFilterButtonState(true);
            }
            anzahl_spieler = val.toString();
            $(this).val(val);

            initSelectFilter();
        }
    });

    $("#select_spielerort").selectbox({
        onChange: function (val, inst) {
            $(this).find('option').removeAttr("selected");
            if (val === '-') {
                $("#select_spielerort").find('option').removeAttr("selected");
                $("#select_spielerort option:eq(0)").attr("selected", "selected");
                $("#select_spielerort").parent().find('a.sbSelector').text('Spiel-Ort');
            } else {
                $("#select_spielerort option[value='" + val + "']").attr("selected", "selected");
                setFilterButtonState(true);
            }
            spiel_ort = val.toString();
            $(this).val(val);

            initSelectFilter();
        }
    });

    $("#select_spielmaterial").selectbox({
        onChange: function (val, inst) {
            $(this).find('option').removeAttr("selected");

            if (val === '-') {
                $("#select_spielmaterial").find('option').removeAttr("selected");
                $("#select_spielmaterial option:eq(0)").attr("selected", "selected");
                $("#select_spielmaterial").parent().find('a.sbSelector').text('Spielmaterial');
            } else {
                $("#select_spielmaterial option[value='" + val + "']").attr("selected", "selected");
                setFilterButtonState(true);
            }
            spiel_material = val.toString();
            $(this).val(val);
            initSelectFilter();
        }
    });

    function initSelectFilter() {
        $('.top10').css({
            'opacity': '1',
            'cursor': 'pointer'
        });
        isSortedTop10 = 0;
        if (kind_alter == '-' && anzahl_spieler == '-' && spiel_ort == '-' && spiel_material == '-') {
            showAllItems();
            setFilterButtonState(false);
        }
        else {
            setFilterButtonState(true);
            showAllItems();
            $('.spiele-item').each(function () {
                var kindesAlterData = $(this).data('kindesalter').toString();
                var spielerAnzahlData = $(this).data('spieleranzahl').toString();
                var spielOrtData = $(this).data('spielort').toString();
                var spielMaterialData = $(this).data('spielmaterial').toString();

                var tst = kindesAlterData.indexOf(kind_alter);
                //console.log(kindesAlterData);
                if (kind_alter != '-' && kindesAlterData.indexOf(kind_alter) == -1)
                    $(this).css('display', 'none');

                if (anzahl_spieler != '-' && spielerAnzahlData.indexOf(anzahl_spieler) == -1)
                    $(this).css('display', 'none');

                if (spiel_ort != '-' && spielOrtData.indexOf(spiel_ort) == -1)
                    $(this).css('display', 'none');

                if (spiel_material != '-' && spielMaterialData.indexOf(spiel_material) == -1)
                    $(this).css('display', 'none');
            });
        }
    }
    initSelectFilter();

    $('.clear-filter').on("click", function (event) {
        event.preventDefault();

        if (filterAble) {
            $("#select_kindesalter").find('option').removeAttr("selected");
            $("#select_kindesalter option:eq(0)").attr("selected", "selected");
            $("#select_kindesalter").selectbox("change", '-', 'Kinderanzahl');

            $("#select_spieleranzahl").find('option').removeAttr("selected");
            $("#select_spieleranzahl option:eq(0)").attr("selected", "selected");
            $("#select_spieleranzahl").selectbox("change", '-', 'Spieleranzahl');

            $("#select_spielerort").find('option').removeAttr("selected");
            $("#select_spielerort option:eq(0)").attr("selected", "selected");
            $("#select_spielerort").selectbox("change", '-', 'Spiel-Ort');

            $("#select_spielmaterial").find('option').removeAttr("selected");
            $("#select_spielmaterial option:eq(0)").attr("selected", "selected");
            $("#select_spielmaterial").selectbox("change", '-', 'Spielmaterial');

            $(".selectfilter").selectbox("attach");

            $(".clear-filter").css('opacity', '0.5');

            $('.top10').css('opacity', '1');
            isSortedTop10 = 0;
            sortById();

            initSelectFilter();
            showAllItems();
        }

    });

    var isSortedTop10 = 0;
    $('.top10').on('click', function(event) {
        event.preventDefault();

        if (isSortedTop10 == 0) {
            var $wrapper = $('.spiele-inner');

            $wrapper.find('.spiele-item').sort(function (a, b) {
                return ($(a).data('likes')) < ($(b).data('likes')) ? 1 : -1;
            })
            .appendTo($wrapper);
            $('.spiele-inner .spiele-item').hide();
            $('.spiele-inner .spiele-item:lt(10)').show();
            $wrapper.find('.spiele-item[data-likes="0"]').hide();
            $('.top10').css({
                'opacity': '0.5',
                'cursor': 'not-allowed'
            });
            setFilterButtonState(true);
            isSortedTop10 = 1;
        }
    });

    function showAllItems() {
        $('.spiele-item').each(function () {
            //(this).css('display', 'inline-block');
            $(this).css('display', 'inline-block').hide().fadeIn(500);
        });
    }

    $('.mobile_scroll_top_nav').on("click", function () {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
    });




    var mywindow = $(window);
    var mypos = mywindow.scrollTop();
    var up = false;
    var newscroll;
    mywindow.scroll(function () {
        newscroll = mywindow.scrollTop();
        if (newscroll > mypos && !up) {
            $('.mobile_scroll_top_nav').css('opacity', '0');
            up = !up;
        } else if (newscroll < mypos && up) {
            //$('.mobile_scroll_top_nav').css('opacity', '1');
            // console.log(!$("#push").hasClass('inview'));
            if (!$("#push").hasClass('inview')) {
                // console.log('einblenden');
                $('.mobile_scroll_top_nav').css('opacity', '1');
            };
            up = !up;
        }
        mypos = newscroll;
    });


    shuffleSort();

    $('#push').bind('inview', function (event, visible) {
        if (visible) {
            $(this).addClass('inview');

        } else {

            $(this).removeClass('inview');
            $('.mobile_scroll_top_nav').css('opacity', '1');
        }
    });

});

function sortById() {
    var $wrapper = $('.spiele-inner');

    $wrapper.find('.spiele-item').sort(function (a, b) {
        return a.getAttribute('data-spiel-id') - b.getAttribute('data-spiel-id');
    })
    .appendTo($wrapper);
}

function shuffleSort() {
    var $container = $(".spiele-inner");
    $container.html(shuffle($container.children().get()));
}

function shuffle(o) {
    for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

function wallpost(link, pic, caption) {
    FB.ui({
        method: 'feed',
        name: 'Spielefinder',
        link: link,
        picture: pic,
        description: 'Entdecke mit dem Milch-Schnitte Spielefinder 50 tolle Spielideen für jeden Anlass!'
    });
}