/**
 * MyBreakContest Initializers
 * Milschnitte | My Break Photo Contest
 *
 * Copyright brandung GmbH & Co.KG
 * http://www.brandung.de/
 *
 * Date: 02.02.2016
 * MIT License (MIT)
 *
 * Author: Felix Leupold (xiel.de)
 */

;(function ( $, window, document, undefined ) {
	"use strict";

	//get/create global objects
	var Brandung = window.Brandung || (window.Brandung = {});
	Brandung.Component = Brandung.Component || {};

	var ms = window.ms || (window.ms = {});
	var globalMyBreakContestCfg = ms.MyBreakContestCfg || (ms.MyBreakContestCfg = {});
	var location = window.location;

	// feature detection
	var historyAPI = detectHistoryAPI() && window.history;

	//name for the component
	var componentName = "MyBreakContest";

	//option defaults
	var defaults = {
		selectors: {
			detailViewInLightboxOpeners: '.contest-gallery-item a, a.contest-slider__slide-inner'
		},
		fancybox: {
			padding: 0,
			type: 'ajax',
			nextEffect: 'none',
			prevEffect: 'none',
			tpl: {
				wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
				image    : '<img class="fancybox-image" src="{href}" alt="" />',
				error    : '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
				closeBtn : '<a class="close-btn lightbox-close-btn" title="Close" href="javascript:;"></a>',
				next     : '<a class="lightbox-dir-btn lightbox-dir-btn--right" title="Next" href="javascript:;"><span class="arrow arrow--big"></span></a>',
				prev     : '<a class="lightbox-dir-btn lightbox-dir-btn--left" title="Previous" href="javascript:;"><span class="arrow arrow--big arrow--left"></span></a>'
			},
			loop: true,
			ajax: {
				data: {
					ajax: true
				}
			}
		},
		initialOpenLightbox: {
			href: false
		}
	}

	// component constructor
	function Component( options ) {
		//extend defaults with given options and global cfg
		this.options = $.extend( true, {}, defaults, options, globalMyBreakContestCfg ) ;

		this._defaults = defaults;
		this._name = componentName;
		this.init();
	}

	$.extend(Component.prototype, {
		init: function(){
			var self = this;
			var o = this.options;

			//properties
			self.lightboxAPI = $.fancybox;
			self.lightboxEventCallbacks = null; //set in bindEvents
			self.hrefAfterLightboxClose = '';
			self.initalLocationHref = location.href;

			//initial calls
			self.bindEvents();
			self.bindLinksToDetailView();
			self.openLightboxInitially();
		},
		bindEvents: function(){
			var self = this;
			var o = this.options;

			//bind popstate (back/fwd browser buttons)
			if(historyAPI){
				$(window).on('popstate', function(e){
					var newState = historyAPI.state || {};
					var newHref = location.href;

					//if lightbox is open and url equals to hrefAfterLightboxClose -> close the lightbox
					if(self.lightboxAPI.current != null && self.hrefAfterLightboxClose === newHref) {
						return self.lightboxAPI.close();
					}

					//if state is shown in lightbox
					if(newState.inLightbox) {
						self.hrefAfterLightboxClose = self.initalLocationHref;
						return self.openLightbox(newHref);
					}

					//fallback to default behaviour (check against inital href to ignore load popstate in Safari)
					if(newHref != self.initalLocationHref){
						location.href = newHref;
					}
				});
			}

			var newItemsAddedToFancyboxGroup = false;

			self.lightboxEventCallbacks = {
				beforeLoad: function(){

					if(this.href === '#'){ return false }
					if ($('.contest-gallery-listing').data('plugin_infiniteScroll')) {

						if (!newItemsAddedToFancyboxGroup) {
							var newItems = $('.contest-gallery-listing').data('plugin_infiniteScroll').newItems;

							for (var i = 0; i < newItems.length; i++) {

								this.group.push({
									href: newItems[i].href,
									isDom: true,
									title: '',
									type: 'ajax'
								});
							}

							newItemsAddedToFancyboxGroup = true;
						}
					}
				},
				afterShow: function(){
					//update history with url of lightbox content

					//if there is no url to open yet after closing, set it to current location
					if(!self.hrefAfterLightboxClose) {
						self.hrefAfterLightboxClose = location.href;
					}

					if(historyAPI && this.href) {
						var lightboxHref = self.getAbsoluteHref(this.href);

						//replace state when location is already on wanted href (eg. directly opened detail view) otherwise push it
						historyAPI[lightboxHref === location.href ? 'replaceState' : 'pushState']({
							inLightbox: true
						}, '', this.href);
					}

					//track lightbox opening
					if('trackEvent' in ms) {
						ms.trackEvent('sendVirtualPageview', this.href);
					}

					var titleForSocialShare = $('.contest-detailview__headline em').text() + ' Pause mit Milchschnitte:';

					$('meta[property="og:title"]').attr('content', titleForSocialShare);
					$('meta[property="og:url"]').attr('content', location.href);
					$('meta[property="og:image"]').attr('content', $('.contest-composed-image__inner img').attr('src'));
					$('meta[property="og:description"]').attr('content', 'Meine Pause mit Milch-Schnitte - ' + $(".contest-detailview__headline em").text() + ' Pause mit Milch-Schnitte.');


					// whatsapp take title from title tag
					$('title').text( titleForSocialShare );

					window.shariff && shariff.init($('.contest-detailview .shariff'));
				},
				afterClose: function(){
					//push new state when the lighbox was closed by user
					if(historyAPI && self.hrefAfterLightboxClose && self.hrefAfterLightboxClose !== location.href) {
						historyAPI.pushState({}, '', self.hrefAfterLightboxClose);
					}

					// reset since when fancybox is opened again a new group is created containing only visible elements which means the dynamically loaded elements are missing
					newItemsAddedToFancyboxGroup = false;
				}
			};
		},
		//init fancybox for detail view
		bindLinksToDetailView: function(_context){
			var self = this;
			var o = this.options;
			var context = _context || document;

			$(o.selectors.detailViewInLightboxOpeners, context).fancybox($.extend(true, {}, o.fancybox, self.lightboxEventCallbacks));
		},
		openLightboxInitially: function(){
			var self = this;
			var o = this.options;
			var openLightbox = o.initialOpenLightbox && o.initialOpenLightbox.href;

			if(!openLightbox) {
				return
			}

			//open lightbox with options given
			self.lightboxAPI.open([
				$.extend(true, {}, o.fancybox, o.initialOpenLightbox, self.lightboxEventCallbacks)
			]);

			//set url to show, after initial lightbox gets closed
			if(o.initialOpenLightbox.hrefAfterClose) {
				self.hrefAfterLightboxClose = self.getAbsoluteHref(o.initialOpenLightbox.hrefAfterClose);
			} else {
				console.warn('MyBreakContest | initialOpenLightbox, no hrefAfterClose was given');
			}
		},
		//open a lightbox with string OR options as parameter
		openLightbox: function(optionsOrString){
			var self = this;
			var o = this.options;

			if(!optionsOrString){
				return console.error('MyBreakContest | openLightbox() missing options or href');
			}

			var openOptions = typeof optionsOrString === 'string' ? {href: optionsOrString} : optionsOrString;

			return self.lightboxAPI.open([
				$.extend(true, {}, o.fancybox, openOptions, self.lightboxEventCallbacks)
			]);
		},
		//get an interpreted absolute href for the document
		getAbsoluteHref: function(relativeOrAbsoluteHref){
			var tempLink = document.createElement('a');
			tempLink.href = relativeOrAbsoluteHref;
			return tempLink.href;
		}
	});

	//make Component available in Brandung namespace
	Brandung.Component[componentName] = function ( options ) {
		return new Component( options );
	}

	if(window.Modernizr) {
		Modernizr.addTest('history', detectHistoryAPI);
	}

	// https://github.com/Modernizr/Modernizr/blob/master/feature-detects/history.js
	function detectHistoryAPI() {
		// Issue #733
		// The stock browser on Android 2.2 & 2.3, and 4.0.x returns positive on history support
		// Unfortunately support is really buggy and there is no clean way to detect
		// these bugs, so we fall back to a user agent sniff :(
		var ua = navigator.userAgent;

		// We only want Android 2 and 4.0, stock browser, and not Chrome which identifies
		// itself as 'Mobile Safari' as well, nor Windows Phone (issue #1471).
		if ((ua.indexOf('Android 2.') !== -1 ||
			(ua.indexOf('Android 4.0') !== -1)) &&
			ua.indexOf('Mobile Safari') !== -1 &&
			ua.indexOf('Chrome') === -1 &&
			ua.indexOf('Windows Phone') === -1) {
		  return false;
		}

		// Return the regular check
		return (window.history && 'pushState' in window.history);
	}

})( jQuery, window, document );

// auto-init component once
;(function ( $, window, document, undefined ) {
	"use strict";

	var Brandung = window.Brandung || (window.Brandung = {});
	Brandung.Component = Brandung.Component || {};

	//auto init once
	$(document).ready(function(){
		var context = document;

		var myBreakContestInstance = Brandung.Component.MyBreakContest();
		var defaults = myBreakContestInstance._defaults;

		initPrizesLightbox(defaults.fancybox);
		initFlexSlider('.contest-slider');
		initCountdown('.remaining-btn-extension');
		initDependentCheckboxes('.contest-infoform form');
	});

	var initDependentCheckboxes = function(formSelector){
		var form = $(formSelector);
		var checkboxes = $('input[type="checkbox"][data-required-dependent]');

		checkDependencies();
		checkboxes.on('change', checkDependencies);

		function checkDependencies() {
			checkboxes.each(function(){
				var checkbox = $(this);
				var dependentFromCheckbox = checkboxes.filter('[id="'+ checkbox.data('requiredDependent') +'"]');
				checkbox.prop('required', dependentFromCheckbox.prop('checked') );
			});
		}
	};

	//init inline content lightbox for prizes
	var initPrizesLightbox = function(_options, _context){
		var options = _options || {};
		var context = _context || document;
		var openers = $('.prize-opener', context);
		var groupOpeners = $('.prize-group-opener', context);

		openers.fancybox($.extend(true, {}, options, { type: 'inline' }));

		groupOpeners.on('click', function(e){
			var groupOpener = $(this);
			var opener = openers.filter(function(){
				return groupOpener.attr('href') === $(this).attr('href')
			}).first();

			opener.trigger('click');
			e.stopPropagation();
			e.preventDefault();
		});
	}

	var initCountdown = function(selector){
		$(selector).each(function(){
			var countdownEl = $(this);
			var daysEl = $('span', countdownEl).first();
			var labelSingle = countdownEl.data('label');
			var labelMultiple = countdownEl.data('labelMultiple');
			var endDate = countdownEl.data('countdownEnd');

			//calc the remaining days from the data attrs
			var timeDiff = (new Date(endDate) - new Date());
			var remainingDays = Math.max(0, Math.ceil(timeDiff / (60 * 60 * 24 * 1000)));
			var label = remainingDays === 1 ? labelSingle : labelMultiple;

			//update the element
			daysEl.text(remainingDays + ' ' + label);
		});
	};

	//contest/featured slider with variable item count visible
	var initFlexSlider = function(selector){
		var $window = $(window);

		$(selector).flexslider({
			animation: "slide",
			animationLoop: false,
			itemWidth: true,
			itemMargin: 5,
			slideshow: false,
			move: 1,
			directionNav: false,
			controlNav: false,
			minItems: getGridSize(), // use function to pull in initial value
			maxItems: getGridSize(), // use function to pull in initial value
			customDirectionNav: $(".custom-navigation a"),
			start: function(slider) { // Fires when the slider loads the first slide
				$(slider)
					.find('img.lazy:eq(0)')
					.each(function() {
						var src = $(this).attr('data-src');
						$(this).attr('src', src).removeAttr('data-src');
					});
			},
			before: function(slider) { // Fires asynchronously with each slider animation
				var slides     = slider.slides,
					index      = slider.animatingTo,
					$slide     = $(slides[index]),
					current    = index,
					nxt_slide  = current + 3,
					prev_slide = current - 3;

				$slide
					.parent()
					.find('img.lazy:eq(' + current + '), img.lazy:eq(' + prev_slide + '), img.lazy:eq(' + nxt_slide + ')')
					.each(function() {
						var src = $(this).attr('data-src');
						$(this).attr('src', src).removeAttr('data-src');
					});
			}
		}).each(function() {
			var sliderElement = $(this);
			var flexsliderInstance = sliderElement.data('flexslider');

			// check grid size on resize event
			$window.resize(function() {
				var gridSize = getGridSize();
				flexsliderInstance.vars.minItems = gridSize;
				flexsliderInstance.vars.maxItems = gridSize;
			});

			sliderElement.on('click', '.dirnav-btn--prev', function(e){
				sliderElement.flexslider("prev");
				e.preventDefault();
			});

			sliderElement.on('click', '.dirnav-btn--next', function(e){
				sliderElement.flexslider("next");
				e.preventDefault();
			});
		});

		// tiny helper function to add breakpoints
		function getGridSize() {
			var gridSize = 3;
			if(window.innerWidth < 980) {
				gridSize = 2;
			}
			if (window.innerWidth < 500) {
				gridSize = 1;
			}
			return gridSize;
		}
	};

})( jQuery, window, document );