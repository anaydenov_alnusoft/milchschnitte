// Feedback
// ************************************************************
// ************************************************************
// ************************************************************
function finderFeedback() {
    feedbackTemplate = '<div id="fb-inselfinder" class="overlay-feedback">'
                        + '<div class="feedback-box">'
                        + '<div class="header">'
                        + '<div class="logo">Milch-Schnitte</div>'
                        + '<h1>Feedback zum Inselfinder</h1>'
                        + '</div>'
                        + '<div class="inner">'
                        + '<div id="start-feedback">'
                        + '<h3>Uns interessiert deine Meinung!</h3>'
                        + '<p>Möchtest du uns Feedback auf dieses Tool geben?</p>'
                        + '<p class="btn-line">'
                        + '<a class="cta-btn no">Nein<span class="arrow"></span></a><a class="cta-btn yes">Ja<span class="arrow"></span></a>'
                        + '</p>'
                        + '</div>'
                        + '<div id="rate-feedback">'
                        + '<form id="form-send-inselfinder-feedback">'
                        + '<h3>Wie hilfreich war das Zeitmanagementtool?</h3>'
                        + '<p>Gib hier deine Bewertung ab:</p>'
                        + '<p id="rate-finder" class="rating">'
                        + '<span data-id="5">★</span>'
                        + '<span data-id="4">★</span>'
                        + '<span data-id="3">★</span>'
                        + '<span data-id="2">★</span>'
                        + '<span data-id="1">★</span>'
                        + '</p>'
                        + '<p>'
                        + 'Möchtest du uns noch etwas mitteilen oder eine Anregung geben?'
                        + '<textarea id="feedback-comment" rows="5" name="text-feedback"></textarea>'
                        + '</p>'
                        + '<p class="btn-line">'
                        + '<input id="send-inselfinder-feedback" type="submit" name="submit" class="submit cta-btn" value="Absenden">'
                        + '<span class="status send-feedback">Daten werden übermittelt.</span>'
                        + '</p>'
                        + '</form>'
                        + '</div>'
                        + '<div id="end-feedback">'
                        + '<h3>Vielen Dank für dein Feedback!</h3>'
                        + '<a class="btn btn-secondary close">Fenster schließen<span class="arrow"></span></a>'
                        + '</div>'
                        + '<div id="error-feedback">'
                        + '<h3 red>Es ist ein Fehler aufgetreten. Bitten probieren Sie es zu einem späterem Zeitpunkt noch einmal.</h3>'
                        + '<a class="btn btn-secondary close">Fenster schließen<span class="arrow"></span></a>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + '</div>';






    setTimeout(function () {
        cookieDays = 365;


        $('body.inselfinder').prepend(feedbackTemplate);
        $('#rate-feedback, #end-feedback, #error-feedback, .status.send-feedback').hide();


        $('#start-feedback .no, .btn.close').click(function () {
            $('#fb-inselfinder').fadeOut();
            $.cookie('feedback_inselfinder', 'true', { expires: 7 });

            setTimeout(function () {
                $('#fb-inselfinder').remove();
            }, 1000);
        });

        $('#fb-inselfinder').click(function () {
            $('#fb-inselfinder').fadeOut();
            setTimeout(function () {
                $('#fb-inselfinder').remove();
            }, 1000);
        });

        $('.feedback-box').click(function (event) {
            event.stopPropagation();
        });

        $('#start-feedback .yes').click(function (e) {
            e.preventDefault();
            $('#start-feedback').hide();
            $('#rate-feedback').fadeIn();
        });

        $('#rate-finder span').click(function () {
            $('#rate-finder').find('span').removeClass('active');
            var num = $(this).data('id');

            for (var i = 1; i <= num; i++) {
                $('#rate-finder span[data-id=' + i + ']').addClass('active');
            }
        });

        function btnFormFeedbackOk_Click(form) {
            $('.status.send-feedback').show();
            var choice = 0;

            if ($('#rate-finder').find('span.active').first().data('id') != null)
                choice = parseInt($('#rate-finder').find('span.active').first().data('id'));

            sData = {
                choice: choice,
                comment: $('#feedback-comment').val()
            };

            var serviceUrl = rootPath + "/WebService.asmx/InselFinderFeedback";

            $.ajax({
                'type': 'POST',
                'contentType': 'application/json; charset=utf-8',
                'url': serviceUrl,
                'data': JSON.stringify(sData, null, 2),
                'dataType': 'json',
                'success': function (rData) {
                    if (rData.d.Status == 1) {
                        $('#rate-feedback, .status.send-feedback').hide();
                        $('#end-feedback').fadeIn();
                        $.cookie('feedback_inselfinder', 'true', { expires: cookieDays });
                    } else {
                        $('#rate-feedback, .status.send-feedback').hide();
                        $('#error-feedback').fadeIn();
                    }
                },
                'error': function (request, status, error) {
                    $('#rate-feedback, .status.send-feedback').hide();
                    $('#error-feedback').fadeIn();
                }
            });

            return false;

        }

        $('input#send-inselfinder-feedback').click(function (e) {
            $('#form-send-inselfinder-feedback').validate({
                submitHandler: function (form) {
                    btnFormFeedbackOk_Click('#form-send-inselfinder-feedback');
                }
            });
        });
    }, 10000);



}

function initPdfCreation() {
    $(document).on('click', '#goto-pdf-creation', btnPdfCreation_Click);
    $('.pdf-creation-please-wait').hide();
    $('#goto-pdf-download').hide();

    $('#goto-pdf-download').click(
        function (e) {
            ms.trackDownload('PDF', 'Download PDF Inselfinder');
        }
    );

    function btnPdfCreation_Click(e) {

        ms.trackEvent('Inselfinder - PDF erzeugen', '/inselfinder/pdf_erzeugen');

        $('.pdf-creation-please-wait').show();
        $('#goto-pdf-download').hide();

        var myPdf = initPdfObj(objDaylyplan);

        sData = myPdf;

        var serviceUrl = rootPath + "/WebService.asmx/InselFinderWochenplanPdf";
        $.ajax({
            'type': 'POST',
            'contentType': 'application/json; charset=utf-8',
            'url': serviceUrl,
            'data': JSON.stringify(sData, null, 2),
            'dataType': 'json',
            'success': function (rData) {
                if (rData.d.Status == 1) {
                    $('#goto-pdf-download').attr('href', rData.d.Url);
                    $('#goto-pdf-download').show();
                    $('#goto-pdf-creation').hide();
                } else {
                    //-- TODO Emi
                    alert(rData.d.StatusMessage);
                }
                $('.pdf-creation-please-wait').hide();
            },
            'error': function (request, status, error) {
                //-- TODO Emi
                alert('Es ist ein Fehler aufgetreten. Bitten probieren Sie es zu einem späterem Zeitpunkt noch einmal.');
            }
        });

        return false;

    }
}











// OBJ
// ************************************************************
// ************************************************************
// ************************************************************
var TimePlan = function () {
    this.monday = new Day("Montag");
    this.tuesday = new Day("Dienstag");
    this.wednesday = new Day("Mittwoch");
    this.thursday = new Day("Donnerstag");
    this.friday = new Day("Freitag");
}

Day = function (dayname) {
    this.dayname = dayname;
    this.childLeave = 360; // Startime is 6 a'clock
    this.childHome = 720;
    this.childBed = 1320; // Endtime is 22 a'clock
    this.withChild = new WithChild(this.childBed, this.childHome);
    this.withoutChild = new WithoutChild(this.childHome, this.childLeave);
}

WithChild = function (childBed, childHome) {
    this.interval = childBed - childHome;
    this.intervalHousework = 0;
    this.intervalSparetime = 0;
    this.hobbychild = 0;
    this.housework = new HouseWork();
    this.sparetime = new SpareTime();
    this.currentInterval = 0;
}

WithoutChild = function (childHome, childLeave) {
    this.interval = childHome - childLeave;
    this.intervalHousework = 0;
    this.intervalSparetime = 0;
    this.worktime = 0;
    this.hobbychild = 0;
    this.housework = new HouseWork();
    this.sparetime = new SpareTime();
    this.currentInterval = 0;
}

HouseWork = function () {
    this.shop = new Activity("Einkaufen", 0);
    this.washing = new Activity("Waschen", 0);
    this.cleaning = new Activity("Putzen", 0);
    this.cooking = new Activity("Kochen", 0);
    this.other = new Activity("Sonstiges", 0);
}

SpareTime = function () {
    this.television = new Activity("Fernsehen", 0);
    this.reading = new Activity("Lesen", 0);
    this.internet = new Activity("Internet", 0);
    this.sports = new Activity("Sport", 0);
    this.shopping = new Activity("Shopping", 0);
    this.friends = new Activity("Freunde", 0);
    this.hobbies = new Activity("Hobbys", 0);
    this.other = new Activity("Sonstiges", 0);
}

Activity = function (activityName, activityValue) {
    this.name = activityName || "";
    this.value = activityValue || 0;
}

// OBJ PDF
// ************************************************************
// ************************************************************
// ************************************************************
Week = function () {
    this.monday = new Array();
    this.tuesday = new Array();
    this.wednesday = new Array();
    this.thursday = new Array();
    this.friday = new Array();
}

ActivityItem = function () {
    this.Type = 'ActivityTypes.Regular';
    this.Caption = '';
    this.Height = 0;
    this.Duration = 0;
    this.TextColor = '';
    this.BackColor = '';
}

ActivityHeader = function () {
    this.Type = 'ActivityTypes.Header';
    this.Caption = '';
    this.Height = 50;
    this.TextColor = '';
    this.BackColor = '';
}

ActivityMessage = function (day) {
    this.Type = 'ActivityTypes.Warning';
    this.Caption = day + " ist keine Inselzeit mehr übrig...";
    this.Height = 1;
    this.TextColor = '#E53216';
    this.BackColor = '#FFFFFF';
}

ActivityWithImage = function () {
    this.Type = '';
    this.Caption = '';
    this.Height = 50;
    this.Duration = 0;
    this.ImageHeight = 0;
    this.TextColor = '';
    this.BackColor = '';
}



// Funktionen
// ************************************************************
// ************************************************************
// ************************************************************
var initPdfObj = function (objDaylyplan) {

    // Erzeuge Obj
    var week = new Week();

    $.each(objDaylyplan, function (key, value) {

        // Day
        day = new ActivityHeader();

        day.Type = 'ActivityTypes.Header';
        day.Caption = value.dayname.toUpperCase();
        day.Height = 50;
        day.TextColor = '#999999';
        day.BackColor = '#FFFFFF';
        week[key].push(day);

        // Header Ohne Kind
        withoutChild = new ActivityItem();
        withoutChild.Type = 'ActivityTypes.Regular';
        withoutChild.Caption = 'Zeit ohne Kind';
        withoutChild.Height = 50;
        withoutChild.Duration = minutesToString(objDaylyplan[key].withoutChild.interval);
        withoutChild.TextColor = '#FFFFFF';
        withoutChild.BackColor = '#01719C';
        week[key].push(withoutChild);

        // Arbeitszeit
        if (objDaylyplan[key].withoutChild.worktime > 0) {
            worktime = new ActivityItem();
            worktime.Type = 'ActivityTypes.Regular';
            worktime.Caption = 'Arbeitszeit';
            worktime.Height = objDaylyplan[key].withoutChild.worktime;
            worktime.Duration = minutesToString(objDaylyplan[key].withoutChild.worktime);
            worktime.TextColor = '#FFFFFF';
            worktime.BackColor = '#00ADF0';
            week[key].push(worktime);
        }


        // Hausarbeit
        if (objDaylyplan[key].withoutChild.intervalHousework > 0) {
            housework = new ActivityItem();
            housework.Type = 'ActivityTypes.Regular';
            housework.Caption = 'Hausarbeit';
            housework.Height = objDaylyplan[key].withoutChild.intervalHousework;
            housework.Duration = minutesToString(objDaylyplan[key].withoutChild.intervalHousework);
            housework.TextColor = '#FFFFFF';
            housework.BackColor = '#00ADF0';
            week[key].push(housework);
        }
        /*
        $.each(objDaylyplan[key].withoutChild.housework, function(activity, value){

            if(value.value > 0) {

                activity = new ActivityItem();

                activity.Caption = value.name.toUpperCase();
                activity.Height = value.value;
                activity.Duration = minutesToString(value.value);
                activity.TextColor = '#FFFFFF';
                activity.BackColor = '#00ADF0';
                week[key].push(activity);
            }
            
        });
        */


        // Freizeit
        if (objDaylyplan[key].withoutChild.intervalSparetime > 0) {
            sparetime = new ActivityItem();
            sparetime.Type = 'ActivityTypes.Regular';
            sparetime.Caption = 'Freizeit';
            sparetime.Height = objDaylyplan[key].withoutChild.intervalSparetime;
            sparetime.Duration = minutesToString(objDaylyplan[key].withoutChild.intervalSparetime);
            sparetime.TextColor = '#FFFFFF';
            sparetime.BackColor = '#00ADF0';
            week[key].push(sparetime);
        }
        /*
        $.each(objDaylyplan[key].withoutChild.sparetime, function(activity, value){

            if(value.value > 0) {

                activity = new ActivityItem();

                activity.Caption = value.name.toUpperCase();
                activity.Height = value.value;
                activity.Duration = minutesToString(value.value);
                activity.TextColor = '#FFFFFF';
                activity.BackColor = '#00ADF0';
                week[key].push(activity);
            }
            
        });
        */


        // HobbyKind
        if (objDaylyplan[key].withoutChild.hobbychild > 0) {
            hobbychild = new ActivityItem();
            hobbychild.Type = 'ActivityTypes.Regular';
            hobbychild.Caption = 'Hobby Kind';
            hobbychild.Height = objDaylyplan[key].withoutChild.hobbychild;
            hobbychild.Duration = minutesToString(objDaylyplan[key].withoutChild.hobbychild);
            hobbychild.TextColor = '#FFFFFF';
            hobbychild.BackColor = '#00ADF0';
            week[key].push(hobbychild);
        }

        // Verblieben Zeit

        if (getCurInterval(key, 'withoutChild') > 30) {
            var size = getCurInterval(key, 'withoutChild') - 50;
            var imgheigth = 0;

            if (size > 130)
                imgheight = 92;
            if (size <= 130 && size > 95)
                imgheight = 70;
            if (size <= 95 && size > 75)
                imgheight = 50;
            if (size <= 75 && size > 55)
                imgheight = 30;
            if (size <= 55)
                imgheight = 0;

            rest = new ActivityWithImage();

            rest.Type = 'ActivityTypes.Hourglass';
            rest.Caption = 'Verbliebene Zeit';
            rest.Height = getCurInterval(key, 'withoutChild');
            rest.Duration = minutesToString(getCurInterval(key, 'withoutChild'));
            rest.ImageHeight = imgheight;
            rest.TextColor = '#00ADF0';
            rest.BackColor = '#00ADF0';
            week[key].push(rest);
        } else if (getCurInterval(key, 'withoutChild') <= 30 && getCurInterval(key, 'withoutChild') > 0) {
            rest = new ActivityWithImage();

            rest.Type = 'ActivityTypes.Hourglass';
            rest.Caption = 'Verbliebene Zeit';
            rest.Height = 50;
            rest.Duration = minutesToString(getCurInterval(key, 'withoutChild'));
            rest.ImageHeight = 0;
            rest.TextColor = '#00ADF0';
            rest.BackColor = '#00ADF0';
            week[key].push(rest);
        }


        // Header mit Kind
        withChild = new ActivityItem();
        withChild.Type = 'ActivityTypes.Regular';
        withChild.Caption = 'Zeit mit Kind';
        withChild.Height = 50;
        withChild.Duration = minutesToString(objDaylyplan[key].withChild.interval);
        withChild.TextColor = '#FFFFFF';
        withChild.BackColor = '#0E943B';
        week[key].push(withChild);


        // Hausarbeit
        if (objDaylyplan[key].withChild.intervalHousework > 0) {
            houseworkWC = new ActivityItem();
            houseworkWC.Type = 'ActivityTypes.Regular';
            houseworkWC.Caption = 'Hausarbeit';
            houseworkWC.Height = objDaylyplan[key].withChild.intervalHousework;
            houseworkWC.Duration = minutesToString(objDaylyplan[key].withChild.intervalHousework);
            houseworkWC.TextColor = '#FFFFFF';
            houseworkWC.BackColor = '#84D95A';
            week[key].push(houseworkWC);
        }
        /*$.each(objDaylyplan[key].withChild.housework, function(activity, value){

            if(value.value > 0) {

                activity = new ActivityItem();

                activity.Caption = value.name.toUpperCase();
                activity.Height = value.value;
                activity.Duration = minutesToString(value.value);
                activity.TextColor = '#FFFFFF';
                activity.BackColor = '#84D95A';
                week[key].push(activity);
            }
            
        });*/


        // Freizeit
        if (objDaylyplan[key].withChild.intervalSparetime > 0) {
            sparetimeWC = new ActivityItem();
            sparetimeWC.Type = 'ActivityTypes.Regular';
            sparetimeWC.Caption = 'Freizeit';
            sparetimeWC.Height = objDaylyplan[key].withChild.intervalSparetime;
            sparetimeWC.Duration = minutesToString(objDaylyplan[key].withChild.intervalSparetime);
            sparetimeWC.TextColor = '#FFFFFF';
            sparetimeWC.BackColor = '#84D95A';
            week[key].push(sparetimeWC);
        }

        /*$.each(objDaylyplan[key].withChild.sparetime, function(activity, value){

            if(value.value > 0) {

                activity = new ActivityItem();

                activity.Caption = value.name.toUpperCase();
                activity.Height = value.value;
                activity.Duration = minutesToString(value.value);
                activity.TextColor = '#FFFFFF';
                activity.BackColor = '#84D95A';
                week[key].push(activity);
            }
            
        });*/


        // HobbyKind
        if (objDaylyplan[key].withChild.hobbychild > 0) {
            hobbychild = new ActivityItem();
            hobbychild.Type = 'ActivityTypes.Regular';
            hobbychild.Caption = 'Hobby Kind';
            hobbychild.Height = objDaylyplan[key].withChild.hobbychild;
            hobbychild.Duration = minutesToString(objDaylyplan[key].withChild.hobbychild);
            hobbychild.TextColor = '#FFFFFF';
            hobbychild.BackColor = '#84D95A';
            week[key].push(hobbychild);
        }



        // Verblieben Zeit
        if (getCurInterval(key, 'withChild') > 30) {

            var size = getCurInterval(key, 'withChild') - 50;
            var imgheigth = 0;

            if (size > 130)
                imgheight = 92;
            if (size <= 130 && size > 95)
                imgheight = 70;
            if (size <= 95 && size > 75)
                imgheight = 50;
            if (size <= 75 && size > 55)
                imgheight = 30;
            if (size <= 55)
                imgheight = 0;

            insel = new ActivityWithImage();

            insel.Type = 'ActivityTypes.Island';
            insel.Caption = 'Inselzeit';
            insel.Height = getCurInterval(key, 'withChild');
            insel.Duration = minutesToString(getCurInterval(key, 'withChild'));
            insel.ImageHeight = imgheight;
            insel.TextColor = '#0E943B';
            insel.BackColor = '#84D95A';
            week[key].push(insel);

        } else if (getCurInterval(key, 'withChild') <= 30 && getCurInterval(key, 'withChild') > 0) {
            insel = new ActivityWithImage();

            insel.Type = 'ActivityTypes.Island';
            insel.Caption = 'Inselzeit';
            insel.Height = 50;
            insel.Duration = minutesToString(getCurInterval(key, 'withChild'));
            insel.ImageHeight = 0;
            insel.TextColor = '#0E943B';
            insel.BackColor = '#84D95A';
            week[key].push(insel);

        } else {
            insel = new ActivityMessage(value.dayname);
            week[key].push(insel);
        }

    });

    return week;

}


var stringToMinutes = function (string) {
    var string = string.replace('h', '').split(':');
    var minutes = parseInt(string[0]) * 60 + parseInt(string[1]);
    return minutes;
}

var minutesToString = function (min) {
    var hour = Math.floor(min / 60);
    var minutes = (Math.floor(min % 60) == 0) ? '00' : Math.floor(min % 60);
    var string = hour + ':' + minutes + ' h';

    return string;
}


var initTabs = function (el) {
    $tabs = $(el).tabs({
        active: 0,
        show: { effect: effectStyle, duration: effectDuration }
    });
    return $tabs;
}


var getCurInterval = function (day, section) {
    var curInterval = 0;
    var interval = objDaylyplan[day][section].interval;
    var housework = objDaylyplan[day][section].intervalHousework;
    var sparetime = objDaylyplan[day][section].intervalSparetime;
    var hobbychild = objDaylyplan[day][section].hobbychild;

    if (section == 'withoutChild') {
        var worktime = objDaylyplan[day][section].worktime;
        curInterval = interval - (worktime + housework + sparetime + hobbychild);
    }
    if (section == 'withChild') {
        curInterval = interval - (housework + sparetime + hobbychild);
    }

    return curInterval
}

var initSelectField = function (dataname, interval, curInterval, text, startTime) {

    var length = interval / minUnit;
    var liTemp = '';
    var ulWidth = (length + 1) * widthLi;
    var last = curInterval / minUnit;

    for (var i = 0; i <= length; i++) {
        time = (i * minUnit) + startTime;

        if (i == 0 && last != 0)
            liTemp += '<li class="first active" data-id=' + i + '>' + minutesToString(time) + '</li>';
        else if (i == last && last != 0)
            liTemp += '<li class="last" data-id=' + i + '>' + minutesToString(time) + '</li>';
        else if (i == last && i == 0)
            liTemp += '<li class="first active last" data-id=' + i + '>' + minutesToString(time) + '</li>';
        else
            liTemp += '<li data-id=' + i + '>' + minutesToString(time) + '</li>';


    }

    var tempSelField = '<div class="select-field">'
                        + '<div class="item">'
                        + '<div class="name">' + text + '</div>'
                        + '<ul class="select ' + dataname + '" data-item=' + dataname + ' style="width:' + ulWidth + 'px;">' + liTemp + '</ul>'
                        + '<div class="blur"><a class="prev" data-item=' + dataname + '></a> <a class="next" data-item=' + dataname + '></a></div>'
                        + '</div>'
                        + '</div>';

    return tempSelField;
}

var bfadSelectTime = function (childLeaveId, childHomeId, childBedId) {

    $.each(objDaylyplan, function (key, value) {
        with (objDaylyplan[key]) {

            childLeave = (childLeaveId * minUnit) + startTime;
            childHome = (childHomeId * minUnit) + startTime;
            childBed = (childBedId * minUnit) + startTime;

            withChild.interval = childBed - childHome;
            withoutChild.interval = childHome - childLeave;
        }
    });

    updateTimeSelectItems();
    $('#select-time #monday').append('<div class="message">Es wurden alle Zeiten übernommen.</div>');
}

var bfadWorktime = function (daypanel) {
    message = '';
    flag = 0;
    var worktimeActive = $('#monday ul.worktime li.active').data('id');
    var worktime = objDaylyplan.monday.withoutChild.worktime;

    $(daypanel).each(function () {
        var day = $(this).attr('id');
        if (worktime > getCurInterval(day, 'withoutChild') + objDaylyplan[day].withoutChild.worktime) {
            flag = 1;
            message += ', ' + objDaylyplan[day].dayname;
        }
        else {
            objDaylyplan[day].withoutChild.worktime = worktime;
        }
    });


    if (flag == 1) {
        message = message.substring(1);
        $('#worktime #monday').append('<div class="message error">' + strBfadOverlay + message + '</div>');
    } else {
        $('#worktime #monday').append('<div class="message">Es wurden alle Zeiten übernommen.</div>');
    }
    updateWorktimeItems(daypanel, worktimeActive);


}

var bfadHousework = function (daypanel) {

    var message = "";
    var ulWoc = '#housework #monday .without-child ul';
    var flag = 0;
    $(daypanel).each(function () {
        var day = $(this).attr('id');
        if (objDaylyplan.monday.withoutChild.intervalHousework > getCurInterval(day, 'withoutChild') + objDaylyplan[day].withoutChild.intervalHousework || objDaylyplan.monday.withChild.intervalHousework > getCurInterval(day, 'withChild') + objDaylyplan[day].withChild.intervalHousework) {
            flag = 1;
            message += ', ' + objDaylyplan[day].dayname;
        }
        else {

            with (objDaylyplan[day].withoutChild.housework) {
                shop.value = $(ulWoc + '.shop').find('li.active').data('id') * minUnit;
                washing.value = $(ulWoc + '.washing').find('li.active').data('id') * minUnit;
                cleaning.value = $(ulWoc + '.cleaning').find('li.active').data('id') * minUnit;
                cooking.value = $(ulWoc + '.cooking').find('li.active').data('id') * minUnit;
                other.value = $(ulWoc + '.other').find('li.active').data('id') * minUnit;

                objDaylyplan[day].withoutChild.intervalHousework = shop.value + washing.value + cleaning.value + cooking.value + other.value;
            }

            var ulWc = '#housework #monday .with-child ul';
            with (objDaylyplan[day].withChild.housework) {
                shop.value = $(ulWc + '.shop').find('li.active').data('id') * minUnit;
                washing.value = $(ulWc + '.washing').find('li.active').data('id') * minUnit;
                cleaning.value = $(ulWc + '.cleaning').find('li.active').data('id') * minUnit;
                cooking.value = $(ulWc + '.cooking').find('li.active').data('id') * minUnit;
                other.value = $(ulWc + '.other').find('li.active').data('id') * minUnit;

                objDaylyplan[day].withChild.intervalHousework = shop.value + washing.value + cleaning.value + cooking.value + other.value;
            }
        }

    });


    if (flag == 1) {
        message = message.substring(1);
        $('#housework #monday').append('<div class="message error">' + strBfadOverlay + message + '</div>');
    } else {
        $('#housework #monday').append('<div class="message">Es wurden alle Zeiten übernommen.</div>');
    }
    updateHouseworkItems(daypanel);
}

var bfadSparetime = function (daypanel) {
    var flag = 0;
    message = "";

    $.each(objDaylyplan, function (day, value) {

        if (objDaylyplan.monday.withoutChild.intervalSparetime > getCurInterval(day, 'withoutChild') + objDaylyplan[day].withoutChild.intervalSparetime || objDaylyplan.monday.withChild.intervalSparetime > getCurInterval(day, 'withChild') + objDaylyplan[day].withChild.intervalSparetime) {
            flag = 1;
            message += ', ' + objDaylyplan[day].dayname;
        }
        else {
            var ulWoc = '#sparetime #monday .without-child ul';
            with (objDaylyplan[day].withoutChild.sparetime) {
                television.value = $(ulWoc + '.television').find('li.active').data('id') * minUnit;
                reading.value = $(ulWoc + '.reading').find('li.active').data('id') * minUnit;
                internet.value = $(ulWoc + '.internet').find('li.active').data('id') * minUnit;
                sports.value = $(ulWoc + '.sports').find('li.active').data('id') * minUnit;
                shopping.value = $(ulWoc + '.shopping').find('li.active').data('id') * minUnit;
                friends.value = $(ulWoc + '.friends').find('li.active').data('id') * minUnit;
                hobbies.value = $(ulWoc + '.hobbies').find('li.active').data('id') * minUnit;
                other.value = $(ulWoc + '.other').find('li.active').data('id') * minUnit;

                objDaylyplan[day].withoutChild.intervalSparetime = television.value + reading.value + internet.value + sports.value + shopping.value + friends.value + hobbies.value + other.value;
            }

            var ulWc = '#sparetime #monday .with-child ul';
            with (objDaylyplan[day].withChild.sparetime) {
                television.value = $(ulWc + '.television').find('li.active').data('id') * minUnit;
                reading.value = $(ulWc + '.reading').find('li.active').data('id') * minUnit;
                internet.value = $(ulWc + '.internet').find('li.active').data('id') * minUnit;
                sports.value = $(ulWc + '.sports').find('li.active').data('id') * minUnit;
                shopping.value = $(ulWc + '.shopping').find('li.active').data('id') * minUnit;
                friends.value = $(ulWc + '.friends').find('li.active').data('id') * minUnit;
                hobbies.value = $(ulWc + '.hobbies').find('li.active').data('id') * minUnit;
                other.value = $(ulWc + '.other').find('li.active').data('id') * minUnit;

                objDaylyplan[day].withChild.intervalSparetime = television.value + reading.value + internet.value + sports.value + shopping.value + friends.value + hobbies.value + other.value;
            }
        }
    });

    if (flag == 1) {
        message = message.substring(1);
        $('#sparetime #monday').append('<div class="message error">' + strBfadOverlay + message + '</div>');
    } else {
        $('#sparetime #monday').append('<div class="message">Es wurden alle Zeiten übernommen.</div>');
    }
    updateSparetimeItems(daypanel);
}

var bfadHobbychild = function (daypanel) {
    var flag = 0;
    message = "";

    var hobbychildWoc = objDaylyplan.monday.withoutChild.hobbychild;
    var hobbychildWc = objDaylyplan.monday.withChild.hobbychild;

    $.each(objDaylyplan, function (day, value) {

        if (hobbychildWoc > getCurInterval(day, 'withoutChild') + objDaylyplan[day].withoutChild.hobbychild || hobbychildWc > getCurInterval(day, 'withChild') + objDaylyplan[day].withChild.hobbychild) {
            flag = 1;
            message += ', ' + objDaylyplan[day].dayname;

        } else {
            var ulWoc = '#hobbychild #monday .without-child ul.hobbychild';
            objDaylyplan[day].withoutChild.hobbychild = $(ulWoc).find('li.active').data('id') * minUnit;
            var ulWc = '#hobbychild #monday .with-child ul.hobbychild';
            objDaylyplan[day].withChild.hobbychild = $(ulWc).find('li.active').data('id') * minUnit;
        }
    });

    if (flag == 1) {
        message = message.substring(1);
        $('#hobbychild #monday').append('<div class="message error">' + strBfadOverlay + message + '</div>');
    } else {
        $('#hobbychild #monday').append('<div class="message">Es wurden alle Zeiten übernommen.</div>');
    }
    updateHobbychildItems(daypanel);
}

var updateTimeSelectItems = function () {
    $('#select-time .day-panel').each(function () {
        var day = $(this).attr('id');

        var childLeaveId = (objDaylyplan[day].childLeave - startTime) / minUnit;
        var childHomeId = (objDaylyplan[day].childHome - startTime) / minUnit;
        var childBedId = (objDaylyplan[day].childBed - startTime) / minUnit;

        var ulChildLeave = $(this).find('ul.child-leave');
        var ulChildHome = $(this).find('ul.child-home');
        var ulChildBed = $(this).find('ul.child-bed');

        ulChildLeave.find('li.active').removeClass('active');
        ulChildLeave.find('li[data-id=' + childLeaveId + ']').addClass('active');
        $(ulChildLeave).css({ "left": -(childLeaveId * widthLi) });

        ulChildHome.find('li.first').removeClass('first');
        ulChildHome.find('li[data-id=' + childLeaveId + ']').addClass('first');
        ulChildHome.find('li.active').removeClass('active');
        ulChildHome.find('li[data-id=' + childHomeId + ']').addClass('active');
        $(ulChildHome).css({ "left": -(childHomeId * widthLi) });

        ulChildBed.find('li.first').removeClass('first');
        ulChildBed.find('li[data-id=' + childHomeId + ']').addClass('first');
        ulChildBed.find('li.active').removeClass('active');
        ulChildBed.find('li[data-id=' + childBedId + ']').addClass('active');
        $(ulChildBed).css({ "left": -(childBedId * widthLi) });
    });
}

var updateWorktimeItems = function (section) {
    $(section).each(function () {
        var day = $(this).attr('id');

        var worktimeId = (objDaylyplan[day].withoutChild.worktime) / minUnit;
        var ulWorktime = $(this).find('.without-child ul.worktime');
        var curIntervalWoc = getCurInterval(day, 'withoutChild');
        lastId = worktimeId + (curIntervalWoc / minUnit);

        $('#worktime' + day + ' .without-child .time').text(minutesToString(curIntervalWoc));
        ulWorktime.find('li.active').removeClass('active');
        ulWorktime.find('li[data-id=' + worktimeId + ']').addClass('active');
        ulWorktime.find('li.last').removeClass('last');
        ulWorktime.find('li[data-id=' + lastId + ']').addClass('last');
        $(ulWorktime).css({ "left": -(worktimeId * widthLi) });
    });
}

var updateHouseworkItems = function (section) {
    $(section).each(function () {
        var day = $(this).attr('id');
        var curIntervalWoc = getCurInterval(day, 'withoutChild');
        var curIntervalWc = getCurInterval(day, 'withChild');
        $('#housework #' + day + ' .without-child .time').text(minutesToString(curIntervalWoc));
        $('#housework' + day + ' .with-child .time').text(minutesToString(curIntervalWc));

        $.each(objDaylyplan[day].withoutChild.housework, function (activity, value) {
            ul = '#housework #' + day + ' .without-child ul.' + activity;
            activeId = value.value / minUnit;
            lastId = activeId + (curIntervalWoc / minUnit);

            $(ul).find('li.active').removeClass('active');
            $(ul).find('li[data-id=' + activeId + ']').addClass('active');
            $(ul).find('li.last').removeClass('last');
            $(ul).find('li[data-id=' + lastId + ']').addClass('last');
            $(ul).css({ "left": -(activeId * widthLi) });
        });

        $.each(objDaylyplan[day].withChild.housework, function (activity, value) {
            ul = '#housework #' + day + ' .with-child ul.' + activity;
            activeId = value.value / minUnit;
            lastId = activeId + (curIntervalWc / minUnit);

            $(ul).find('li.active').removeClass('active');
            $(ul).find('li[data-id=' + activeId + ']').addClass('active');
            $(ul).find('li.last').removeClass('last');
            $(ul).find('li[data-id=' + lastId + ']').addClass('last');
            $(ul).css({ "left": -(activeId * widthLi) });
        });
    });
}

var updateSparetimeItems = function (section) {
    $(section).each(function () {
        var day = $(this).attr('id');
        var curIntervalWoc = getCurInterval(day, 'withoutChild');
        var curIntervalWc = getCurInterval(day, 'withChild');
        $('#sparetime #' + day + ' .without-child .time').text(minutesToString(curIntervalWoc));
        $('#sparetime #' + day + ' .with-child .time').text(minutesToString(curIntervalWc));

        $.each(objDaylyplan[day].withoutChild.sparetime, function (activity, value) {
            ul = '#sparetime #' + day + ' .without-child ul.' + activity;
            activeId = value.value / minUnit;
            lastId = activeId + (curIntervalWoc / minUnit);

            $(ul).find('li.active').removeClass('active');
            $(ul).find('li[data-id=' + activeId + ']').addClass('active');
            $(ul).find('li.last').removeClass('last');
            $(ul).find('li[data-id=' + lastId + ']').addClass('last');
            $(ul).css({ "left": -(activeId * widthLi) });
        });

        $.each(objDaylyplan[day].withChild.sparetime, function (activity, value) {
            ul = '#sparetime #' + day + ' .with-child ul.' + activity;
            activeId = value.value / minUnit;
            lastId = activeId + (curIntervalWc / minUnit);

            $(ul).find('li.active').removeClass('active');
            $(ul).find('li[data-id=' + activeId + ']').addClass('active');
            $(ul).find('li.last').removeClass('last');
            $(ul).find('li[data-id=' + lastId + ']').addClass('last');
            $(ul).css({ "left": -(activeId * widthLi) });
        });
    });
}

var updateHobbychildItems = function (section) {
    $(section).each(function () {

        var day = $(this).attr('id');

        var hobbychildIdWoc = (objDaylyplan[day].withoutChild.hobbychild) / minUnit;
        var ulHobbychildWoc = $(this).find('.without-child ul.hobbychild');
        var curIntervalWoc = getCurInterval(day, 'withoutChild');

        var hobbychildIdWc = (objDaylyplan[day].withChild.hobbychild) / minUnit;
        var ulHobbychildWc = $(this).find('.with-child ul.hobbychild');
        var curIntervalWc = getCurInterval(day, 'withChild');

        $('#' + day + ' .without-child .time').text(minutesToString(curIntervalWoc));
        ulHobbychildWoc.find('li.active').removeClass('active');
        ulHobbychildWoc.find('li[data-id=' + hobbychildIdWoc + ']').addClass('active');
        $(ulHobbychildWoc).css({ "left": -(hobbychildIdWoc * widthLi) });

        $('#' + day + ' .with-child .time').text(minutesToString(curIntervalWc));
        ulHobbychildWc.find('li.active').removeClass('active');
        ulHobbychildWc.find('li[data-id=' + hobbychildIdWc + ']').addClass('active');
        $(ulHobbychildWc).css({ "left": -(hobbychildIdWc * widthLi) });
    });
}

var updateAllItems = function () {

    updateWorktimeItems('#worktime .day-panel');
    updateHouseworkItems('#housework .day-panel');
    updateSparetimeItems('#sparetime .day-panel');
    updateHobbychildItems('#hobbychild .day-panel');
}

var updateDay = function () {
    $('#select-time .day-panel').each(function () {
        var day = $(this).attr('id');

        with (objDaylyplan[day]) {
            childLeave = ($(this).find('ul.child-leave li.active').data('id') * minUnit) + startTime;
            childHome = ($(this).find('ul.child-home li.active').data('id') * minUnit) + startTime;
            childBed = ($(this).find('ul.child-bed li.active').data('id') * minUnit) + startTime;

            withChild.interval = childBed - childHome;
            withoutChild.interval = childHome - childLeave;
        }
    });
}

var updateWorktime = function () {
    $('#worktime .day-panel').each(function () {
        var day = $(this).attr('id');
        objDaylyplan[day].withoutChild.worktime = ($(this).find('ul.worktime li.active').data('id') * minUnit);
    });
}

var updateHousework = function () {
    $('#housework .day-panel').each(function () {
        var day = $(this).attr('id');
        houseworkIntWoc = 0;
        houseworkIntWc = 0;

        $.each(objDaylyplan[day].withoutChild.housework, function (activity, value) {
            ul = '#housework #' + day + ' .without-child ul.' + activity;
            value.value = $(ul).find('li.active').data('id') * minUnit;
            houseworkIntWoc += value.value;
        });

        $.each(objDaylyplan[day].withChild.housework, function (activity, value) {
            ul = '#housework #' + day + ' .with-child ul.' + activity;
            value.value = $(ul).find('li.active').data('id') * minUnit;
            houseworkIntWc += value.value;
        });
    });
}

var updateSparetime = function () {
    $('#sparetime .day-panel').each(function () {
        var day = $(this).attr('id');
        sparetimeIntWoc = 0;
        sparetimeIntWc = 0;

        $.each(objDaylyplan[day].withoutChild.sparetime, function (activity, value) {
            ul = '#sparetime #' + day + ' .without-child ul.' + activity;
            value.value = $(ul).find('li.active').data('id') * minUnit;
            sparetimeIntWoc += value.value;
        });

        $.each(objDaylyplan[day].withChild.sparetime, function (activity, value) {
            ul = '#sparetime #' + day + ' .with-child ul.' + activity;
            value.value = $(ul).find('li.active').data('id') * minUnit;
            sparetimeIntWc += value.value;
        });
    });
}

var updateHobbychild = function () {
    $('#hobbychild .day-panel').each(function () {
        var day = $(this).attr('id');
        objDaylyplan[day].withoutChild.hobbychild = ($(this).find('.without-child ul.hobbychild li.active').data('id') * minUnit);
        objDaylyplan[day].withChild.hobbychild = ($(this).find('.with-child ul.hobbychild li.active').data('id') * minUnit);
    });
}


var drawMap = function (param) {

    var templ = '';

    $.each(objDaylyplan, function (day, value) {

        var withChild = objDaylyplan[day].withChild.interval;
        var withChildTime = minutesToString(withChild);
        var withoutChild = objDaylyplan[day].withoutChild.interval;
        var withoutChildTime = minutesToString(withoutChild);

        var contentWoc = '';
        var contentWc = '';
        var templWorktimeWoc = '';
        var liTemp = '';

        var restIntervalWoc = getCurInterval(day, 'withoutChild');
        var insel = getCurInterval(day, 'withChild');




        templWorktimeWoc = '<a href="#finder-worktime_#' + day + '" class="btn-edit">'
                            + '<div class="item" style="height:' + objDaylyplan[day].withoutChild.worktime / divider + 'px">'
                            + '<div id="title">Arbeiten<span class="time"><span class="icon pen"></span>' + minutesToString(objDaylyplan[day].withoutChild.worktime) + '</span></div>'
                            + '</div>'
                            + '</a>';



        /*$.each(objDaylyplan[day].withoutChild.housework, function(activity, value){
            if(value.value > 0) liTemp += '<li>' + value.name + '</li>'
        });*/

        templHouseworkWoc = '<a href="#finder-housework_#' + day + '" class="btn-edit">'
                            + '<div class="item" style="height:' + objDaylyplan[day].withoutChild.intervalHousework / divider + 'px">'
                            + '<div id="title">Hausarbeit<span class="time"><span class="icon pen"></span>' + minutesToString(objDaylyplan[day].withoutChild.intervalHousework) + '</span></div>'
                            + '<ul>' + liTemp + '</ul>'
                            + '</div>'
                            + '</a>';



        /* $.each(objDaylyplan[day].withoutChild.sparetime, function(activity, value){
            if(value.value > 0) liTemp += '<li>' + value.name + '</li>'
        });*/

        templSparetimeWoc = '<a href="#finder-sparetime_#' + day + '" class="btn-edit">'
                            + '<div class="item" style="height:' + objDaylyplan[day].withoutChild.intervalSparetime / divider + 'px">'
                            + '<div id="title">Freizeit<span class="time"><span class="icon pen"></span>' + minutesToString(objDaylyplan[day].withoutChild.intervalSparetime) + '</span></div>'
                            + '<ul>' + liTemp + '</ul>'
                            + '</div>'
                            + '</a>';


        templHobbychildWoc = '<a href="#finder-hobbychild_#' + day + '" class="btn-edit">'
                            + '<div class="item" style="height:' + objDaylyplan[day].withoutChild.hobbychild / divider + 'px">'
                            + '<div id="title">Hobby Kind<span class="time"><span class="icon pen"></span>' + minutesToString(objDaylyplan[day].withoutChild.hobbychild) + '</span></div>'
                            + '</div>'
                            + '</a>';

        if (restIntervalWoc > 0) {

            var imgheight = (restIntervalWoc / divider) - 40;
            var iHeight = 0;

            if (imgheight > 130)
                iHeight = 92;
            if (imgheight <= 130 && imgheight > 95)
                iHeight = 70;
            if (imgheight <= 95 && imgheight > 75)
                iHeight = 50;
            if (imgheight <= 75 && imgheight > 55)
                iHeight = 30;
            if (imgheight <= 55)
                iHeight = 0;


            templRestWoc = '<div class="item rest" style="height:' + restIntervalWoc / divider + 'px">'
                            + '<div id="title">Verbliebene Zeit<span class="time">' + minutesToString(restIntervalWoc) + '</span></div>'
                            + '<div class="img-insel" style="height:' + imgheight + 'px;"><div class="center"><img src="/fileadmin/template/media/sanduhr.png"  style="width:' + iHeight + 'px; height:' + iHeight + 'px;"/></div></div>'
                            + '</div>';
        } else {
            templRestWoc = '';
        }




        /*$.each(objDaylyplan[day].withChild.housework, function(activity, value){
            if(value.value > 0) liTemp += '<li>' + value.name + '</li>'
        });*/

        templHouseworkWc = '<a href="#finder-housework_#' + day + '" class="btn-edit">'
                            + '<div class="item" style="height:' + objDaylyplan[day].withChild.intervalHousework / divider + 'px">'
                            + '<div id="title">Hausarbeit<span class="time"><span class="icon pen"></span>' + minutesToString(objDaylyplan[day].withChild.intervalHousework) + '</span></div>'
                            + '<ul>' + liTemp + '</ul>'
                            + '</div>'
                            + '</a>';


        /*$.each(objDaylyplan[day].withChild.sparetime, function(activity, value){
            if(value.value > 0) liTemp += '<li>' + value.name + '</li>'
        });*/

        templSparetimeWc = '<a href="#finder-sparetime_#' + day + '" class="btn-edit">'
                            + '<div class="item" style="height:' + objDaylyplan[day].withChild.intervalSparetime / divider + 'px">'
                            + '<div id="title">Freizeit<span class="time"><span class="icon pen"></span>' + minutesToString(objDaylyplan[day].withChild.intervalSparetime) + '</span></div>'
                            + '<ul>' + liTemp + '</ul>'
                            + '</div>'
                            + '</a>';



        templHobbychildWc = '<a href="#finder-hobbychild_#' + day + '" class="btn-edit">'
                            + '<div class="item" style="height:' + objDaylyplan[day].withChild.hobbychild / divider + 'px">'
                            + '<div id="tiltle">Hobby Kind<span class="time"><span class="icon pen"></span>' + minutesToString(objDaylyplan[day].withChild.hobbychild) + '</span></div>'
                            + '</div>'
                            + '</a>';

        if (insel > 0) {
            var imgheight = (insel / divider) - 40;
            var iHeight = 0;

            if (imgheight > 130)
                iHeight = 92;
            if (imgheight <= 130 && imgheight > 95)
                iHeight = 70;
            if (imgheight <= 95 && imgheight > 75)
                iHeight = 50;
            if (imgheight <= 75 && imgheight > 55)
                iHeight = 30;
            if (imgheight <= 55)
                iHeight = 0;


            templInsel = '<div class="item insel" style="height:' + insel / divider + 'px">'
                        + '<div id="title">Insel<span class="time">' + minutesToString(insel) + '</span></div>'
                        + '<div class="img-insel" style="height:' + imgheight + 'px;"><div class="center"><img src="/fileadmin/template/media/insel.png"  style="width:' + iHeight + 'px; height:' + iHeight + 'px;"/></div></div>'
                        + '</div>';
        } else {
            templInsel = '<div class="item message">' + objDaylyplan[day].dayname + ' ist keine Inselzeit mehr übrig ...</div>';
        }


        if (objDaylyplan[day].withoutChild.worktime > 0) {
            templWorktimeWoc2 = '<div class="item" style="height:' + objDaylyplan[day].withoutChild.worktime / divider + 'px">'
                                + '<div id="title">Arbeiten<span class="time">' + minutesToString(objDaylyplan[day].withoutChild.worktime) + '</span></div>'
                                + '</div>';
        } else {
            templWorktimeWoc2 = '';
        }




        /*$.each(objDaylyplan[day].withoutChild.housework, function(activity, value){
            if(value.value > 0) liTemp += '<li>' + value.name + '</li>'
        });*/
        if (objDaylyplan[day].withoutChild.intervalHousework > 0) {
            templHouseworkWoc2 = '<div class="item" style="height:' + objDaylyplan[day].withoutChild.intervalHousework / divider + 'px">'
                                + '<div id="title">Hausarbeit<span class="time">' + minutesToString(objDaylyplan[day].withoutChild.intervalHousework) + '</span></div>'
                                + '<ul>' + liTemp + '</ul>'
                                + '</div>';

        } else {
            templHouseworkWoc2 = '';
        }




        /* $.each(objDaylyplan[day].withoutChild.sparetime, function(activity, value){
            if(value.value > 0) liTemp += '<li>' + value.name + '</li>'
        });*/

        if (objDaylyplan[day].withoutChild.intervalSparetime > 0) {
            templSparetimeWoc2 = '<div class="item" style="height:' + objDaylyplan[day].withoutChild.intervalSparetime / divider + 'px">'
                                + '<div id="title">Freizeit<span class="time">' + minutesToString(objDaylyplan[day].withoutChild.intervalSparetime) + '</span></div>'
                                + '<ul>' + liTemp + '</ul>'
                                + '</div>';

        } else {
            templSparetimeWoc2 = '';
        }



        if (objDaylyplan[day].withoutChild.hobbychild > 0) {
            templHobbychildWoc2 = '<div class="item" style="height:' + objDaylyplan[day].withoutChild.hobbychild / divider + 'px">'
                                    + '<div id="title">Hobby Kind<span class="time">' + minutesToString(objDaylyplan[day].withoutChild.hobbychild) + '</span></div>'
                                    + '</div>';
        } else {
            templHobbychildWoc2 = '';
        }



        /*$.each(objDaylyplan[day].withChild.housework, function(activity, value){
            if(value.value > 0) liTemp += '<li>' + value.name + '</li>'
        });*/

        if (objDaylyplan[day].withChild.intervalHousework > 0) {
            templHouseworkWc2 = '<div class="item" style="height:' + objDaylyplan[day].withChild.intervalHousework / divider + 'px">'
                                + '<div id="title">Hausarbeit<span class="time">' + minutesToString(objDaylyplan[day].withChild.intervalHousework) + '</span></div>'
                                + '<ul>' + liTemp + '</ul>'
                                + '</div>';
        } else {
            templHouseworkWc2 = '';
        }



        /*$.each(objDaylyplan[day].withChild.sparetime, function(activity, value){
            if(value.value > 0) liTemp += '<li>' + value.name + '</li>'
        });*/

        if (objDaylyplan[day].withChild.intervalSparetime > 0) {
            templSparetimeWc2 = '<div class="item" style="height:' + objDaylyplan[day].withChild.intervalSparetime / divider + 'px">'
                                + '<div id="title">Freizeit<span class="time">' + minutesToString(objDaylyplan[day].withChild.intervalSparetime) + '</span></div>'
                                + '<ul>' + liTemp + '</ul>'
                                + '</div>';
        } else {
            templSparetimeWc2 = '';
        }



        if (objDaylyplan[day].withChild.hobbychild > 0) {
            templHobbychildWc2 = '<div class="item" style="height:' + objDaylyplan[day].withChild.hobbychild / divider + 'px">'
                                + '<div id="tiltle">Hobby Kind<span class="time">' + minutesToString(objDaylyplan[day].withChild.hobbychild) + '</span></div>'
                                + '</div>';
        } else {
            templHobbychildWc2 = '';
        }



        if (param == 'easy') {

            templ += '<div class="day-item" data-name=' + day + '>'
                    + '<div class="title-day">' + value.dayname + '</div>'
                    + '<div class="toggle-item">'
                    + '<div class="section without-child">'
                    + '<div class="section-title">' + strWithoutChild + '<span class="time">' + withoutChildTime + '</span></div>'
                    + '<a href="#' + day + '" class="edit" style="height: ' + withoutChild / 4 + 'px;"><span class="icon pen"></span></a>'
                    + '</div>'
                    + '<div class="section with-child">'
                    + '<div class="section-title">' + strWithChild + '<span class="time">' + withChildTime + '</span></div>'
                    + '<a href="#' + day + '" class="edit" style="height: ' + withChild / 4 + 'px;"><span class="icon pen"></span></a>'
                    + '</div>'
                    + '</div>'
                    + '</div>';
        }

        if (param == 'opt') {

            contentWoc = templWorktimeWoc + templHouseworkWoc + templSparetimeWoc + templHobbychildWoc + templRestWoc;
            contentWc = templHouseworkWc + templSparetimeWc + templHobbychildWc + templInsel;

            templ += '<div class="day-item" data-name=' + day + '>'
                    + '<div class="title-day">' + value.dayname + '</div>'
                    + '<div class="toggle-item">'
                    + '<div class="section without-child">'
                    + '<div class="section-title">' + strWithoutChild + '<span class="time">' + withoutChildTime + '</span></div>'
                    + contentWoc
                    + '</div>'
                    + '<div class="section with-child">'
                    + '<div class="section-title">' + strWithChild + '<span class="time">' + withChildTime + '</span></div>'
                    + contentWc
                    + '</div>'
                    + '</div>'
                    + '</div>';
        }

        if (param == 'eval') {

            contentWoc2 = templWorktimeWoc2 + templHouseworkWoc2 + templSparetimeWoc2 + templHobbychildWoc2 + templRestWoc;
            contentWc2 = templHouseworkWc2 + templSparetimeWc2 + templHobbychildWc2 + templInsel;

            templ += '<div class="day-item" data-name=' + day + '>'
                    + '<div class="title-day">' + value.dayname + '</div>'
                    + '<div class="toggle-item">'
                    + '<div class="section without-child">'
                    + '<div class="section-title">' + strWithoutChild + '<span class="time">' + withoutChildTime + '</span></div>'
                    + contentWoc2
                    + '</div>'
                    + '<div class="section with-child">'
                    + '<div class="section-title">' + strWithChild + '<span class="time">' + withChildTime + '</span></div>'
                    + contentWc2
                    + '</div>'
                    + '</div>'
                    + '</div>';
        }

    });

    return templ;
}

// init Templates
// ************************************************************
// ************************************************************
// ************************************************************
var initWorktime = function (obj) {

    $('#worktime').hide();

    var templ = '';

    obj.each(function () {
        var day = $(this).attr('id');
        var intervalWoc = objDaylyplan[day].withoutChild.interval;
        var curIntervalWoc = getCurInterval(day, 'withoutChild');
        var ulWidth = curIntervalWoc / minUnit * widthLi;
        var templSelect = initSelectField('worktime', intervalWoc, curIntervalWoc, 'Arbeiten', 0);


        $(this).find('.row-fluid').append(quest);

        templ = '<div class="select-content without-child span6"  data-name="without-child">'
                + '<div class="title-select">' + strWithoutChild + '<span class="icon clock"></span><span class="time">' + minutesToString(curIntervalWoc) + '</span></div>'
                + templSelect
                + '</div>';

        var quest = '<div class="row-fluid for-quest"><div class="span12"><h4 class="quest left">Wie viel Stunden gehst du einer BERUFLICHEN TÄTIGKEIT nach, bis du dein Kind wiedersiehst?</h4><br/></div></div>';
        $(this).prepend(quest);
        $(this).find('.row-fluid.work').append(templ);

        if ($(this).attr('id') == 'monday') $(this).append(btnForAllDays)
    });

    $('#select-your-worktime input[type=radio]').prop('checked', false);
    $('#select-your-worktime input[type=radio]').click(function () {
        if ($("input[name='worktime']:checked").val() == 'yes')
            $('#worktime').fadeIn('fast');
        $('#finder-worktime #goto-finder-housework').fadeIn('fast');
        if ($("input[name='worktime']:checked").val() == 'no')
            $('#worktime').fadeOut('fast');
        $('#finder-worktime #goto-finder-housework').fadeIn('fast');
    });


    // ClickHandling
    $('#worktime a.prev, #worktime a.next').click(function () {

        //var dayItem = '#' + $(this).closest('.day-panel').attr('id');
        var day = $(this).closest('.day-panel').attr('id');
        var ulCurrent = '#' + $(this).closest('.day-panel').attr('id') + ' .select-content[data-name=' + $(this).closest('.select-content').data('name') + '] ul.' + $(this).attr('data-item');
        var section = '#' + $(this).closest('.day-panel').attr('id') + ' .select-content[data-name=' + $(this).closest('.select-content').data('name') + ']';
        var sectionName = $(this).closest('.select-content').data('name');
        var selectItem = $(this).attr('data-item');
        var activeLi = $(ulCurrent).find('li.active');
        var nextLi = activeLi.next();
        var prevLi = activeLi.prev();

        $('#worktime #monday .message').remove();

        if ($(this).hasClass('next')) {

            if (!activeLi.hasClass('last')) {

                activeLi.removeClass('active');
                nextLi.addClass('active');
                newPos = nextLi.data('id') * widthLi;
                $(ulCurrent).animate({ "left": -newPos }, 200);

                if (sectionName == 'without-child') {
                    objDaylyplan[day].withoutChild.worktime += minUnit;
                    curInterval = getCurInterval(day, 'withoutChild');
                    curIntervalNum = curInterval / minUnit;
                }
                $(section).find('.time').text(minutesToString(curInterval));
            }
            else {
                $(ulCurrent).effect("shake", "fast");
            }
        }

        if ($(this).hasClass('prev')) {

            if (!activeLi.hasClass('first')) {

                activeLi.removeClass('active');
                prevLi.addClass('active');
                newPos = prevLi.data('id') * widthLi;
                $(ulCurrent).animate({ "left": -newPos }, 200);

                if (sectionName == 'without-child') {
                    objDaylyplan[day].withoutChild.worktime -= minUnit;
                    curInterval = getCurInterval(day, 'withoutChild');
                    curIntervalNum = curInterval / minUnit;
                }
                $(section).find('.time').text(minutesToString(curInterval));
            }
            else {
                $(ulCurrent).effect("shake", "fast");
            }
        }

    });
    // Update alle Tage Screen Auswahl Worktime
    // ************************************************************
    $('#worktime #bfad').click(function () {
        $('#housework #monday .message').remove();
        bfadWorktime('#worktime .day-panel');
    });

    $('#goto-finder-housework').click(function (e) {
        e.preventDefault();
        updateWorktime();
        initHousework($('#housework .day-panel'));
        $finder.tabs('select', $(this).attr("href"));
        $(window).scrollTop($("body").offset().top);
    });

    $('#finder-worktime #goto-finder-map-opt.opt').click(function (e) {
        e.preventDefault();
        updateWorktime();
        $('#map-opt').find('.day-item').remove();
        $('#finder-map-opt #map-opt').append(drawMap('opt'));
        $finder.tabs('select', $(this).attr("href"));
        $(window).scrollTop($("body").offset().top);
        $('.arrow-bar li span').removeClass('current');
        $('.arrow-bar span[data-name=optimize]').addClass('current');

        $('#finder-map-opt #map-opt a.btn-edit').click(function (e) {
            e.preventDefault();
            updateAllItems();
            urlArray = $(this).attr('href').split('_');
            $finder.tabs('select', urlArray[0]);
            $(urlArray[0] + ' ul li a[href=' + urlArray[1] + ']').trigger("click");
            $(urlArray[0]).find('.cta-btn.next').hide();
            $(urlArray[0]).find('.cta-btn.opt').show();
            updateWorktimeItems()
        });

        if ($(window).width() < 767) {
            $('#map-opt .day-item[data-name=monday] .toggle-item').show();
            $('#map-opt .title-day').click(function () {
                $(this).next('.toggle-item').slideToggle('slow');
            });
        }

    });
}


var initHousework = function (obj) {
    var templ = '';

    obj.each(function () {
        var day = $(this).attr('id');
        var intervalWoc = objDaylyplan[day].withoutChild.interval;
        var intervalWc = objDaylyplan[day].withChild.interval;
        var curIntervalWoc = getCurInterval(day, 'withoutChild');
        var curIntervalWc = getCurInterval(day, 'withChild');
        var ulWidth = curIntervalWoc / minUnit * widthLi;
        var templHouseWorkWoc = "";
        var templHouseWorkWc = "";

        $.each(objDaylyplan[day].withoutChild.housework, function (activity, value) {
            templHouseWorkWoc += initSelectField(activity, intervalWoc, curIntervalWoc, value.name, 0);
        });

        $.each(objDaylyplan[day].withChild.housework, function (activity, value) {
            templHouseWorkWc += initSelectField(activity, intervalWc, curIntervalWc, value.name, 0);
        });

        templ = '<div class="select-content without-child span6" data-name="without-child">'
                + '<div class="title-select">' + strWithoutChild + '<span class="icon clock"></span><span class="time">' + minutesToString(curIntervalWoc) + '</span></div>'
                + templHouseWorkWoc
                + '</div>'
                + '<div class="select-content with-child span6" data-name="with-child">'
                + '<div class="title-select">' + strWithChild + '<span class="icon insel"></span><span class="time">' + minutesToString(curIntervalWc) + '</span></div>'
                + templHouseWorkWc
                + '</div>';

        $(this).find('.row-fluid').append(templ);

        if ($(this).attr('id') == 'monday') $(this).append(btnForAllDays)
    });


    // ClickHandling
    $('#housework a.prev, #housework a.next').click(function (click) {

        $('#housework #monday .message').remove();

        //var dayItem = '#' + $(this).closest('.day-panel').attr('id');
        var day = $(this).closest('.day-panel').attr('id');
        var ulCurrent = '#' + $(this).closest('.day-panel').attr('id') + ' .select-content[data-name=' + $(this).closest('.select-content').data('name') + '] ul.' + $(this).attr('data-item');
        var section = '#' + $(this).closest('.day-panel').attr('id') + ' .select-content[data-name=' + $(this).closest('.select-content').data('name') + ']';
        var sectionName = $(this).closest('.select-content').data('name');
        var selectItem = $(this).attr('data-item');
        var activeLi = $(ulCurrent).find('li.active');
        var nextLi = activeLi.next();
        var prevLi = activeLi.prev();

        if ($(this).hasClass('next')) {

            if (!activeLi.hasClass('last')) {

                activeLi.removeClass('active');
                nextLi.addClass('active');
                newPos = nextLi.data('id') * widthLi;
                $(ulCurrent).animate({ "left": -newPos }, 200);

                if (sectionName == 'without-child') {
                    objDaylyplan[day].withoutChild.intervalHousework += minUnit;
                    curInterval = getCurInterval(day, 'withoutChild');
                    curIntervalNum = curInterval / minUnit;

                    $('#' + day + ' .without-child ul').each(function () {
                        if (!$(this).hasClass(selectItem)) {
                            last = $(this).find('li.last');
                            newLastId = $(this).find('li.active').data('id') + curIntervalNum;
                            newLast = $(this).find('li[data-id=' + newLastId + ']');
                            last.removeClass('last');
                            newLast.addClass('last');
                        }
                    });
                }

                if (sectionName == 'with-child') {
                    objDaylyplan[day].withChild.intervalHousework += minUnit;
                    curInterval = getCurInterval(day, 'withChild');
                    curIntervalNum = curInterval / minUnit;

                    $('#' + day + ' .with-child ul').each(function () {
                        if (!$(this).hasClass(selectItem)) {
                            last = $(this).find('li.last');
                            newLastId = $(this).find('li.active').data('id') + curIntervalNum;
                            newLast = $(this).find('li[data-id=' + newLastId + ']');
                            last.removeClass('last');
                            newLast.addClass('last');
                        }
                    });
                }
                $(section).find('.time').text(minutesToString(curInterval));
            }
            else {
                $(ulCurrent).effect("shake", "fast");
            }
        }

        if ($(this).hasClass('prev')) {

            if (!activeLi.hasClass('first')) {

                activeLi.removeClass('active');
                prevLi.addClass('active');
                newPos = prevLi.data('id') * widthLi;
                $(ulCurrent).animate({ "left": -newPos }, 200);

                if (sectionName == 'without-child') {
                    objDaylyplan[day].withoutChild.intervalHousework -= minUnit;
                    curInterval = getCurInterval(day, 'withoutChild');
                    curIntervalNum = curInterval / minUnit;

                    $('#' + day + ' .without-child ul').each(function () {
                        if (!$(this).hasClass(selectItem)) {
                            last = $(this).find('li.last');
                            newLastId = $(this).find('li.active').data('id') + curIntervalNum;
                            newLast = $(this).find('li[data-id=' + newLastId + ']');
                            last.removeClass('last');
                            newLast.addClass('last');
                        }
                    });
                }

                if (sectionName == 'with-child') {
                    objDaylyplan[day].withChild.intervalHousework -= minUnit;
                    curInterval = getCurInterval(day, 'withChild');
                    curIntervalNum = curInterval / minUnit;

                    $('#' + day + ' .with-child ul').each(function () {
                        if (!$(this).hasClass(selectItem)) {
                            last = $(this).find('li.last');
                            newLastId = $(this).find('li.active').data('id') + curIntervalNum;
                            newLast = $(this).find('li[data-id=' + newLastId + ']');
                            last.removeClass('last');
                            newLast.addClass('last');
                        }
                    });
                }
                $(section).find('.time').text(minutesToString(curInterval));
            }
            else {
                $(ulCurrent).effect("shake", "fast");
            }
        }

    });

    // Update alle Tage Screen Auswahl Worktime
    // ************************************************************
    $('#housework #bfad').click(function () {
        $('#housework #monday .message').remove();
        bfadHousework('#housework .day-panel');
    });

    $('#goto-finder-sparetime').click(function (e) {
        e.preventDefault();
        updateHousework();
        initSparetime($('#sparetime .day-panel'));
        $finder.tabs('select', $(this).attr("href"));
        $(window).scrollTop($("body").offset().top);
    });

    $('#finder-housework #goto-finder-map-opt.opt').click(function (e) {
        e.preventDefault();
        updateHousework();
        $('#map-opt').find('.day-item').remove();
        $('#finder-map-opt #map-opt').append(drawMap('opt'));
        $finder.tabs('select', $(this).attr("href"));
        $(window).scrollTop($("body").offset().top);

        $('.arrow-bar li span').removeClass('current');
        $('.arrow-bar span[data-name=optimize]').addClass('current');

        $('#finder-map-opt #map-opt a.btn-edit').click(function (e) {
            e.preventDefault();
            updateAllItems();
            urlArray = $(this).attr('href').split('_');
            $finder.tabs('select', urlArray[0]);
            $(urlArray[0] + ' ul li a[href=' + urlArray[1] + ']').trigger("click");
            $(urlArray[0]).find('.cta-btn.next').hide();
            $(urlArray[0]).find('.cta-btn.opt').show();
        });

        if ($(window).width() < 767) {
            $('#map-opt .day-item[data-name=monday] .toggle-item').show();
            $('#map-opt .title-day').click(function () {
                $(this).next('.toggle-item').slideToggle('slow');
            });
        }
    });
}

var initSparetime = function (obj) {
    var templ = '';

    obj.each(function () {
        var day = $(this).attr('id');
        var intervalWoc = objDaylyplan[day].withoutChild.interval;
        var intervalWc = objDaylyplan[day].withChild.interval;
        var curIntervalWoc = getCurInterval(day, 'withoutChild');
        var curIntervalWc = getCurInterval(day, 'withChild');
        var ulWidth = curIntervalWoc / minUnit * widthLi;
        var templSparetimeWoc = "";
        var templSparetimeWc = "";

        $.each(objDaylyplan[day].withoutChild.sparetime, function (activity, value) {
            templSparetimeWoc += initSelectField(activity, intervalWoc, curIntervalWoc, value.name, 0);
        });

        $.each(objDaylyplan[day].withChild.sparetime, function (activity, value) {
            templSparetimeWc += initSelectField(activity, intervalWc, curIntervalWc, value.name, 0);
        });


        templ = '<div class="select-content without-child span6" data-name="without-child">'
                + '<div class="title-select">' + strWithoutChild + '<span class="icon clock"></span><span class="time">' + minutesToString(curIntervalWoc) + '</span></div>'
                + templSparetimeWoc
                + '</div>'
                + '<div class="select-content with-child span6" data-name="with-child">'
                + '<div class="title-select">' + strWithChild + '<span class="icon insel"></span><span class="time">' + minutesToString(curIntervalWc) + '</span></div>'
                + templSparetimeWc
                + '</div>';

        $(this).find('.row-fluid').append(templ);

        if ($(this).attr('id') == 'monday') $(this).append(btnForAllDays)
    });

    // ClickHandling
    $('#sparetime a.prev, #sparetime a.next').click(function () {

        $('#sparetime #monday .message').remove();

        //var dayItem = '#' + $(this).closest('.day-panel').attr('id');
        var day = $(this).closest('.day-panel').attr('id');
        var ulCurrent = '#' + $(this).closest('.day-panel').attr('id') + ' .select-content[data-name=' + $(this).closest('.select-content').data('name') + '] ul.' + $(this).attr('data-item');
        var section = '#' + $(this).closest('.day-panel').attr('id') + ' .select-content[data-name=' + $(this).closest('.select-content').data('name') + ']';
        var sectionName = $(this).closest('.select-content').data('name');
        var selectItem = $(this).attr('data-item');
        var activeLi = $(ulCurrent).find('li.active');
        var nextLi = activeLi.next();
        var prevLi = activeLi.prev();

        if ($(this).hasClass('next')) {

            if (!activeLi.hasClass('last')) {

                activeLi.removeClass('active');
                nextLi.addClass('active');
                newPos = nextLi.data('id') * widthLi;
                $(ulCurrent).animate({ "left": -newPos }, 200);

                if (sectionName == 'without-child') {
                    objDaylyplan[day].withoutChild.intervalSparetime += minUnit;
                    curInterval = getCurInterval(day, 'withoutChild');
                    curIntervalNum = curInterval / minUnit;

                    $('#' + day + ' .without-child ul').each(function () {
                        if (!$(this).hasClass(selectItem)) {
                            last = $(this).find('li.last');
                            newLastId = $(this).find('li.active').data('id') + curIntervalNum;
                            newLast = $(this).find('li[data-id=' + newLastId + ']');
                            last.removeClass('last');
                            newLast.addClass('last');
                        }
                    });
                }

                if (sectionName == 'with-child') {
                    objDaylyplan[day].withChild.intervalSparetime += minUnit;
                    curInterval = getCurInterval(day, 'withChild');
                    curIntervalNum = curInterval / minUnit;

                    $('#' + day + ' .with-child ul').each(function () {
                        if (!$(this).hasClass(selectItem)) {

                            last = $(this).find('li.last');
                            newLastId = $(this).find('li.active').data('id') + curIntervalNum;
                            newLast = $(this).find('li[data-id=' + newLastId + ']');
                            last.removeClass('last');
                            newLast.addClass('last');
                        }
                    });
                }
                $(section).find('.time').text(minutesToString(curInterval));
            }
            else {
                $(ulCurrent).effect("shake", "fast");
            }
        }

        if ($(this).hasClass('prev')) {

            if (!activeLi.hasClass('first')) {

                activeLi.removeClass('active');
                prevLi.addClass('active');
                newPos = prevLi.data('id') * widthLi;
                $(ulCurrent).animate({ "left": -newPos }, 200);

                if (sectionName == 'without-child') {
                    objDaylyplan[day].withoutChild.intervalSparetime -= minUnit;
                    curInterval = getCurInterval(day, 'withoutChild');
                    curIntervalNum = curInterval / minUnit;

                    $('#' + day + ' .without-child ul').each(function () {
                        if (!$(this).hasClass(selectItem)) {

                            last = $(this).find('li.last');
                            newLastId = $(this).find('li.active').data('id') + curIntervalNum;
                            newLast = $(this).find('li[data-id=' + newLastId + ']');
                            last.removeClass('last');
                            newLast.addClass('last');
                        }
                    });
                }

                if (sectionName == 'with-child') {
                    objDaylyplan[day].withChild.intervalSparetime -= minUnit;
                    curInterval = getCurInterval(day, 'withChild');
                    curIntervalNum = curInterval / minUnit;

                    $('#' + day + ' .with-child ul').each(function () {
                        if (!$(this).hasClass(selectItem)) {

                            last = $(this).find('li.last');
                            newLastId = $(this).find('li.active').data('id') + curIntervalNum;
                            newLast = $(this).find('li[data-id=' + newLastId + ']');
                            last.removeClass('last');
                            newLast.addClass('last');
                        }
                    });
                }
                $(section).find('.time').text(minutesToString(curInterval));
            }
            else {
                $(ulCurrent).effect("shake", "fast");
            }
        }

    });

    // Update alle Tage Screen Auswahl Freizeit
    // ************************************************************
    $('#sparetime #bfad').click(function () {
        //Setze alle Tage auf den selben Wert
        $('#sparetime #monday .message').remove();
        bfadSparetime('#sparetime .day-panel');

    });

    $('#goto-finder-hobbychild').click(function (e) {
        e.preventDefault();
        updateSparetime();
        initHobbychild($('#hobbychild .day-panel'));
        $finder.tabs('select', $(this).attr("href"));
        $(window).scrollTop($("body").offset().top);
    });

    $('#finder-sparetime #goto-finder-map-opt.opt').click(function (e) {
        e.preventDefault();
        updateSparetime();
        $('#map-opt').find('.day-item').remove();
        $('#finder-map-opt #map-opt').append(drawMap('opt'));
        $finder.tabs('select', $(this).attr("href"));
        $(window).scrollTop($("body").offset().top);

        $('.arrow-bar li span').removeClass('current');
        $('.arrow-bar span[data-name=optimize]').addClass('current');

        $('#finder-map-opt #map-opt a.btn-edit').click(function (e) {
            e.preventDefault();
            updateAllItems();
            urlArray = $(this).attr('href').split('_');
            $finder.tabs('select', urlArray[0]);
            $(urlArray[0] + ' ul li a[href=' + urlArray[1] + ']').trigger("click");
            $(urlArray[0]).find('.cta-btn.next').hide();
            $(urlArray[0]).find('.cta-btn.opt').show();
        });

        if ($(window).width() < 767) {
            $('#map-opt .title-day').click(function () {
                $('#map-opt .day-item[data-name=monday] .toggle-item').show();
                $(this).next('.toggle-item').slideToggle('slow');
            });
        }
    });


}

var initHobbychild = function (obj) {

    $('#hobbychild').hide();
    $('#finder-hobbychild #goto-finder-map-opt.next').hide();

    var templ = '';

    obj.each(function () {
        var day = $(this).attr('id');
        var intervalWoc = objDaylyplan[day].withoutChild.interval;
        var intervalWc = objDaylyplan[day].withChild.interval;
        var curIntervalWoc = getCurInterval(day, 'withoutChild');
        var curIntervalWc = getCurInterval(day, 'withChild');
        var ulWidth = curIntervalWoc / minUnit * widthLi;
        var templHobbychildWoc = initSelectField('hobbychild', intervalWoc, curIntervalWoc, 'Hobby Kind', 0);
        var templHobbychildWc = initSelectField('hobbychild', intervalWc, curIntervalWc, 'Hobby Kind', 0);

        templ = '<div class="span8">'
                + '<div class="select-content without-child"  data-name="without-child">'
                + '<div class="title-select">' + strWithoutChild + '<span class="icon clock"></span><span class="time">' + minutesToString(curIntervalWoc) + '</span></div>'
                + templHobbychildWoc
                + '</div>'
                + '<div class="select-content with-child"  data-name="with-child">'
                + '<div class="title-select">' + strWithChild + '<span class="icon clock"></span><span class="time">' + minutesToString(curIntervalWc) + '</span></div>'
                + templHobbychildWc
                + '</div>'
                + '<div id="escort">'
                + '<h3>Begleitest du dein Kind?</h3>'
                + '<form class="select-child-escort">'
                + '<input type="radio" name="hobbychild-escort" value="yes" checked=""> Ja'
                + '<input type="radio" name="hobbychild-escort" value="no"> Nein'
                + '</form>'
                + '</div>'
                + '</div>'
                + '<div class="span4">'
                + '<div class="info woc">Diese Zeit wird dir nun dem Bereich „Zeit ohne Kind“ zugeordnet, in der du Aufgaben erledigen kannst, um gemeinsame Zeit mit dem Kind zu gewinnen.</div>'
                + '<div class="info wc">Diese Zeit wird dir nun dem Bereich „Zeit mit Kind“ zugeordnet, die du gemeinsam mit deinem Kind nutzt. </div>'
                + '</div>';

        $(this).find('.row-fluid').append(templ);

        if ($(this).attr('id') == 'monday') $(this).append(btnForAllDays)
    });

    //$('#hobbychild .select-content.with-child').hide();
    $('#select-child-hobby input[type=radio]').prop('checked', false);
    $('#select-child-hobby input[type=radio]').click(function () {
        if ($("input[name='hobbychild']:checked").val() == 'yes') {
            $('#hobbychild').fadeIn('fast');

            if ($('#finder-hobbychild #goto-finder-map-opt.opt').not(':visible'))
                $('#finder-hobbychild #goto-finder-map-opt.next').fadeIn('fast');
        }

        if ($("input[name='hobbychild']:checked").val() == 'no') {
            $('#hobbychild').fadeOut('fast');

            if ($('#finder-hobbychild #goto-finder-map-opt.opt').not(':visible'))
                $('#finder-hobbychild #goto-finder-map-opt.next').fadeIn('fast');
        }

    });

    $('#hobbychild .select-content.without-child, #hobbychild .info.woc').hide();
    //$('.select-child-escort input[type=radio]').prop('checked', false);
    $('.select-child-escort').click(function () {


        area = '#hobbychild #' + $(this).closest('.day-panel').attr('id');
        day = $(this).closest('.day-panel').attr('id');
        $(area).find('#escort .advice').remove();


        if ($(area).find("input:checked").val() == 'yes') {
            $(area + ' .select-content.without-child').hide();
            $(area + ' .select-content.with-child').show();
            $(area + ' .info.wc').fadeIn('fast');
            $(area + ' .info.woc').hide();
        }

        if ($(area).find("input:checked").val() == 'no') {

            $(area + ' .select-content.without-child').show();
            $(area + ' .select-content.with-child').hide();
            $(area + ' .info.wc').hide();
            $(area + ' .info.woc').fadeIn('fast');

            $(area).find('ul li.active').removeClass('active');
            $(area).find('ul li[data-id=0]').addClass('active');
            $(area).find('ul').css({ 'left': 0 });
        }

        $(area).find('ul li.active').removeClass('active');
        $(area).find('ul li[data-id=0]').addClass('active');
        $(area).find('ul').css({ 'left': 0 });
        objDaylyplan[day].withoutChild.hobbychild = 0;
        objDaylyplan[day].withChild.hobbychild = 0;
        $(area).find('.select-content.without-child .time').text(minutesToString(getCurInterval(day, 'withoutChild')));
        $(area).find('.select-content.with-child .time').text(minutesToString(getCurInterval(day, 'withChild')));

        $(area).find('#escort').append('<span class="advice">Bitte geben Sie die Zeit erneut ein.</span>')
    });

    // ClickHandling
    $('#hobbychild a.prev, #hobbychild a.next').click(function () {

        $('#hobbychild #monday .message').remove();
        $('#hobbychild').find('#escort .advice').remove();

        //var dayItem = '#' + $(this).closest('.day-panel').attr('id');
        var day = $(this).closest('.day-panel').attr('id');
        var ulCurrent = '#' + $(this).closest('.day-panel').attr('id') + ' .select-content[data-name=' + $(this).closest('.select-content').data('name') + '] ul.' + $(this).attr('data-item');
        var section = '#' + $(this).closest('.day-panel').attr('id') + ' .select-content[data-name=' + $(this).closest('.select-content').data('name') + ']';
        var sectionName = $(this).closest('.select-content').data('name');
        var selectItem = $(this).attr('data-item');
        var activeLi = $(ulCurrent).find('li.active');
        var nextLi = activeLi.next();
        var prevLi = activeLi.prev();


        if ($(this).hasClass('next')) {

            if (!activeLi.hasClass('last')) {

                activeLi.removeClass('active');
                nextLi.addClass('active');
                newPos = nextLi.data('id') * widthLi;
                $(ulCurrent).animate({ "left": -newPos }, 200);

                if (sectionName == 'without-child') {
                    objDaylyplan[day].withoutChild.hobbychild += minUnit;
                    curInterval = getCurInterval(day, 'withoutChild');
                    curIntervalNum = curInterval / minUnit;
                }

                if (sectionName == 'with-child') {
                    objDaylyplan[day].withChild.hobbychild += minUnit;
                    curInterval = getCurInterval(day, 'withChild');
                    curIntervalNum = curInterval / minUnit;
                }
                $(section).find('.time').text(minutesToString(curInterval));
            }
            else {
                $(ulCurrent).effect("shake", "fast");
            }
        }

        if ($(this).hasClass('prev')) {

            if (!activeLi.hasClass('first')) {

                activeLi.removeClass('active');
                prevLi.addClass('active');
                newPos = prevLi.data('id') * widthLi;
                $(ulCurrent).animate({ "left": -newPos }, 200);

                if (sectionName == 'without-child') {
                    objDaylyplan[day].withoutChild.hobbychild -= minUnit;
                    curInterval = getCurInterval(day, 'withoutChild');
                    curIntervalNum = curInterval / minUnit;
                }

                if (sectionName == 'with-child') {
                    objDaylyplan[day].withChild.hobbychild -= minUnit;
                    curInterval = getCurInterval(day, 'withChild');
                    curIntervalNum = curInterval / minUnit;
                }
                $(section).find('.time').text(minutesToString(curInterval));
            }
            else {
                $(ulCurrent).effect("shake", "fast");
            }
        }

    });

    // Update alle Tage Screen Auswahl Worktime
    // ************************************************************
    $('#hobbychild #bfad').click(function () {
        //Setze alle Tage auf den selben Wert
        $('#hobbychild #monday .message').remove();
        bfadHobbychild('#hobbychild .day-panel');

    });

    $('#finder-hobbychild #goto-finder-map-opt.next').click(function (e) {
        e.preventDefault();
        updateHobbychild();
        $('#map-opt').find('.day-item').remove();
        $('#finder-map-opt #map-opt').append(drawMap('opt'));
        $finder.tabs('select', $(this).attr("href"));
        $(window).scrollTop($("body").offset().top);
        $('.arrow-bar li span').removeClass('current');
        $('.arrow-bar span[data-name=optimize]').addClass('current');

        $('#map-opt a.btn-edit').click(function (e) {
            e.preventDefault();
            updateAllItems();
            urlArray = $(this).attr('href').split('_');

            $finder.tabs('select', urlArray[0]);

            $(urlArray[0] + ' ul li a[href=' + urlArray[1] + ']').trigger("click");
            $('#map-opt').find('.day-item').remove();
            $(urlArray[0]).find('.cta-btn.next').hide();
            $(urlArray[0]).find('.cta-btn.opt').show();
        });

        if ($(window).width() < 767) {
            $('#map-opt .day-item[data-name=monday] .toggle-item').show();
            $('#map-opt .title-day').click(function () {
                $(this).next('.toggle-item').slideToggle('slow');
            });
        }

        ms.trackEvent('Inselfinder - Optimierung', '/inselfinder/optimierung');

    });

    $('#finder-hobbychild #goto-finder-map-opt.opt').click(function (e) {
        e.preventDefault();
        updateHobbychild();
        $('#map-opt').find('.day-item').remove();
        $('#finder-map-opt #map-opt').append(drawMap('opt'));

        $finder.tabs('select', $(this).attr("href"));
        $(window).scrollTop($("body").offset().top);
        $('.arrow-bar li span').removeClass('current');
        $('.arrow-bar span[data-name=optimize]').addClass('current');

        $('#finder-map-opt #map-opt a.btn-edit').click(function (e) {
            e.preventDefault();
            updateAllItems();
            urlArray = $(this).attr('href').split('_');
            $finder.tabs('select', urlArray[0]);
            $(urlArray[0] + ' ul li a[href=' + urlArray[1] + ']').trigger("click");
            $(urlArray[0]).find('.cta-btn.next').hide();
            $(urlArray[0]).find('.cta-btn.opt').show();
        });

        if ($(window).width() < 767) {
            $('#map-opt .day-item[data-name=monday] .toggle-item').show();
            $('#map-opt .title-day').click(function () {
                $(this).next('.toggle-item').slideToggle('slow');
            });
        }
    });
}


// Global Vars
// ************************************************************
// ************************************************************
// ************************************************************
var objDaylyplan = new TimePlan();

var minUnit = 30;       // in 30er Schritten
var startTime = 6 * 60; // 6Uhr
var endTime = 22 * 60;  // 22Uhr
var timeInterval = endTime - startTime;

var widthLi = 200;
var divider = 1.25;
var effectStyle = 'fade';
var effectDuration = '200';
var mScreen = 768;

// TemplatesSettings
// ************************************************************
// ************************************************************
// ************************************************************
var strWithoutChild = "Zeit ohne Kind";
var strWithChild = "Zeit mit Kind";

var strChildLeave = "Wann verl&auml;sst dein Kind das Haus?";
var strChildHome = "Wann siehst du dein Kind nachmittags wieder?";
var strChildBed = "Wann geht dein Kind zu Bett?";

var strBfad = '<span class="red">Es wurden alle Zeiten übernommen.</span>';
var strBfadOverlay = '<span class="red">Daten konnten nicht für alle Tage übernommen werden. Überprüfen Sie, ob an den anderen Tagen genügend Zeit zur Verfügung steht:</span>';

var btnForAllDays = '<a class="btn btn-secondary" id="bfad">Für alle Wochentage übernehmen<span class="arrow"></span></a>';

var templSecSelectTime = '<div class="select-content child-leave span4">'
                        + '<h4 class="quest">' + strChildLeave + '</h4>'
                        + '</div>'
                        + '<div class="select-content child-home span4">'
                        + '<h4 class="quest">' + strChildHome + '</h4>'
                        + '</div>'
                        + '<div class="select-content child-bed span4">'
                        + '<h4 class="quest">' + strChildBed + '</h4>'
                        + '</div>';



$(function () {

    // Screen Intro
    // ************************************************************
    // ************************************************************
    // ************************************************************
    // Checke auf Cookie

    initPdfCreation();

    $('#section-inseltipps').hide();
    $('.cta-btn.opt').hide();
    $('#finder-worktime #goto-finder-housework').hide();
    $('#finder-hobbychild #goto-finder-map-opt.next').hide();

    $finder = initTabs('#finder');
    $('#goto-finder-select-time').click(function (e) {
        e.preventDefault();
        $finder.tabs('select', $(this).attr("href"));
        $('.arrow-bar span[data-name=time-tracking]').addClass('current');

        ms.trackEvent('Inselfinder - Zeiterfassung', '/inselfinder/zeiterfassung');
    });

    // Screen Auswahl Select Intervall
    // ************************************************************
    // ************************************************************
    // ************************************************************


    // Init
    $selectTimeDay = initTabs('#select-time');

    // vars
    dayPanelSelectTime = $("#select-time .day-panel");

    dayPanelSelectTime.each(function () {
        $(this).find('.row-fluid').append(templSecSelectTime);
        $(this).find('.child-leave').append(initSelectField('child-leave', timeInterval, timeInterval, '', startTime));
        $(this).find('.child-home').append(initSelectField('child-home', timeInterval, timeInterval, '', startTime));
        $(this).find('.child-bed').append(initSelectField('child-bed', timeInterval, timeInterval, '', startTime));

        if ($(this).attr('id') == 'monday') $(this).append(btnForAllDays)
    });

    // Falls gespeicherte Daten
    updateTimeSelectItems();


    dayPanelSelectTime = $("#select-time .day-panel");

    // Clickhandling Screen Auswahl Select Intervall
    // ************************************************************
    $('#select-time a.prev, #select-time a.next').click(function () {
        $('#select-time #monday .message').remove();

        day = '#' + $(this).closest('.day-panel').attr('id');
        ulCurrent = '#' + $(this).closest('.day-panel').attr('id') + ' ul.' + $(this).attr('data-item');

        activeLi = $(ulCurrent).find('li.active');
        nextLi = activeLi.next();
        prevLi = activeLi.prev();

        if ($(this).hasClass('next')) {

            if (!activeLi.hasClass('last')) {

                activeLi.removeClass('active');
                nextLi.addClass('active');
                newPos = nextLi.data('id') * widthLi;
                $(ulCurrent).animate({ "left": -newPos }, 200);

                if ($(ulCurrent).hasClass('child-leave')) {

                    // Checke ob kleiner
                    if (nextLi.data('id') > $(day + ' .child-home li.first').data('id')) {
                        if ($(day + ' .child-home li.first').hasClass('active')) {
                            cur = $(day + ' .child-home li.first');
                            cur.removeClass('first active');
                            cur.next().addClass('first active');
                            $(day + ' ul.child-home').animate({ "left": -newPos }, 200);
                        } else {
                            cur = $(day + ' .child-home li.first');
                            cur.removeClass('first');
                            cur.next().addClass('first');
                        }

                    }

                    // Checke ob kleiner
                    if (nextLi.data('id') > $(day + ' .child-bed li.first').data('id')) {
                        if ($(day + ' .child-bed li.first').hasClass('active')) {
                            cur = $(day + ' .child-bed li.first');
                            cur.removeClass('first active');
                            cur.next().addClass('first active');
                            $(day + ' ul.child-bed').animate({ "left": -newPos }, 200);
                        } else {
                            cur = $(day + ' .child-bed li.first');
                            cur.removeClass('first');
                            cur.next().addClass('first');
                        }

                    }
                }// if

                if ($(ulCurrent).hasClass('child-home')) {

                    // Checke ob kleiner
                    if (nextLi.data('id') > $(day + ' .child-bed li.first').data('id')) {
                        if ($(day + ' .child-bed li.first').hasClass('active')) {
                            cur = $(day + ' .child-bed li.first');
                            cur.removeClass('first active');
                            cur.next().addClass('first active');
                            $(day + ' ul.child-bed').animate({ "left": -newPos }, 200);
                        } else {
                            cur = $(day + ' .child-bed li.first');
                            cur.removeClass('first');
                            cur.next().addClass('first');
                        }

                    }

                }// if

            }// if last
            else {
                $(ulCurrent).effect("shake", "fast");
            }

        }// if next

        if ($(this).hasClass('prev')) {

            if (!activeLi.hasClass('first')) {
                activeLi.removeClass('active');
                prevLi.addClass('active');
                newPos = prevLi.data('id') * widthLi;
                $(ulCurrent).animate({ "left": -newPos }, 200);

                if ($(ulCurrent).hasClass('child-leave')) {
                    cur = $(day + ' .child-home li.first');
                    cur.removeClass('first');
                    cur.prev().addClass('first');
                }// if

                if ($(ulCurrent).hasClass('child-home')) {
                    cur = $(day + ' .child-bed li.first');
                    cur.removeClass('first');
                    cur.prev().addClass('first');
                }// if

            }// if first
            else {
                $(ulCurrent).effect("shake", "fast");
            }

        }// if prev
    });

    // Update alle Tage Screen Auswahl Select Intervall
    // ************************************************************
    $('#select-time #bfad').click(function () {

        childLeaveId = $('#monday .child-leave li.active').data('id');
        childHomeId = $('#monday .child-home li.active').data('id');
        childBedId = $('#monday .child-bed li.active').data('id');

        //Setze alle Tage auf den selben Wert
        bfadSelectTime(childLeaveId, childHomeId, childBedId);
    });

    $('#goto-map').click(function (e) {
        e.preventDefault();
        updateDay();
        $('#finder-map #map').append(drawMap('easy'));
        $finder.tabs('select', $(this).attr("href"));
        $(window).scrollTop($("body").offset().top);

        if ($(window).width() < 767) {
            $('#map .day-item[data-name=monday] .toggle-item').show();
            $('#map .title-day').click(function () {
                $(this).next('.toggle-item').slideToggle('slow');
            });
        }

        $('a.edit').click(function (e) {
            e.preventDefault();
            $finder.tabs('select', '#finder-select-time');
            $('#map').find('.day-item').remove();
            $('#select-time ul li a[href=' + $(this).attr('href') + ']').trigger("click");
        });

    });

    $('#goto-select-time').click(function (e) {
        e.preventDefault();
        $finder.tabs('select', $(this).attr("href"));
        $('#finder-map #map .day-item').remove();
        $(window).scrollTop($("body").offset().top);
    });


    $('#goto-worktime').click(function (e) {
        e.preventDefault();
        initWorktime($('#worktime .day-panel'));
        $finder.tabs('select', $(this).attr("href"));
        $('.arrow-bar li span').removeClass('current');
        $('.arrow-bar span[data-name=day-activity]').addClass('current');
        $(window).scrollTop($("body").offset().top);

        ms.trackEvent('Inselfinder - Tagesaktivitaeten', '/inselfinder/tagesaktivitaeten');

    });

    $('#goto-map-eval').click(function (e) {
        e.preventDefault();

        // Feedback
        if ($.cookie('feedback_inselfinder') != 'true') {
            finderFeedback();
        }

        $('#finder-map-eval #map-eval').append(drawMap('eval'));
        $('#section-inseltipps').fadeIn('fast');
        $('.arrow-bar li span').removeClass('current');
        $('.arrow-bar span[data-name=evaluation]').addClass('current');
        $finder.tabs('select', $(this).attr("href"));
        $(window).scrollTop($("body").offset().top);

        if ($(window).width() < 767) {
            $('#map-eval .day-item[data-name=monday] .toggle-item').show();
            $('#map-eval .title-day').click(function () {
                $(this).next('.toggle-item').slideToggle('slow');
            });
        }

        ms.trackEvent('Inselfinder - Auswertung', '/inselfinder/auswertung');

    });


    $('#go-finder-map-opt').click(function (e) {
        e.preventDefault();
        $('#map-eval').find('.day-item').remove();
        $finder.tabs('select', $(this).attr("href"));
        $('#section-inseltipps').hide();
        $('.arrow-bar li span').removeClass('current');
        $('.arrow-bar span[data-name=optimize]').addClass('current');
        $(window).scrollTop($("body").offset().top);
        $('#goto-pdf-download').hide();
    });



    // Worktime
    // ************************************************************
    // ************************************************************
    // ************************************************************
    $worktimeDay = initTabs('#worktime');
    $houseworkDay = initTabs('#housework');
    $sparetimeDay = initTabs('#sparetime');
    $hobbychildDay = initTabs('#hobbychild');

});