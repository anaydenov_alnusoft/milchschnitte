var Brandung = Brandung || {};
Brandung.Component = Brandung.Component || {};

Brandung.Component.Schnittentest = (function() {

	var self = {
		settings: {
			// images need to be in following path (example):
			// fileadmin\media\alltagstypentest\schnittentest\male\1.png
			maleImages: ['1.png', '2.png', '3.png', '4.png'],

			// fileadmin\media\alltagstypentest\schnittentest\female\1.png
			femaleImages: ['1.png', '2.png', '3.png', '4.png'],
			results: {
				original: 'Du bist ein Original, genau wie die Milch-Schnitte. Du bleibst dir treu und folgst nicht gleich jedem Trend nur, weil er neu ist. Man erkennt dich an deinen Markenzeichen und liebt dich für deine authentische Art.<br><br>Auch wenn du mal aneckst, wirst du für deine ehrliche Art und deine Meinung geschätzt.',
				frisch: 'Du bist genauso frisch wie die Milch-Schnitte. Meist startest Du schon munter und gut gelaunt in den Tag. Wenn es etwas zu erleben gibt, dann braucht man dich nicht lange fragen.<br><br> Du bist immer frisch, frech, fröhlich, frei dabei und für jeden Spaß zu haben.',
				unkompliziert: 'Du bist unkompliziert wie Milch-Schnitte. Dich überrascht so schnell nichts und du bleibst lässig, auch wenn es mal nicht 100% nach Plan geht.<br><br> Hürden nimmst du locker und mit einem Lächeln auf den Lippen.',
				cool: 'Dein Coolnessfaktor liegt bei gleichauf mit einer kühlen Milch-Schnitte. Du magst es, dich schick zu machen und bist immer elegant, egal wohin es geht.<br><br> Stylish by nature.'
			}
		}
	},
	_ = {
		// keys have to be in following format: [male/female]-[imageNames]
		points: {
			'male-1.png': ['cool'],
			'male-2.png': ['frisch'],
			'male-3.png': ['original'],
			'male-4.png': ['unkompliziert'],

			'female-1.png': ['cool', 'unkompliziert'],
			'female-2.png': ['frisch', 'original'],
			'female-3.png': ['cool'],
			'female-4.png': ['unkompliziert']
		}
	};

	/**
	 * init component by request user gender
	 */
	self.init = function() {
		self.requestGender();
	};

	/**
	 * - shows transparent overlay, fade in the gender prompt and attach click
	 * listeners
	 * - based on user click the _.gender variable is set
	 * - finally starts the test
	 */
	self.requestGender = function() {
		$('.popup-mask').addClass('popup-altersschranke-mask');
		$('.gender-prompt').fadeIn();

		$('.gender-prompt a').click(function(e) {
			if ($(this).text() === "Männlich") {
				_.gender = 'male';
			} else {
				_.gender = 'female';
			}
			self.startTest();
		});
	};

	/**
	 * - start the test by setting some variables
	 * - detach all listeners since this function is also used to restart, if
	 * the listeners wont be detached they will be attached twice in second
	 * round
	 * - since this function is also used to restart it also fade out every
	 * other view
	 * - calls generateImages function
	 */
	self.startTest = function() {
		_.counter = 0;
		_.cool = 0;
		_.frisch = 0;
		_.original = 0;
		_.unkompliziert = 0;

		$('#schnittentest-nein').off();
		$('#schnittentest-ja').off();
		$('#schnittentest-repeat').off();

		$('.schnittentest-result').fadeOut();
		$('article .container.schnittentest-startscreen').fadeOut();
		$('.gender-prompt').fadeOut();

		self.generateImages();
	};

	/**
	 * - generates the images by fading in the wrapper for the test at first
	 * - based on gender get the image file names from settings object
	 * - loop through all images, creating images by using divs and background-image
	 * - also save filename with $.data('filename') to use it later
	 * - prepend generated images to the .schnittentest-wrapper
	 * - calls next function to rotate and to update the text below images to
	 * count images
	 */
	self.generateImages = function() {

		var imageNames,
			schnittentestWrapper = $('.schnittentest-wrapper'),
			imageElement;

		schnittentestWrapper.fadeIn();

		imageNames = self.settings[_.gender + 'Images'];

		for (var i = 0, len = imageNames.length; i < len; i++) {
			imageElement = $('<div></div>');
			imageElement.addClass('schnittentest-wrapper__image');
			imageElement.data('filename', imageNames[i]);
			imageElement.css({
				'background-image': "url('fileadmin/media/alltagstypentest/schnittentest/" + _.gender + "/" + imageNames[i] + "')"
			});
			schnittentestWrapper.prepend(imageElement);
		}

		self.rotateImages();
		self.updateCounter();
	};

	/**
	 * - rotating images by referencing every image at first and saving in $imgs
	 * - loop through each image and apply inline transform css rule to rotate
	 * - calls attachlisteners
	 */
	self.rotateImages = function() {
		var $imgs = $('.schnittentest-wrapper__image'),
			rndRotation,
			currentImg;

		for (var i = 0, len = $imgs.length; i < len; i++) {

			// create random degrees between -13 and 13
			// 13 was choosen randomly, no particular reason
			rndRotation = parseInt(Math.random() * 26 + 0.5) - 13;

			currentImg = $imgs.eq(i);
			currentImg.css({
				'transform': 'rotate(' + rndRotation + 'deg)'
			});
		}

		self.attachListeners();
	};

	/**
	 * - attach listeners to handle swiping and button clicks
	 * - buttonEnabled is used to disable buttons during image swipe animation,
	 * important to prevent wrong behaviour
	 * - if swiperight or yes click: points are referenced by using gender and
	 * filename, which was saved through $.data('filename')
	 * - swipes and clicks are handled by extra swipeImage() function
	 * - points are handled by extra addPoints() function
	 */
	self.attachListeners = function() {

		var points,
			image,
			buttonEnabled = true;

		$('.schnittentest-wrapper__image').on('swipeleft', function(e) {

			image = $(this);
			self.swipeImage(image, 'left');
		});

		$('.schnittentest-wrapper__image').on('swiperight', function(e) {

			image = $(this);
			self.swipeImage(image, 'right');

			points = _.points[_.gender + '-' +  image.data('filename')];
			self.addPoints(points);
		});

		$('#schnittentest-nein').click(function() {

			if (buttonEnabled) {
				image = $('.schnittentest-wrapper__image').last();
				self.swipeImage(image, 'left');

				buttonEnabled = false;

				setTimeout(function() {
					buttonEnabled = true;
				}, 500);
			}
		});

		$('#schnittentest-ja').click(function() {

			if (buttonEnabled) {
				image = $('.schnittentest-wrapper__image').last();
				self.swipeImage(image, 'right');

				points = _.points[_.gender + '-' +  image.data('filename')];
				self.addPoints(points);
				buttonEnabled = false;

				setTimeout(function() {
					buttonEnabled = true;
				}, 500);
			}
		});
	};

	/**
	 * - handle image swipe animation by adding css class which has transition
	 * rule
	 * - detach listener to prevent multiple swiping same image
	 * - calls updateCounter()
	 * @param {jQuery Element} image [image that should be swiped]
	 * @param {String} direction [direction of the swipe]
	 */
	self.swipeImage = function(image, direction) {
		image.off();
		image.addClass('schnittentest-rotate-' + direction);
		image.delay(500).queue(function() {
			image.remove();
		});

		self.updateCounter();
	};

	/**
	 * - count points by looping through points array and incrementing certain
	 * properties
	 * @param  {Array of Strings} points [contains the types which should get points]
	 */
	self.addPoints = function(points) {

		for (var i = 0, len = points.length; i < len; i++) {
			_[points[i]]++;
		}
	};

	/**
	 * - increase counter and updates the text below images
	 * - check for test end and calls finishTest() if last image was swiped
	 */
	self.updateCounter = function() {
		var numberOfImages = self.settings[_.gender + 'Images'].length;

		_.counter++;
		$('.schnittentest-image-counter').text('Bild ' + _.counter + ' von ' + numberOfImages);

		if (_.counter > numberOfImages) {
			$('.schnittentest-image-counter').text('');
			setTimeout(function() {
				self.finishTest();
			},500);
		}
	};

	/**
	 * - finish the by creating headline string based on gender
	 * - determine which property get the most points
	 * - display image, headline and type-text by using $.html() and changing
	 * src attribute of image
	 * - attach listener to restart the test
	 */
	self.finishTest = function() {

		var max = -1,
			type,
			headline = 'Du bist ',
			mobileSuffix = '';

		if ($(window).width() < 480) {
			headline += '<br>';
			mobileSuffix += '-mobile';
		}
		headline += '"';

		$('.schnittentest-result').fadeIn();
		$('.schnittentest-wrapper').fadeOut();

		if (_.original > max) {
			max = _.original;
			type = 'original';
		}

		if (_.frisch > max) {
			max = _.frisch;
			type = 'frisch';
		}

		if (_.unkompliziert > max) {
			max = _.unkompliziert;
			type = 'unkompliziert';
		}

		if (_.cool > max) {
			max = _.cool;
			type = 'cool';
		}

		if (type !== 'original') {
			if (_.gender === 'male') headline += 'Der';
			if (_.gender === 'female') headline += 'Die';
		} else {
			headline += 'Das';
		}

		if (type === 'original') headline += ' Original"';
		if (type === 'frisch') headline += ' Frische"';
		if (type === 'unkompliziert') headline += ' Unkomplizierte"';
		if (type === 'cool') headline += ' Coole"';

		$('.schnittentest-result h2').html(headline);
		$('.schnittentest-result p').html(self.settings.results[type]);

		$('.schnittentest-result img').attr('src',
			[
				'fileadmin/media/alltagstypentest/schnittentest/',
				_.gender,
				'/',
				type,
				mobileSuffix,
				'.jpg'
			].join('')
		);

		$('#schnittentest-repeat').click(function() {
			self.startTest();
		});
	};

	return self;
})();

$(document).ready(function(){

	$('#schnittentest-start').click(function(){
		Brandung.Component.Schnittentest.init();
	});
});