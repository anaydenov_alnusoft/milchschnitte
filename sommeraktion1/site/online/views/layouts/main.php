<!doctype html>
<!--[if lt IE 7]> <html lang="de" class="no-js ie6"> <![endif]-->
<!--[if IE 7]> <html lang="de" class="no-js ie7"> <![endif]-->
<!--[if IE 8]> <html lang="de" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="de" class="no-js"> <!--<![endif]-->

<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use online\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<head>
    <meta charset="UTF-8">
    <title>Sommeraktion - Milch-Schnitte</title><meta property="og:title" content="Milch-Schnitte"><meta property="og:type" content="website"><meta property="og:url" content="http://www.milchschnitte.de/"><meta property="og:image" content="http://www.milchschnitte.de/images/milch-schnitte_fbshare.jpg"><meta property="og:site_name" content="Milch-Schnitte"><meta property="og:description" content="Unkompliziert und frisch aus dem Kühlschrank ist Milch-Schnitte der leckere Snack für die kleine Pause im Alltag.">
    <?php $this->head() ?>
    <meta name="description" content="Gro&szlig;er Tag, kleine Pause. Jetzt den neuen Milch-Schnitte TV Spot ansehen." />
    <meta name="keywords" content="Milch-Schnitte, Milchschnitte, Alltagstypen, Alltagstypentest, Milch-Schnitte-Zutaten, Milch-Schnitte-Wallpaper, Milch-Schnitte-Downloads, Ferrero" />

    <meta name="WT.ti" content="Milch-Schnitte - Sommeraktion">
    <meta name="WT.cg_n" content="Milch-Schnitte-Sommeraktion">
    <meta name="DCS.dcsuri" content="milch-schnitte/Sommeraktion">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <script type="text/javascript">
            var base = location.protocol + "//" + location.hostname +    (location.port && ":" + location.port) + "/";
            document.getElementsByTagName("base")[0].href =  + (location.hostname == '10.172.30.25')?base+"milchschnitte/":base;
    </script>
    <link id="favicon" rel="shortcut icon" href="/fileadmin/template/icons/favicon.ico">
    <link rel="apple-touch-icon" href="/fileadmin/template/icons/57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fileadmin/template/icons/72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fileadmin/template/icons/114.png">
    
        
    
<script>dataLayer = [];</script><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MJVLXS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MJVLXS');</script></head>
    

<body>
<?php $this->beginBody() ?>    

<div id="wrapper">

        <!-- start header -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="container container--logo">
                            <h1 class="logo"><a href="/" title="Milch-Schnitte">Milch-Schnitte</a></h1>
                            <img class="kinder_signatur" src="/fileadmin/template/media/kinder_signatur.png" title="Kinder">
                        </div>
                        <div id="nav">
                            <ul>
                                <li class="first product active">
                                    <a href="/die-milch-schnitte/">Milch-Schnitte</a>
                                    <ul class="sub-menu">
                                        <li><a href="/die-milch-schnitte/">Die Milch-Schnitte</a></li>
                                        <li><a href="/die-milch-schnitte/backen-mit-milch-schnitte/">Backen mit Milch-Schnitte</a></li>
                                        <li><a href="/tv-spots/">TV-Spot</a></li>
                                        <li><a href="/die-milch-schnitte/meine-pause/galerie/">Meine Pause</a></li>
										<li class="active"><a href="/sommeraktion/">Sommeraktion</a></li>
                                    </ul>
                                </li>
                                <li class="family-life">
                                    <a href="/mein-familienalltag/">Familienalltag</a>
                                    <ul class="sub-menu">
                                        <li class="tablet-only"><a href="/mein-familienalltag/">&Uuml;bersicht</a></li>
                                        <li><a href="/mein-familienalltag/bastelspass/">Bastelspass</a></li>
                                        <li><a href="/mein-familienalltag/spielefinder/">Spielefinder</a></li>
                                        <li><a href="/mein-familienalltag/inselfinder/">Inselfinder</a></li>
                                        <li><a href="/mein-familienalltag/inseltipps/">Inseltipps</a></li>
                                    </ul>
                                </li>
                                <li class="study">
                                    <a href="/alltagsstudie/alltag-in-deutschland/">Alltagsstudie</a>
                                    <ul class="sub-menu">
                                        <li><a href="/alltagsstudie/alltag-in-deutschland/">Alltag in Deutschland</a></li>
                                        <li><a href="/alltagsstudie/alltagsstrategien/">Alltagsstrategien</a></li>
                                        <li><a href="/alltagsstudie/alltagsmanagertypen/">Alltagsmanagertypen</a></li>
                                        <li><a href="/alltagsstudie/alltagstipps/">Alltagstipps</a></li>
                                        <li><a href="/alltagsstudie/service/">Service</a></li>
                                    </ul>
                                </li>
                                <li class="last survey">
                                    <a href="/alltagstypentest/">Typentest</a>
                                    <ul class="sub-menu">
                                        <li class="tablet-only"><a href="/alltagstypentest/">Alltagstypentest</a></li>
                                        <!-- <li><a href="#">Welche Schnitte bist du?</a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- end header -->

    <article>
        <container class="container--main-teaser">
            <div class="box box--sub-menu"></div>
        </container>

        <div class="container">

            <?= $content ?>    

	</div>  
		</article>

    <!-- push for sticky footer -->
    <div id="push"></div>
</div>

<!-- sticky footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="span12">
                <ul class="nav">
                    <li><a href="/impressum/" class="popup" target="_blank">Impressum</a></li>
                    <li><a href="/datenschutz/" class="popup" target="_blank">Datenschutz</a></li>
                    <li><a href="/faq/" class="popup" target="_blank">FAQ</a></li>
                    <li class="last"><a href="http://www.ferrero.de" target="_blank">Ferrero</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<div id="ie-bgr">
    <img src="/fileadmin/template/media/bgr.png">
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>