<?php

namespace online\assets;

use yii\web\AssetBundle;

/**
 * Main online application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/fileadmin/template/css/bootstrap.css',
        '/fileadmin/template/fancybox/source/jquery.fancybox.css',
        '/fileadmin/template/mediaelement-js/mediaelementplayer.css',
        '/fileadmin/template/css/layout.css',
        'css/site.css',
    ];
    public $js = [
        '/fileadmin/template/mediaelement-js/mediaelement-and-player.min.js',
        '/fileadmin/template/js/picturefill.min.js',
        '/fileadmin/template/js/modernizr.js',
        '/fileadmin/template/js/jquery.flexslider-min.js',
        '/fileadmin/template/js/responsive-nav.min.js',
        '/fileadmin/template/fancybox/source/jquery.fancybox.pack.js',
        '/fileadmin/template/js/shared.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
