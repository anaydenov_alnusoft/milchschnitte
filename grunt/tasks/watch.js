/**
 * The awesome watch task which recognizes changes in the specified filetypes
 * and rebuilds the project after hitting strg + s
 *
 * https://npmjs.org/package/grunt-contrib-watch
 */

module.exports = {
	styles: {
		files: [
			'<%= paths.src %>/sass/**/*.scss'
		],
		tasks: [
			'sass'
		]
	}
	// scripts: {
	// 	files: [
	// 		'<%= Config.PRIVATE_DIR %>/js/**/*.js',
	// 		'<%= Config.PRIVATE_DIR %>/component/**/*.js'
	// 	],
	// 	tasks: [
	// 		'copy:privateHandlerToPublicFolder',
	// 		'copy:privateUtilToPublicFolder',
	// 		'copy:privateFunctionToPublicFolder',
	// 		'copy:privateComponentToPublicFolder',
	// 		'concat:mainJS',
	// 		'clean:globalJsInPublicFolder'
	// 	]
	// },
	// tpl: {
	// 	files:  [
	// 		'<%= Config.PRIVATE_DIR %>/templates/tpl/**/*.tpl',
	// 		'<%= Config.PRIVATE_DIR %>/component/**/*.tpl'
	// 	],
	// 	tasks: [
	// 		'tasty_swig'
	// 	]
	// }
};