/**
 * Compiles sass files using libsass (damn fast!)
 *
 * https://www.npmjs.org/package/grunt-sass
 */

module.exports = {
	options: {
		outputStyle: 'expanded',
		sourceMap: false,
		indentWidth: 4
	},
	files: {
		expand: true,
		cwd: '<%= paths.src %>/sass',
		src: ['**/*.scss'],
		dest: '<%= paths.dev %>/css',
		extDot: 'first',
		rename  : function (dest, src) {
			var _new_ext = 'css';

			//Get src filename
			src = src.split("/");
			var filename = src.pop();

			//Apply new extension to filename
			var arr  = filename.split(".");
			arr.pop();
			arr.push(_new_ext);
			filename = arr.join(".");

			dest = dest || src.join("/");

			return dest + '/' + filename;
		}
	}
	// component: {
	// 	expand: true,
	// 	cwd: '<%= Config.PRIVATE_DIR %>/component',
	// 	src: ['**/*.scss'],
	// 	dest: '<%= Config.PUBLIC_DIR %>/component',
	// 	ext: '.css'
	// }
};

