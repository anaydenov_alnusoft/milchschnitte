<?php
session_start();
setlocale(LC_ALL, "de_DE@euro", "de_DE", "de", "ge");
header("Cache-Control: no-cache");
header("Pragma: no-cache");
header("Content-Type: application/json");

require_once("../system/includes.php");


$returnValue = new stdClass;

if ($_SERVER["QUERY_STRING"] != "") {

    $queryStringArray = array();
    parse_str(urldecode($_SERVER["QUERY_STRING"]), $queryStringArray);

    if ($queryStringArray["action"] == "dailyGalleries") {

        $items = array();

        $resultDate = $connection->dbRow("DISTINCT DATE_FORMAT(created, '%m-%d') as created","MeinePause","status = '2'","created desc");
        while ($rowDate = fetch($resultDate)) {

            $dateArray = explode("-", $rowDate["created"]);

            $dateFrom = $galleryYear . "-" . $dateArray[0] . "-" . $dateArray[1] . " 0-00-00";
            $dateTo = $galleryYear . "-" . $dateArray[0] . "-" . $dateArray[1] . " 23-59-59";

            $dayImageArray = array();

            $result = $connection->dbRow("*","MeinePause","status = '2' && (created BETWEEN '" . $dateFrom . "' AND '" . $dateTo ."')","RAND() LIMIT 3");
            while ($row = fetch($result)) {
                $pictureDateDecrypted = unserialize($row["pictureData"]);
                if ($pictureDateDecrypted["size167"]) {
                    array_push($dayImageArray, $pictureDateDecrypted["size167"]);
                }
            }

            for ($i = count($dayImageArray); $i < 3; $i++) {
                array_push($dayImageArray, $pictureDateDecrypted["size167"]);
            }

            $itemsDay = array(
                'title' => $dateArray[1] . "." . $dateArray[0] . "." . $galleryYear,
                'href' => $galleryPath . $dateArray[1] . "-" . $dateArray[0],
                'images' => $dayImageArray
            );

            array_push($items, $itemsDay);
        }

        $returnValue->items = $items;
    }

    if ($queryStringArray["action"] == "detailGallery" && isset($page)) {

        $items = array();

        $dateArray = explode("-", $page);


        $dateFrom = $galleryYear . "-" . $dateArray[1] . "-" . $dateArray[0] . " 0-00-00";
        $dateTo = $galleryYear . "-" . $dateArray[1] . "-" . $dateArray[0] . " 23-59-59";

        $result = $connection->dbRow("*","MeinePause","status = '2' && (created BETWEEN '" . $dateFrom . "' AND '" . $dateTo ."')","created desc");
        while ($row = fetch($result)) {

            $pictureDateDecrypted = unserialize($row["pictureData"]);
            $date = new Datetime($row["created"]);

            $itemArray = array(
                'href' => $galleryPath . $date->format("d-m") . "/pause-" . $row["id"],
                'imageSrc' => $pictureDateDecrypted["size208"],
                'category' => $row["category"]
            );
            array_push($items, $itemArray);
        }

        $returnValue->items = $items;
    }

} else {
    $returnValue->imageProcessingSuccessfull = false;
    $returnValue->errorMessage = "NO PARAMS";
}

echo json_encode($returnValue);