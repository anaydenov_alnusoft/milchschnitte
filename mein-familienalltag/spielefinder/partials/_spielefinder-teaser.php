<div class="row">
    <div class="span12">
        <section class="box product-teaser main-teaser teaser-xlarge main-teaser spielefinder">
            <picture>
                <!--[if IE 9]><video style="display: none;"><![endif]-->
                <source srcset="/fileadmin/media/mein-familienalltag/spielefinder/mainteaser_768.jpg" media="(max-width: 767px)">
                <source srcset="/fileadmin/media/mein-familienalltag/spielefinder/mainteaser.jpg">
                <!--[if IE 9]></video><![endif]-->
                <img title="" alt="" src="/fileadmin/media/mein-familienalltag/spielefinder/mainteaser.jpg">
            </picture>
            <div class="product-teaser__content-wrapper align-left left">
                <h2 class="white">Zeit zum Spielen!<br>Mit dem Milch-Schnitte Spielefinder.</h2>
                <p>
                    Egal ob nachmittags mit Freunden, im gemeinsamen Urlaub oder beim Kindergeburtstag – Kinder lieben Spiele! Der Milch-Schnitte Spielefinder bietet 50 abwechslungsreiche Spielideen. Mit der praktischen Filterfunktion findest du für jeden Anlass genau das richtige Spiel.
                    <br> So wird jeder Nachmittag zu einem spannenden Erlebnis für dein Kind.
                    <br>
                    <br>
                    <b>LOS GEHT`S!</b>
                </p>
            </div>
        </section>
    </div>
</div>