<div class="row">
    <div class="span12">
        <div class="toggle-filter-menu">
            <div class="select-style-">
                <a href="#" class="sbSelector">Menü</a>
            </div>
        </div>
        <div class="row-fluid filter">
            <div class="filter-menu">
                <div class="select-style-">
                    <select name="kindesalter" id="select_kindesalter" class="selectfilter" tabindex="1">
                        <option value="-" selected="selected">Kindesalter</option>
                        <option value="-">keine Auswahl</option>
                        <option value="1">3-5 Jahre</option>
                        <option value="2">6-9 Jahre</option>
                        <option value="3">ab 10 Jahre</option>
                    </select>
                </div>
                <div class="select-style-">
                    <select name="spieleranzahl" id="select_spieleranzahl" class="selectfilter" tabindex="2">
                        <option value="-">Spieleranzahl</option>
                        <option value="-">keine Auswahl</option>
                        <option value="1">2-4 Spieler</option>
                        <option value="2">5-8 Spieler</option>
                        <option value="3">ab 9 Spieler</option>
                    </select>
                </div>
                <div class="select-style-">
                    <select name="spielort" id="select_spielerort" class="selectfilter" tabindex="3">
                        <option value="-">Spiel-Ort</option>
                        <option value="-">keine Auswahl</option>
                        <option value="1">drinnen</option>
                        <option value="2">draussen</option>
                        <option value="3">unterwegs</option>
                    </select>
                </div>
                <div class="select-style-">
                    <select name="spielmaterial" id="select_spielmaterial" class="selectfilter" tabindex="4">
                        <option value="-">Spielmaterial</option>
                        <option value="-">keine Auswahl</option>
                        <option value="1">mit Material</option>
                        <option value="2">ohne Material</option>
                    </select>
                </div>
            </div>
            <div class="clear-top10">
                <div class="select-style-">
                    <a href="#" class="clear-filter sbSelector">zurücksetzen</a>
                </div>
                <div class="select-style-" style="display: none;">
                    <a href="#" class="toggle top10 sbSelector">Top 10</a>
                </div>
            </div>
        </div>

    </div>
</div>