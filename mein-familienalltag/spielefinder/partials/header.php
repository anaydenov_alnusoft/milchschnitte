<!-- start header -->
<header>
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="container container--logo">
                    <h1 class="logo"><a href="/" title="Milch-Schnitte">Milch-Schnitte</a></h1>
                    <img class="kinder_signatur" src="../../fileadmin/template/media/kinder_signatur.png" title="Kinder">
                </div>
                <div id="nav">
                    <ul>
                        <li class="first product">
                            <a href="/die-milch-schnitte/">Milch-Schnitte</a>
                            <ul class="sub-menu">
                                <li><a href="/die-milch-schnitte/">Die Milch-Schnitte</a></li>
                                <li><a href="/die-milch-schnitte/backen-mit-milch-schnitte/">Backen mit Milch-Schnitte</a></li>
                                <li><a href="/tv-spots/">TV-Spot</a></li>
                            </ul>
                        </li>
                        <li class="family-life active">
                            <a href="/mein-familienalltag/">Familienalltag</a>
                            <ul class="sub-menu">
                                <li class="tablet-only"><a href="/mein-familienalltag/">Übersicht</a></li>
                                <li><a href="/mein-familienalltag/bastelspass/">Bastelspass</a></li>
                                <li class="active"><a href="/mein-familienalltag/spielefinder/">Spielefinder</a></li>
                                <li><a href="/mein-familienalltag/inselfinder/">Inselfinder</a></li>
                                <li><a href="/mein-familienalltag/inseltipps/">Inseltipps</a></li>
                            </ul>
                        </li>
                        <li class="study">
                            <a href="/alltagsstudie/alltag-in-deutschland/">Alltagsstudie</a>
                            <ul class="sub-menu">
                                <li><a href="/alltagsstudie/alltag-in-deutschland/">Alltag in Deutschland</a></li>
                                <li><a href="/alltagsstudie/alltagsstrategien/">Alltagsstrategien</a></li>
                                <li><a href="/alltagsstudie/alltagsmanagertypen/">Alltagsmanagertypen</a></li>
                                <li><a href="/alltagsstudie/alltagstipps/">Alltagstipps</a></li>
                                <li><a href="/alltagsstudie/service/">Service</a></li>
                            </ul>
                        </li>
                        <li class="last survey">
                            <a href="/alltagstypentest/">Typentest</a>
                            <ul class="sub-menu">
                                <li class="tablet-only"><a href="/alltagstypentest/">Alltagstypentest</a></li>
                                <!-- <li><a href="#">Welche Schnitte bist du?</a></li> -->
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- end header -->