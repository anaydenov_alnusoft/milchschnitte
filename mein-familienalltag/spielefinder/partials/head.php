<head runat="server"><base href="http://www.milchschnitte.de/" id="baseTag" runat="server" />
    <meta charset="UTF-8" />
    <title>Spielefinder - Milch-Schnitte</title>
    <meta property="og:title" content="Milch-Schnitte Spielefinder" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.milchschnitte.de/mein-familienalltag/spielefinder/" />
    <meta property="og:image" content="http://test.milchschnitte.de/fileadmin/media/mein-familienalltag/spielefinder/milch-schnitte-share.jpg" />
    <meta property="og:site_name" content="Milch-Schnitte Spielefinder" />
    <meta property="og:description" content="Entdecke mit dem Milch-Schnitte Spielefinder 50 tolle Spielideen für jeden Anlass!" />

    <meta name="description" content="Abwechslungsreiche Spielideen für jeden Anlass. Der Milch-Schnitte Spielefinder macht jeden Nachmittag zum Erlebnis. Mit praktischer Filterfunktion." />
    <meta name="keywords" content="Zeitmanagement, Zeit Familienalltag, Ratgeber Familie, Alltag organisieren, Freizeittipps, Familienspiele, Familienaktivitäten" />

    <meta name="WT.ti" content="Mein Familienalltag" />
    <meta name="WT.cg_n" content="Mein Familienalltag" />
    <meta name="DCS.dcsuri" content="/mein-familienalltag" />

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <script type="text/javascript">
            var base = location.protocol + "//" + location.hostname +    (location.port && ":" + location.port) + "/";
            document.getElementsByTagName("base")[0].href =  + (location.hostname == '10.172.30.25')?base+"milchschnitte/":base;
    </script>
    <link id="favicon" rel="shortcut icon" href="../../fileadmin/template/icons/favicon.ico" />

    <link rel="apple-touch-icon" href="../../fileadmin/template/icons/57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="../../fileadmin/template/icons/72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="../../fileadmin/template/icons/114.png" />

    <link rel="stylesheet" type="text/css" href="../../fileadmin/template/css/bootstrap.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../fileadmin/template/fancybox/source/jquery.fancybox.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../fileadmin/template/css/layout.css" media="screen" />

    <!--[if IE]><link rel="stylesheet" type="text/css" href="../../fileadmin/template/css/ie.css" media="screen"><![endif]-->
    <!--[if lte IE 8]>
        <link rel="stylesheet" type="text/css" href="../../fileadmin/template/css/ie8.css" media="screen">

    <![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="../../fileadmin/template/css/ie7.css" media="screen"><![endif]-->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/fileadmin/template/js/picturefill.min.js"></script>
    <script src="../../fileadmin/template/js/modernizr.js" type="text/javascript"></script>

    <script>dataLayer = [];</script>
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-MJVLXS" height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <script>    (function (w, d, s, l, i) { w[l] = w[l] || []; w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' }); var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f); })(window, document, 'script', 'dataLayer', 'GTM-MJVLXS');</script>
</head>