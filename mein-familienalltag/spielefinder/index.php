<?php
require_once("../../system/includes.php");


/*

location:
1 - drinnen
2 - draussen
3 - unterwegs

requirements:
1 - mit material
2 - ohne material

personCount:
1 - 2 - 4
2 - 5 - 8
3 - ab 9

 */

$result = $connection->dbRow("*","spielefinder","status = 1","likes desc");

?>

<!DOCTYPE html>

<html lang="de" class="no-js">
<?php include_once('./partials/head.php'); ?>
<body class="spielefinder">
    <form id="form1" runat="server">

        <div id="wrapper">

            <?php include_once('./partials/header.php'); ?>

            <article>
                <container class="container--main-teaser">
                    <div class="box box--sub-menu"></div>
                </container>
                <div class="container">

                    <?php include_once('./partials/_spielefinder-teaser.php'); ?>
                    <?php include_once('./partials/_spielefinder-filter.php'); ?>

                    <!-- games -->
                    <div class="row spiele-inner">
                        <?php while ($row = fetch($result)) { ?>
                        <?php
                            $dataAge = '';
                            $ageFrom = (int)($row['ageFrom']);
                            $ageTo = (int)($row['ageTo']);

                            if ($ageFrom >= 3 && $ageTo <= 5) $dataAge = '1';
                            else if ($ageFrom <= 5 && $ageTo >= 6 &&$ageTo <= 9) $dataAge = '1,2';
                            else if ($ageFrom <= 5 && $ageTo >= 6 && $ageTo >= 10) $dataAge = '1,2,3';
                            else if ($ageFrom >= 5 && $ageTo <= 9) $dataAge = '2';
                            else if ($ageFrom >= 5 && $ageTo >= 10) $dataAge = '2,3';
                            else if ($ageFrom >= 10) $dataAge = '3';
                        ?>
                        <div class="spiele-item span4"
                                data-spieleranzahl="<?php echo $row['personCount'] ?>"
                                data-kindesalter="<?php echo $dataAge; ?>"
                                data-spielmaterial="<?php echo $row['requirements'] ?>"
                                data-spielort="<?php echo $row['location'] ?>"
                                data-likes="<?php echo $row['likes'] ?>">
                            <a class="product-teaser__link" href="<?php echo '/mein-familienalltag/spielefinder/'.$row['url']; ?>">
                                <section class="box product-teaser teaser-onecol">
                                    <picture>
                                        <img src="<?php echo '/mein-familienalltag/spielefinder/Bilder/'.$row['picture']; ?>" alt="">
                                    </picture>
                                </section>
                                <div class="product-teaser__content-wrapper top">
                                    <h2>
                                        <span class="spielname"><?php echo $row['title'];?></span>
                                    </h2>
                                    <p><?php echo $row['subline'];?></p>
                                </div>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                    <!-- games -->

                    <div class="mobile_scroll_top_nav"></div>
                </div>
            </article>


            <!-- push for sticky footer -->
            <div id="push"></div>
        </div>

        <?php include_once('./partials/footer.php'); ?>

        <div id="ie-bgr">
            <img src="../../fileadmin/template/media/bgr.png">
        </div>

        <?php include_once('./partials/_scripts.php'); ?>

    </form>

    <?php include_once('./partials/_facebook.php'); ?>
</body>
</html>
