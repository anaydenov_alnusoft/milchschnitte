<?php
require_once("../../system/includes.php");

$gameMd5 = md5($game);

if (isset($_COOKIE['milch-schnitte-likes'][$gameMd5])) {
    exit();
}

$member_data = $connection->dbSelect("*", "spielefinder", "url = '" . $connection->avoidInjection($game) . "'");

if ($member_data !== false) {
    $dbUpdate          = array();
    $dbUpdate["likes"] = $member_data["likes"] + 1;
    $connection->dbUpdate(
        "likes", "spielefinder", "id='" . $connection->avoidInjection((int)$member_data["id"]) . "'", $dbUpdate
    );

    setcookie('milch-schnitte-likes[' . $gameMd5 . ']', 'true', mktime(0, 0, 0, 12, 31, 2030));

    echo $dbUpdate["likes"];
}