<?php
require_once("../../system/includes.php");

$member_data = $connection->dbSelect( "*","spielefinder","url = '".$connection->avoidInjection($game)."'" );
if ($member_data === false) {
    header("HTTP/1.0 404 Not Found");
    header("Location: /404.html");
    exit();
}

$location = str_replace('1', 'drinnen', $member_data['location']);
$location = str_replace('2', ' draussen', $location);
$location = str_replace('3', ' unterwegs', $location);
?>

<!DOCTYPE html>
<html lang="de" class="no-js">
<head runat="server"><base href="http://www.milchschnitte.de/" id="baseTag" runat="server" />



    <meta charset="UTF-8" />
    <title>Spielefinder - Milch-Schnitte</title>
    <meta property="og:title" content="Milch-Schnitte Spielefinder" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.milchschnitte.de/mein-familienalltag/spielefinder/" />
    <meta property="og:image" content="http://www.milchschnitte.de/fileadmin/media/mein-familienalltag/spielefinder/milch-schnitte-share.jpg" />
    <meta property="og:site_name" content="Milch-Schnitte Spielefinder" />
    <meta property="og:description" content="Entdecke mit dem Milch-Schnitte Spielefinder 50 tolle Spielideen für jeden Anlass!" />

    <meta name="description" content="Abwechslungsreiche Spielideen für jeden Anlass. Der Milch-Schnitte Spielefinder macht jeden Nachmittag zum Erlebnis. Mit praktischer Filterfunktion." />
    <meta name="keywords" content="Zeitmanagement, Zeit Familienalltag, Ratgeber Familie, Alltag organisieren, Freizeittipps, Familienspiele, Familienaktivitäten" />

    <meta name="WT.ti" content="Mein Familienalltag" />
    <meta name="WT.cg_n" content="Mein Familienalltag" />
    <meta name="DCS.dcsuri" content="/mein-familienalltag" />

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <script type="text/javascript">
            var base = location.protocol + "//" + location.hostname +    (location.port && ":" + location.port) + "/";
            document.getElementsByTagName("base")[0].href =  + (location.hostname == '10.172.30.25')?base+"milchschnitte/":base;
    </script>
    <link id="favicon" rel="shortcut icon" href="../../fileadmin/template/icons/favicon.ico" />

    <link rel="apple-touch-icon" href="../../fileadmin/template/icons/57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="../../fileadmin/template/icons/72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="../../fileadmin/template/icons/114.png" />

    <link rel="stylesheet" type="text/css" href="../../fileadmin/template/css/bootstrap.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../fileadmin/template/fancybox/source/jquery.fancybox.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../fileadmin/template/css/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../../fileadmin/template/css/print.css" media="print" />
    <link rel="stylesheet" type="text/css" href="/fileadmin/template/css/shariff.min.css">
    <link rel="stylesheet" type="text/css" href="/fileadmin/template/css/font-awesome.min.css">

    <!--[if IE]><link rel="stylesheet" type="text/css" href="../../fileadmin/template/css/ie.css" media="screen"><![endif]-->
    <!--[if lte IE 8]>
        <link rel="stylesheet" type="text/css" href="../../fileadmin/template/css/ie8.css" media="screen">

    <![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="../../fileadmin/template/css/ie7.css" media="screen"><![endif]-->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/fileadmin/template/js/picturefill.min.js"></script>
    <script src="/fileadmin/template/js/shariff.min.js" type="text/javascript"></script>
    <script src="../../fileadmin/template/js/modernizr.js" type="text/javascript"></script>

    <script>dataLayer = [];</script>
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-MJVLXS" height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <script>    (function (w, d, s, l, i) { w[l] = w[l] || []; w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' }); var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f); })(window, document, 'script', 'dataLayer', 'GTM-MJVLXS');</script>
</head>
<body class="spielefinder spielefinder--detail">

        <div id="wrapper">

        <?php include_once('./partials/header.php'); ?>



    <article>
        <container class="container--main-teaser">
            <div class="box box--sub-menu"></div>
        </container>

        <div class="container">

    <div class="row">
        <div class="span12">
            <section class="link-list--header">
                <a href=""></a>
                <a class="red" href="/mein-familienalltag/spielefinder/">
                    <i class="fa fa-bars"></i>
                    <b>Zurück zur Übersicht</b>
                </a>
                <a href=""></a>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="span12">
            <section class="box product-teaser main-teaser teaser-xlarge main-teaser spiel-inner print-wrapper">
                <div class="top-wrapper">
                    <h2>
                        <?php echo $member_data['title'] ?>
                        <span class="mag_ich_anzahl">
                            <i class="fa fa-heart"></i>
                            <span class="mag_ich_anzahl_inner"><?php echo $member_data['likes'] ?></span>
                        </span>
                    </h2>
                    <div class="row-fluid">
                        <div class="span2">Kindesalter:</div>
                        <div class="span1"><b><?php echo $member_data['ageFrom'] ?>-<?php echo $member_data['ageTo'] ?></b></div>
                        <div class="span2">Spiel-Ort:</div>
                        <div class="span3 uppercase"><b><?php echo $location; ?></b></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span2">Spieleranzahl:</div>
                        <div class="span1"><b><?php echo $member_data['personCountString'] ?></b></div>
                        <div class="span2">Spielmaterial:</div>
                        <div class="span3"><b><?php echo $member_data['requirementsString'] ?></b></div>
                    </div>

                    <img src="/mein-familienalltag/spielefinder/Bilder/<?php echo $member_data['picture'] ?>" alt="">
                </div>

                <div class="bottom-wrapper picture">
                    <h2>Spielanleitung:</h2>
                    <p>
                    	<?php echo $member_data['description'] ?>
                    </p>

                    <h2>Spielende:</h2>
                    <p><?php echo $member_data['gameEnd'] ?></p>

                    <div class="social-wrapper row-fluid">
                        <div class="span4 cta-btn social-print">
                            <div class="text">
                                <i class="fa fa-file-pdf-o"></i>
                                Anleitung drucken
                            </div>
                        </div>
                        <div class="span4 cta-btn social-like">
                            <div class="text">
                                <i class="fa fa-heart"></i>
                                Dieses Spiel mag ich.
                            </div>
                        </div>
                        <div class="span4 cta-btn social-share">
                            <div class="text">
                                <i class="fa fa-share-alt"></i>
                                Spiel teilen
                            </div>
                            <div class="shariff"></div>
                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>

    <div class="row">
        <div class="span12">
            <section class="link-list--header link-list--margin">
                <a href=""></a>
                <a class="red" href="/mein-familienalltag/spielefinder/">
                    <i class="fa fa-bars"></i>
                    <b>Zurück zur Übersicht</b>
                </a>
                <a href=""></a>
            </section>
        </div>
    </div>

    <div class="mobile_scroll_top_nav"></div>

</div>  </article>

            <!-- push for sticky footer -->
            <div id="push"></div>
        </div>

        <?php include_once('./partials/footer.php'); ?>

        <div id="ie-bgr">
            <img src="../../fileadmin/template/media/bgr.png">
        </div>
        <script src="../../fileadmin/template/js/responsive-nav.min.js" type="text/javascript"></script>
        <script src="../../fileadmin/template/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
        <script src="../../fileadmin/template/js/shared.js" type="text/javascript"></script>
        <script src="../../fileadmin/template/js/jquery.custom.min.js" type="text/javascript"></script>
        <script src="../../fileadmin/template/js/itip.js" type="text/javascript"></script>
        <script src="../../fileadmin/template/js/jquery.selectbox-0.2.min.js" type="text/javascript"></script>
        <script src="../../fileadmin/template/js/printThis.js" type="text/javascript"></script>

        <script>
            jQuery(document).ready(function(){
                $('.social-like:not(.disabled)').click(function(){
                    jQuery.ajax({
                        method: 'get',
                        url: '/mein-familienalltag/spielefinder/updateLike.php',
                        data: {
                            game: '<?php echo $member_data['url'] ?>'
                        }
                    }).
                    done(function(msg) {
                        $('.mag_ich_anzahl_inner').text(msg);
                    });
                });
            });
        </script>

    <?php include_once('./partials/_facebook.php'); ?>
</body>
</html>
