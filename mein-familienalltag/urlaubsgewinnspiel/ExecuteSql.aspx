﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExecuteSql.aspx.cs" Inherits="FerreroGewinnspiel.mein_familienalltag.ExecuteSql" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:TextBox ID="ConnectionString" runat="server" Width="917px"></asp:TextBox>
        <br />
        <asp:TextBox ID="Sql" runat="server" Height="368px" Rows="20" Width="918px" TextMode="MultiLine"></asp:TextBox>
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Execute" style="height: 26px" />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Select" style="height: 26px" />
        <br />
        <asp:TextBox ID="Result" runat="server" Height="211px" Rows="20" Width="918px" TextMode="MultiLine"></asp:TextBox>
    </form>
</body>
</html>
