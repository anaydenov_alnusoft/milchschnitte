﻿<!doctype html>

<%@ Page Language="C#" CodeBehind="~/mein-familienalltag/urlaubsgewinnspiel/Default.aspx.cs" Inherits="FerreroGewinnspiel.mein_familienalltag.Default" %>


<!--[if lt IE 7]> <html lang="de" class="no-js ie6"> <![endif]-->
<!--[if IE 7]> <html lang="de" class="no-js ie7"> <![endif]-->
<!--[if IE 8]> <html lang="de" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="de" class="no-js">
<!--<![endif]-->

<head>

    <meta charset="UTF-8">
    <title>Mein Familien Alltag - Milch-Schnitte</title>
    <meta property="og:title" content="Milch-Schnitte">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://www.milchschnitte.de/mein-familienalltag/urlaubsgewinnspiel/">
    <meta property="og:image" content="http://www.milchschnitte.de/images/urlaubgewinnspiel-fb-share.jpg">
    <meta property="og:site_name" content="Urlaubsgewinnspiel">
    <meta property="og:description" content="Mit Milch-Schnitte und etwas Glück eine Reise in den Europa-Park gewinnen.">

    <meta name="description" content="Milch-Schnitte – Die kleine Pause im Alltag. Mit einer Mischung aus Spielen, Fragebögen, Tipps und interaktiven Aufgabenstellungen bietet Milch-Schnitte mit Mein Familienalltag kleine Momente der persönlichen Begegnung zwischen Eltern und Kind." />
    <meta name="keywords" content="Milch-Schnitte, Milchschnitte, Familienalltag, Ratgeber Familie, Kinder machen glücklich, Familienzeit, Familienratgeber, Milch-Schnitte-Zutaten, Milch-Schnitte-Wallpaper, Milch-Schnitte-Downloads, Ferrero" />

    <meta name="WT.ti" content="Mein Familienalltag">
    <meta name="WT.cg_n" content="Mein Familienalltag">
    <meta name="DCS.dcsuri" content="/mein-familienalltag">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link id="favicon" rel="shortcut icon" href="/fileadmin/template/icons/favicon.ico">

    <link rel="apple-touch-icon" href="/fileadmin/template/icons/57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fileadmin/template/icons/72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fileadmin/template/icons/114.png">

    <link rel="stylesheet" type="text/css" href="/fileadmin/template/css/bootstrap.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/fileadmin/template/fancybox/source/jquery.fancybox.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/fileadmin/template/css/layout.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/fileadmin/template/css/swt-custom.css" media="screen">
    <!--[if IE]><link rel="stylesheet" type="text/css" href="/fileadmin/template/css/ie.css" media="screen"><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="/fileadmin/template/css/ie8.css" media="screen"><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="/fileadmin/template/css/ie7.css" media="screen"><![endif]-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script src="/fileadmin/template/js/modernizr.js" type="text/javascript"></script>

    <script>dataLayer = [];</script>
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-MJVLXS" height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <script>(function (w, d, s, l, i) { w[l] = w[l] || []; w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' }); var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f); })(window, document, 'script', 'dataLayer', 'GTM-MJVLXS');</script>


</head>



<body class="inseln">
    <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1600788593533159',
      xfbml      : true,
      version    : 'v2.3'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
    <form id="Form1" runat="server" style="height: 100%">
        <div id="wrapper">

        <!-- start header -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="container container--logo">
                            <h1 class="logo"><a href="/" title="Milch-Schnitte">Milch-Schnitte</a></h1>
                            <img class="kinder_signatur" src="/fileadmin/template/media/kinder_signatur.png" title="Kinder">
                        </div>
                        <div id="nav">
                            <ul>
                                <li class="first product">
                                    <a href="/die-milch-schnitte/">Milch-Schnitte</a>
                                    <ul class="sub-menu">
                                        <li><a href="/die-milch-schnitte/">Die Milch-Schnitte</a></li>
                                        <li><a href="/die-milch-schnitte/backen-mit-milch-schnitte/">Backen mit Milch-Schnitte</a></li>
                                        <li><a href="/tv-spots/">TV-Spot</a></li>
                                    </ul>
                                </li>
                                <li class="family-life active">
                                    <a href="/mein-familienalltag/">Familienalltag</a>
                                    <ul class="sub-menu">
                                        <li class="tablet-only"><a href="/mein-familienalltag/">Übersicht</a></li>
                                        <li><a href="/mein-familienalltag/bastelspass/">Bastelspass</a></li>
                                        <li><a href="/mein-familienalltag/spielefinder/">Spielefinder</a></li>
                                        <li><a href="/mein-familienalltag/inselfinder/">Inselfinder</a></li>
                                        <li><a href="/mein-familienalltag/inseltipps/">Inseltipps</a></li>
                                    </ul>
                                </li>
                                <li class="study">
                                    <a href="/alltagsstudie/alltag-in-deutschland/">Alltagsstudie</a>
                                    <ul class="sub-menu">
                                        <li><a href="/alltagsstudie/alltag-in-deutschland/">Alltag in Deutschland</a></li>
                                        <li><a href="/alltagsstudie/alltagsstrategien/">Alltagsstrategien</a></li>
                                        <li><a href="/alltagsstudie/alltagsmanagertypen/">Alltagsmanagertypen</a></li>
                                        <li><a href="/alltagsstudie/alltagstipps/">Alltagstipps</a></li>
                                        <li><a href="/alltagsstudie/service/">Service</a></li>
                                    </ul>
                                </li>
                                <li class="last survey">
                                    <a href="/alltagstypentest/">Typentest</a>
                                    <ul class="sub-menu">
                                        <li class="tablet-only"><a href="/alltagstypentest/">Alltagstypentest</a></li>
                                        <!-- <li><a href="#">Welche Schnitte bist du?</a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- end header -->

            
            <article>
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <section class="box teaser-large insel-teaser">
                                <!-- Header -->
                                <div class="insel-col1">
                                    <%--<img src="/images/header.jpg" />--%>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="inner intro" style="padding-top: 20px;">
                                                <h2>Ferienzeit ist Familienzeit</h2>
                                                <p>
                                                    <span>Sechs Wochen Ferien sind das Größte und endlich ist jede Menge Zeit für gemeinsame Unternehmungen.
                                                    Mit dem Milch-Schnitte Urlaubsgewinnspiel gibt es jetzt die Chance auf einen spannenden Besuch im Europa-Park für die ganze Familie!
                                                    </span>
                                                </p>
                                                <p>
                                                    <h3 class="shorter">Einfach Quiz lösen und mit etwas Glück gewinnen! Viel Erfolg!</h3>
                                                </p>
                                                <p>
                                                    <a class="cta-btn zum-quiz" href="#">Zum Quiz <span class="arrow"></span></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Header -->
                                
                                <!-- Preise -->
                                <div class="gradient-bgr clearfix">
                                    <h3 style="text-align: center;">Das kannst du gewinnen!</h3>
                                    <div class="box_left text-center">
                                        <img src="/images/europapark-400.png" />
                                        <div>
                                            <p>
                                                2 x Gutscheincards einlösbar für Verzehr, Eintritt, 
                                                Hotels und Shoppinggeschäfte im <span class="insel-bold">Wert von jeweils 800€
                                                plus Taschengeld in Höhe von 200€ für die Anreise</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="box_left text-center">
                                        <img src="/images/samsonite-400.png" />
                                        <div>
                                            <p>
                                                10 hochwertige Samsonite Koffer inklusive einer passenden 
                                                Kulturtasche im <span class="insel-bold">Wert von ca. 230€</span>
                                            </p>
                                        </div>
                                    </div>
                                    <%--<img src="/images/preise.jpg" />--%>
                                </div>
                                <div style="clear: both;"></div>
                                <!-- Preise -->

                                <!-- Step1 Quiz -->
                                <div runat="server" id="pnl_step1" class="gradient-bgr">
                                    <h3 style="text-align: center;">Finde den richtigen Weg in den Europa-Park!</h3>
                                    <img src="/fileadmin/template/media/quiz-transparent.png" />
                                    <div>
                                        <%--<img src="/images/quiz.jpg" />--%>

                                        <div class="row">
                                            <div class="span3">
                                            </div>
                                            <div class="span6">
                                                <div class="form-checkbox">
                                                    <label for="answer-a" />
                                                    <input type="radio" id="answer-a" name="answer" value="a" />
                                                    <h3>A</h3>
                                                </div>

                                                <div class="form-checkbox">
                                                    <label for="answer-b" />
                                                    <input type="radio" id="answer-b" name="answer" value="b" />
                                                    <h3>B</h3>
                                                </div>

                                                <div class="form-checkbox">
                                                    <label for="answer-c" />
                                                    <input type="radio" id="answer-c" name="answer" value="c" />
                                                    <h3>C</h3>
                                                </div>
												 <span id="reqAnswer" style="color: red; display: none;">
                                                    Es muss eine Antwort ausgewählt werden
                                                </span>

                                                <div class="form-checkbox">
                                                    <a class="cta-btn btn-teilnehmen" onclick="showStepTwo()">Teilnehmen <span class="arrow"></span></a>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                        <div class="row">
                                            <div class="span12" style="text-align: right; margin-bottom: 24px;">
                                                <a href='/fileadmin/media/mein-familienalltag/urlaubsgewinnspiel/teilnahmebedingungen_urlaubsgewinnspiel.pdf' target='_blank' class="btn-teilnahmeBedinugen">Teilnahmebedingungen</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Step1 Quiz -->

                                <!-- Step2 Adresse -->
                                <asp:Panel runat="server" ID="pnl_step2" ClientIDMode="Static" Style="display: none">
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="inner intro">
                                                <h2>Jetzt noch schnell Daten eingeben, damit wir dich im Falle des Gewinns benachrichtigen können.</h2>
                                                <div class="first-part">
                                                    <div class="form-element">
                                                        <asp:Label runat="server" AssociatedControlID="ddl_anrede">Anrede*</asp:Label>
                                                        <asp:DropDownList runat="server" ID="ddl_anrede">
                                                            <asp:ListItem Value="Mr">Herr</asp:ListItem>
                                                            <asp:ListItem Value="Mrs">Frau</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="part-50">
                                                        <div class="form-element">
                                                            <asp:Label runat="server" AssociatedControlID="tb_vorname">Vorname*</asp:Label>
                                                            <asp:TextBox ID="tb_vorname" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="tb_vorname" Text="Es muss ein Vorname eingegeben werden" Display="Dynamic" ValidationGroup="submit" />
                                                        </div>
                                                    </div>
                                                    <div class="part-50">
                                                        <div class="form-element">
                                                            <asp:Label runat="server" AssociatedControlID="tb_nachname">Nachname*</asp:Label>
                                                            <asp:TextBox ID="tb_nachname" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="tb_nachname" Text="Es muss ein Nachname eingegeben werden" Display="Dynamic" ValidationGroup="submit" />
                                                        </div>
                                                    </div>

                                                    <div class="form-element">
                                                        <asp:Label runat="server" AssociatedControlID="tb_geburtsDatum">Geburtsdatum*</asp:Label>
                                                        <asp:TextBox ID="tb_geburtsDatum" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="tb_geburtsDatum" Text="Es muss ein Geburtsdatum eingegeben werden" Display="Dynamic" ValidationGroup="submit" />
                                                        <asp:RegularExpressionValidator runat="server"
                                                            ValidationExpression="^(((0[1-9]|[12]\d|3[01])\.(0[13578]|1[02])\.((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\.(0[13456789]|1[012])\.((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\.02\.((19|[2-9]\d)\d{2}))|(29\.02\.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$"
                                                            ControlToValidate="tb_geburtsDatum"
                                                            ErrorMessage="Kein gültiges Datumsformat - (TT.MM.JJJJ) "
                                                            Display="Dynamic"
                                                            ValidationGroup="submit" />


                                                        <asp:CustomValidator runat="server" ID="CustomValidator1" ControlToValidate="tb_geburtsDatum" ClientValidationFunction="validateDate" ErrorMessage="Sie müssen mindesten 18 Jahre alt sein!" Display="Dynamic" EnableClientScript="true" ValidationGroup="submit" />
                                                        <script>
                                                            function validateDate(source, arguments) {
                                                                var today = new Date();

                                                                var bdYear = $("#tb_geburtsDatum").val().substring(6, 10);
                                                                var bdMonth = $("#tb_geburtsDatum").val().substring(3, 5);
                                                                var bdDay = $("#tb_geburtsDatum").val().substring(0, 2);

                                                                var birthDate = new Date(bdYear, bdMonth - 1, bdDay);//-1 - da januar = 0

                                                                var ageDifMs = Date.now() - birthDate.getTime();
                                                                var ageDate = new Date(ageDifMs);

                                                                if (Math.abs(ageDate.getUTCFullYear() - 1970) >= 18) {
                                                                    arguments.IsValid = true;
                                                                } else {
                                                                    arguments.IsValid = false;
                                                                }
                                                            }
                                                        </script>
                                                    </div>

                                                    <div class="form-element">
                                                        <asp:Label runat="server" AssociatedControlID="tb_email">E-Mail Adresse*</asp:Label>
                                                        <asp:TextBox ID="tb_email" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator runat="server" ID="reqEmail" ControlToValidate="tb_email" Text="Es muss eine E-Mail Adresse eingegeben werden" Display="Dynamic" ValidationGroup="submit" />
                                                        <asp:RegularExpressionValidator ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" runat="server" ID="regExEmail" ControlToValidate="tb_email" Text="Die E-Mail Adresse hat das falsche Format" Display="Dynamic" ValidationGroup="submit"></asp:RegularExpressionValidator>
                                                    </div>
                                                </div>
                                                <div class="second-part clearfix">
                                                    <div class="form-element">
                                                        <label class="bolder">Adresse*</label>
                                                    </div>
                                                    <div class="part-80">
                                                        <div class="form-element">
                                                            <asp:Label runat="server" AssociatedControlID="tb_strasse">Strasse</asp:Label>
                                                            <asp:TextBox ID="tb_strasse" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="tb_strasse" Text="Es muss eine Straße eingegeben werden" Display="Dynamic" ValidationGroup="submit" />
                                                        </div>
                                                    </div>
                                                    <div class="part-20">
                                                        <div class="form-element">
                                                            <asp:Label runat="server" AssociatedControlID="tb_nummer">Nummer</asp:Label>
                                                            <asp:TextBox ID="tb_nummer" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="tb_nummer" Text="Es muss eine Hausnummer eingegeben werden" Display="Dynamic" ValidationGroup="submit" />
                                                        </div>
                                                    </div>
                                                    <div class="part-30">
                                                        <div class="form-element">
                                                            <asp:Label runat="server" AssociatedControlID="tb_plz">PLZ</asp:Label>
                                                            <asp:TextBox ID="tb_plz" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="tb_plz" Text="Es muss eine PLZ eingegeben werden" Display="Dynamic" ValidationGroup="submit" />

                                                            <asp:RegularExpressionValidator runat="server" ControlToValidate="tb_plz" ErrorMessage="Dies ist keine gültige PLZ Nummer" ValidationExpression="^\d{5,5}$" ValidationGroup="submit" Display="Dynamic" />

                                                        </div>
                                                    </div>
                                                    <div class="part-70">
                                                        <div class="form-element">
                                                            <asp:Label runat="server" AssociatedControlID="tb_ort">Ort</asp:Label>
                                                            <asp:TextBox ID="tb_ort" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="tb_ort" Text="Es muss ein Ort eingegeben werden" Display="Dynamic" ValidationGroup="submit" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <p></p>

                                                <div>
                                                    <asp:CheckBox runat="server" ID="cb_agbGelesen" Text="Ich habe die <a href='/fileadmin/media/mein-familienalltag/urlaubsgewinnspiel/teilnahmebedingungen_urlaubsgewinnspiel.pdf' target='_blank'>Teilnahmebedingungen</a> und <a href='../../datenschutz/' class='popup' target='_blank' runat='server'>Datenschutzerklärung</a> gelesen und bin damit einverstanden." ClientIDMode="Static" />
                                                    <asp:CustomValidator runat="server" ID="reqAgb" ClientValidationFunction="validateTerms" ErrorMessage="AGB müssen akzeptiert werden" Display="Dynamic" EnableClientScript="true" ValidationGroup="submit" />
                                                    <script>
                                                        function validateTerms(source, arguments) {
                                                            var $c = $('#cb_agbGelesen');
                                                            if ($c.prop("checked")) {
                                                                arguments.IsValid = true;
                                                            } else {
                                                                arguments.IsValid = false;
                                                            }
                                                        }
                                                    </script>
                                                </div>

                                                <div>
                                                    <asp:CheckBox runat="server" ID="cb_weitereBenachrichtigungen" />
                                                    <%--<asp:RequiredFieldValidator runat="server" ControlToValidate="cb_weitereBenachrichtigungen" />--%>
                                                </div>

                                                <div class="pflichtfelder"><sup>*</sup>Pflichtfelder</div>

                                                <%--<asp:Button --%>
                                                <asp:LinkButton runat="server"
                                                    CssClass="cta-btn btn-absenden"
                                                    ID="btn_submit"
                                                    OnClick="btn_submit_Click"
                                                    ValidationGroup="submit">Absenden</asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <!-- Step2 Adresse -->

                                <!-- Step3 Danke -->
                                <asp:Panel runat="server" ID="pnl_step3" ClientIDMode="Static" Style="display: none">
                                    <div class="row-fluid">
                                        <div class="span3"></div>
                                        <div class="span6">
                                            <div class="inner intro text-center">
                                                <h3 class="danke-text">
                                                    <asp:Literal runat="server" ID="lit_htmlFormattedDisplayText" Visible="false" />
                                                    Vielen Dank für deine Teilnahme! 
                                                </h3>
                                                <h3 class="text-center" style="margin-bottom: 20px;">Im Falle eines Gewinns werden wir dich per E-Mail benachrichtigen.</h3>
                                                <h3>Jetzt auf Facebook teilen: <div class="fb-share-button" data-href="http://www.milchschnitte.de/mein-familienalltag/urlaubsgewinnspiel/" data-layout="button_count"></div></h3>
                                            </div>
                                        </div>
                                        <div class="span3"></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="inner intro text-center">
                                                <h3>Noch mehr Abwechslung gibt es mit dem Milch-Schnitte Spielefinder!</h3>

                                                <a class="cta-btn" href="/mein-familienalltag/spielefinder/">Jetzt entdecken <span class="arrow"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <!-- Step3 Danke -->
                            </section>
                        </div>



                        <%-- ToDo --%>
                        <div id="teilnahme-bedingungen" style="display: none; background-color: white; min-height: 300px;">
                            <div class="inner intro">
                                <h3>Teilenahmebedingungen</h3>
                                <p>
                                    lorem ipsum
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <!-- push for sticky footer -->
            <div id="push"></div>
        </div>

        <!-- sticky footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <ul class="nav">
                            <li><a href="~/impressum/" class="popup" target="_blank" runat="server">Impressum</a></li>
                            <li><a href="~/datenschutz/" class="popup" target="_blank" runat="server">Datenschutz</a></li>
                            <li><a href="~/faq/" class="popup" target="_blank" runat="server">FAQ</a></li>
                            <li class="last"><a href="http://www.ferrero.de" target="_blank">Ferrero</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <div id="ie-bgr">
            <img src="/fileadmin/template/media/bgr.png">
        </div>


        <script src="/fileadmin/template/js/responsive-nav.min.js" type="text/javascript"></script>
        <script src="/fileadmin/template/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
        <script src="/fileadmin/template/js/shared.js" type="text/javascript"></script>
        <script src="/fileadmin/template/js/jquery.custom.min.js" type="text/javascript"></script>
        <script src="/fileadmin/template/js/itip.js" type="text/javascript"></script>
        
        <script>
            //test
            $(document).ready(function () {
                $('a[href="http://www.ferrero.de/kontakt"]').attr('target', '_blank');
            });
            
            var resetForm = function(elem){
                elem.each(function(){
                    var s = $(this);
                    
                    if(s.attr('id') === 'cb_weitereBenachrichtigungen' || s.attr('id') === 'cb_agbGelesen'){
                        s.prop('checked', false);
                    }else if(s.attr('id') === 'answer-a' || s.attr('id') === 'answer-b' || s.attr('id') === 'answer-c'){
                        //skip
                        
                    }else{
                        s.val('');
                    }
                });
                return true;
            }
            
            $('.zum-quiz').on('click', function (e) {
                e.preventDefault();
                var pnl_step1 = $('#pnl_step1');
                var pnl_step2 = $('#pnl_step2');
                var pnl_step3 = $('#pnl_step3');
                var abstand = 10;
                var scrollTo = 0;
                if (pnl_step1.css('display') === 'block') {
                    scrollTo = pnl_step1.offset().top - abstand;
                } else if (pnl_step2.css('display') === 'block') {
                    pnl_step2.hide();
                    pnl_step1.show();
                    scrollTo = pnl_step1.offset().top - abstand;
                } else if (pnl_step3.css('display') === 'block') {
                    pnl_step3.hide();
                    pnl_step1.show();
                    resetForm($('#Form1 #pnl_step1 input, #Form1 #pnl_step2 input, #Form1 #pnl_step2 textarea, #Form1 #pnl_step2 select'));
                    scrollTo = pnl_step1.offset().top - abstand;
                }
                $('html,body').animate({ scrollTop: scrollTo }, 800);
            });

             //validate checkbox
            function showStepTwo() {
                if (!$("input[name='answer']:checked").val()) {
                    $("#reqAnswer").css("display", "block");
                }
                else {
                    $("#pnl_step1").slideUp();
                    $("#pnl_step2").slideToggle();
                    
                    dataLayer.push({ 'event':'raffle','rafflestatus': 'start' });
                }
            }

            //todo 
            //validate inputs
            //send to server
            function showStepThree() {
                $("#pnl_step1").hide();
                $("#pnl_step2").hide();
                $("#pnl_step3").show();
                
                dataLayer.push({'event':'raffle','rafflestatus': 'end'});
            }

            function showTeilnahmeBedingungen() {
                $.fancybox.open("#teilnahme-bedingungen", {
                    padding: 0,
                    fitToView: true,
                    width: '960',
                    height: '600',
                    autoSize: false,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none',
                    scrolling: 'no',
                    afterShow: function () {
                        //$('#hf_spielId').removeAttr('value');
                    },
                    afterClose: function () {
                        //$('#hf_spielId').removeAttr('value');
                    }
                });
            }
        </script>

    </form>
</body>
</html>
