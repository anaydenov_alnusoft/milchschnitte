/**
 * Milchschnitte Gruntfile
 * http://www.milchschnitte.de/
 *
 * Copyright (c) 2016 brandung GmbH & Co.KG
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function (grunt) {

	// load only used tasks
	require('jit-grunt')(grunt, { 
		// replace: 'grunt-text-replace',
		// cmq: 'grunt-combine-media-queries'
	});

	/**
	 * Measures the time each task takes
	 *
	 * https://www.npmjs.com/package/time-grunt
	 */
	require("time-grunt")(grunt);

	var options = {
		// Project settings
		config: {
			// in this directory you can find your grunt config tasks
			src: "grunt/tasks/*.js"
		},
		// define your path structure
		paths: {
			// helpers folder with grunt tasks and styleguide templates, tests and photobox
			helper: 'helpers',
			// resources folder with working files
			src: 'fileadmin/template',
			// dist folder
			dist: '_dist', 
			// dev/working folder
			dev: 'fileadmin/template'
		},
		// define your ports for grunt-contrib-connect
		ports: {
			app: '9000',
			test: '9001',
			livereload: 35729
		}
	};

	// Load grunt configurations automatically
	var configs = require('load-grunt-configs')(grunt, options);

	/**
	 * Init our grunt config
	 */
	grunt.initConfig(configs);


	/******************
	 * App Main Tasks *
	 ******************/

	/**
	 * The 'default' task
	 */
	grunt.registerTask('default', [
		'sass'
	]);

	/**
	 * The 'project:serve' task for developing
	 */
	grunt.registerTask('project:serve', [
		'default',
		'watch'
	]);

	/**
	 * The 'project:finish' task prepares files for deployment
	 */
	// grunt.registerTask('project:finish', [
	// 	'default',
	// 	'cssmin',
	// ]);
};
