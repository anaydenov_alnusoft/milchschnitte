<?php

/* @var $this yii\web\View */

$this->title = 'Index';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="index-page">
    <h2 class="ptb20 text-center georgia-blue">Dies sind deine zwei Gewinnchancen!</h2>
    <p class="text-center tradegothic-blue">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
    <div class="pull-left text-center half-side">
        <img src="images/icon-tagespreis.png" alt=""/>
        <h2 class="georgia-blue mtb20">1. Tagespreis</h2>
        <p class="tradegothic-blue">Beantworte jeden Tag die Tagesfrage 
        und sichere dir deine Chance auf den Tagespreis</p>
    </div>
    <div class="pull-right text-center half-side">
        <img src="images/icon-hauptpreis.png" alt=""/>
        <h2 class="georgia-blue mtb20">2. Hauptpreis</h2>
        <p class="tradegothic-blue">Mit jeder richtig beantworteten Frage
        sammelst du Lose für den Hauptpreis, der am Ende 
        unseres Sommer-Countdowns verlosen wird.</p>
    </div>
    <p class="clearfix"></p>
    <p class="text-center">
        <a href="#" class="btn btn-red jetzt">Jetzt mitmachen <i class="fa fa-chevron-right"></i></a>
    </p>
</div>