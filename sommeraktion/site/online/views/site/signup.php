<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reg-form">
    <h2 class="georgia-blue">Registrieren</h2>
    <p class="tradegothic-blue">Bitte gib deine persönlichen Daten lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
        <div class="form-lable">
            <div class="pull-left half-div">
                <?= $form->field($model, 'vorname')->textInput(['class' => false, 'autofocus' => true, 'placeholder' => 'Vorname'])->label(false)->error(false) ?>
            </div>
            <div class="pull-right half-div">
                <?= $form->field($model, 'nachname')->textInput(['class' => false, 'placeholder' => 'Nachname'])->label(false)->error(false) ?>
            </div>
        </div>
        <div class="form-lable">
            <div class="pull-left half-div">
                <?= $form->field($model, 'geburtsdatum')->textInput(['class' => false, 'placeholder' => 'Geburtsdatum'])->label(false)->error(false) ?>
            </div>
            <div class="pull-right half-div">
                <?= $form->field($model, 'email')->textInput(['class' => false, 'placeholder' => 'Email-Adresse'])->label(false)->error(false) ?>
            </div>
        </div>
        <div class="form-lable">
            <div class="pull-left half-div">
                <?= $form->field($model, 'password')->passwordInput(['class' => false, 'placeholder' => 'Password'])->label(false)->error(false) ?>
            </div>
            <div class="pull-right half-div">
                <?= $form->field($model, 'password_repeat')->passwordInput(['class' => false, 'placeholder' => 'Password'])->label(false)->error(false) ?>
            </div>
        </div>
        <div class="form-lable half-div pull-left">
            <div class="pull-left col-1of4">
                <?= $form->field($model, 'strasse')->textInput(['class' => false, 'placeholder' => 'Strasse'])->label(false)->error(false) ?>
            </div>
            <div class="pull-left col-2of4 col-2of4-margin">
                <?= $form->field($model, 'nr')->textInput(['class' => false, 'placeholder' => 'Nr.'])->label(false)->error(false) ?>
            </div>
        </div>
        <div class="form-lable half-div pull-right">
            <div class="pull-left col-2of4">
                <?= $form->field($model, 'plz')->textInput(['class' => false, 'placeholder' => 'PLZ'])->label(false)->error(false) ?>
            </div>
            <div class="pull-left col-1of4">
                <?= $form->field($model, 'ort')->textInput(['class' => false, 'placeholder' => 'Ort'])->label(false)->error(false) ?>
            </div>
        </div>
        <p><?= $form->errorSummary($model); ?></p>
        <p>Hiermit akzeptiere ich die Teilnahmebedingungen und Datenschutzbestimmungen der Ferrero Deutschland GmbH. Indem ich unten die Schaltfläche „Absenden“ anklicke, erkläre ich meine Teilnahme an der Aktion.</p>
        <div class="form-lable radio-btns">
            <div class="pull-left">
                <input id="radiobutton1" type="radio" name="radiobutton1"/>
                <label for="radiobutton1">
                    <span class="pl20">Ich möchte von der Ferrero Deutschland GmbH („Ferrero“) auf mich zugeschnittene Informationen und Angebote erhalten und gestatte Ferrero hierfür, meine Teilnahme an Aktionen und die Nutzung von Ferrero-Onlinediensten (Webseiten, Apps, Newsletter, etc.) zu erfassen und auszuwerten, auch unter Einbeziehung von Daten, die Ferrero von Dritten bekommt, z.B. von Marktforschungsinstituten. Details zu Ihrer Einwilligung finden Sie <a href="#">hier</a>. </span>
                </label>
            </div>
            <div class="pull-right">
                <input id="radiobutton2" type="radio" name="radiobutton2"/><label for="radiobutton2">Ferrero darf mich zu diesem Zweck per elektronischer Post (insb. E-Mail) kontaktieren.</label>
                <p><br/>Ich kann meine Einwilligungen jederzeit widerrufen, z.B. unter <a href="#"><span class="span-underline">www.ferrero.de/kontakt.</span></a> </p>
            </div>
            <?= Html::submitButton('<span>Registrieren &amp; Teilnehmen</span>', ['class' => 'btn-form', 'name' => 'signup-button']) ?>
        </div>
     <?php ActiveForm::end(); ?>
</div>                                
