<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use online\assets\AppAsset;
$assets = new AppAsset();

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reg-form">
    <h2>Anmelden oder Registrieren</h2>
    <p>Um an der Tages- und Hauptverlosung teilnehmen zu können musst du dich anmelden bzw. registrieren.</p>
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <div class="form-lable">
            <div class="half-div-login">
                <?= $form->field($model, 'email')->textInput(['class' => false, 'autofocus' => true, 'placeholder' => 'E-Mail-Adresse'])->label(false)->error(false) ?>
            </div>
        </div>
        <div class="form-lable">
            <div class="half-div-login">
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Passwort'])->label(false)->error(false) ?>
                <!--<?= $form->field($model, 'rememberMe')->checkbox() ?>-->
                <p><?= $form->errorSummary($model); ?></p>
            </div>
        </div>
        <div class="half-div-login link text-right">
            <?= Html::a('Passwort vergessen?', ['/request-password-reset.html']) ?>
        </div>
        <?= Html::submitButton('<span>Einloggen &amp; teilnehmen</span>', ['class' => 'half-div-login btn-form-login', 'name' => 'login-button']) ?>
        <div class="half-div-login link-text">
            <a href="<?= $assets->baseUrl ?>/site/signup">Neu dabei? Hier geht´s zur Registrierung.</a>
        </div>
        <div class="half-div-login link-button">
            <a href="<?= $assets->baseUrl ?>/site/signup">Registrieren > </a>
        </div>
    <?php ActiveForm::end(); ?>
</div>