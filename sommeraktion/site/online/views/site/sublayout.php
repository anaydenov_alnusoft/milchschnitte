<?php

/* @var $content string */
/* @var $is_index boolean */

use online\assets\AppAsset;
$assets = new AppAsset();

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Opened fridge and content box outline -->
<div class="page-content first">
    <div class="top-container clearfix">
        <div class="row clearfix">
            <div class="pull-left calendar-container<?php if(!$is_index){?> form-container open-fridge"<?php }?>">
                <p class="text-center<?php if(!$is_index){?> logo-position"<?php }?>">
                    <img src="<?= $assets->baseUrl ?>/images/brand.png" class="ptb20" alt="Kinder"/>
                </p>
                <?php if (!$is_index) { ?>
                <div class="calendar form-calenader">
                    <div class="btn-close"><a href="#">X</a></div>
                    <p>Heutige Gewinnspielfrage</p>
                    <p class="form-calendar-text">Seit welchem Jahr sorgt die Milch-Schnitte® für kühle Erfrischung im Sommer?</p>
                    <form class="form-calendar-label" method="get">
                        <div class="half-div">
                            <input id="year1" type="radio" name="year1" value="year1"/><label for="year1">1914</label>
                        </div>
                        <div class="half-div">
                            <input id="year2" type="radio" name="year2" value="year2"/><label for="year2">1978</label>
                        </div>
                        <div class="half-div">
                            <input id="year3" type="radio" name="year3" value="year3"/><label for="year3">2000</label>
                        </div>
                    </form>
                    <button class="btn-form-login btn-calendar-form">
                        <span>Antwort senden</span>
                    </button>
                </div>
                <?php } else { ?>
                <div class="calendar" id="calendar">
                    <img src="images/calendar.png" alt="Calendar"/>
                    <i class="calendar-icon day" data-day="20"></i>
                    <i class="calendar-icon day" data-day="19" style="left:20%"></i>
                    <i class="calendar-icon day" data-day="18" style="left:35%"></i>
                    <i class="calendar-icon day" data-day="17" style="left:48%"></i>
                    <i class="calendar-icon day" data-day="16" style="left:62%"></i>
                    <i class="calendar-icon day" data-day="15" style="left:78%"></i>
                    
                    <i class="calendar-icon day" data-day="14" style="top:36%"></i>
                    <i class="calendar-icon day" data-day="13" style="top:36%;left:20%"></i>
                    <i class="calendar-icon day" data-day="12" style="top:36%;left:35%"></i>
                    <i class="calendar-icon day" data-day="11" style="top:36%;left:48%"></i>
                    <i class="calendar-icon day" data-day="10" style="top:36%;left:62%"></i>
                    <i class="calendar-icon day" data-day="9" style="top:36%;left:78%"></i>
                    
                    <i class="calendar-icon day" data-day="8" style="top:57%;"></i>
                    <i class="calendar-icon day" data-day="7" style="top:57%;left:20%;"></i>
                    <i class="calendar-icon day" data-day="6" style="top:57%;left:35%"></i>
                    <i class="calendar-icon day" data-day="5" style="top:57%;left:48%"></i>
                    <i class="calendar-icon day" data-day="4" style="top:57%;left:62%"></i>
                    <i class="calendar-icon day" data-day="3" style="top:57%;left:78%"></i>
                    <i class="calendar-icon day" data-day="2" style="top:76%;"></i>
                    <i class="calendar-icon day" data-day="1" style="top:76%;left:20%"></i>
                 </div>
                 <div class="slider clearfix">
                     <div class="slider-circle pull-left"></div>
                     <img src="images/dash-line.png" alt="" class="pull-left dash-line"/>
                     <img src="images/slider-button.png" alt="" class="pull-left slider-button"/>
                 </div>
                 <div class="row pt20 pinned-imgs">
                     <img src="images/photo-pool.png" class="pull-left pool-img" alt=""/>
                     <img src="images/griff-img.png" class="pull-right griff-img" alt=""/>
                 </div>
                 <div class="text-right more-div">
                     <a href="#tabbed" class="more">Mehr erfahren</a>
                 </div>                
                <?php } ?>
            </div>
            <div class="pull-right text-center">
                <p><img<?php if(!$is_index){?> class="img-right-form"<?php }?> src="<?= $assets->baseUrl ?>/images/frisch-sommer.png" alt=""/></p>
                <?php if ($is_index) { ?>
                <p class="plr30">Mach mit beim Milch-Schnitte® Sommergewinnspiel und gewinne tolle Preise, die deinen Sommer unvergesslich machen!</p>
                <p class="ptb20">
                    <img src="images/milch-schnite.png" alt="" class="pull-right milch-schnite-img"/>
                </p>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="bottom-container clearfix">
        <div class="contest-box">
            <!-- tab navigation -->
            <div class="tabs tabs--in-box tabs--narrow-select btn-shadow-wrapper">
                <ul>
                    <li>
                        <a href="#">Gewinnspiel <span class="arrow arrow--down"></span></a>
                    </li>
                    <li>
                        <a href="#">Preise <span class="arrow arrow--down"></span></a>
                    </li>
                    <li class="active">
                        <a href="#">Meine Lose <span class="arrow arrow--down"></span></a>
                    </li>
                </ul>
            </div>
            <!-- main content box -->
            <section class="content-box content-box--narrow-no-frame">
                <div class="content-box__inner content-box__inner--vertical-padding">
                    <div class="contest-submission">
                        <div class="row-fluid<?php if(!$is_index){?> tabs-content<?php }?>">
                            <div class="span12">
                              <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>            
            <!-- main content box end -->
        </div>
    </div>
</div>