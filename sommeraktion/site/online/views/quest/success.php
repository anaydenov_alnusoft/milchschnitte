<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Your answer is correct!';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="quest-success">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
</div>
