<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\QuestForm */
/* @var $src_model \frontend\models\Questions */
/* @var $date_fmt string */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Question of the day ('.$date_fmt.')';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="quest-form">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please choose the right answer:</p>

    <div class="row">
        <div class="col-lg-5">
            <p><?= Html::encode($src_model->question) ?></p>
            <?php $form = ActiveForm::begin(['id' => 'form-quest']);
				  echo $form->field($model, 'answer')->radioList([
				    '1' => $src_model->answer1,
				    '2' => $src_model->answer2,
				  	'3' => $src_model->answer3,
				   ]);  ?>
				<?= $form->field($model, 'question_id')->hiddenInput(['value'=> $src_model->id])->label(false); ?>
				<?= $form->field($model, 'right_answer')->hiddenInput(['value'=> $src_model->right_answer])->label(false); ?>
                <div class="form-group">
                    <?= Html::submitButton('Submit Answer', ['class' => 'btn btn-primary', 'name' => 'answer-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</div>
