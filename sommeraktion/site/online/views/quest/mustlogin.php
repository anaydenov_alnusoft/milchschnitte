<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'You must be logged in to see this page';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="quest-mustlogin">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
</div>
