<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

if ($cleared) {
	$this->title = 'Your answer of question for this day is cleared!';
} else {
	$this->title = 'You didn\'t answered question for this day yet!';
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="quest-clear">
    <h1><?= Html::encode($this->title) ?> (<span style="color:red"><?= $date_fmt ?></span>)</h1>
</div>
</div>
