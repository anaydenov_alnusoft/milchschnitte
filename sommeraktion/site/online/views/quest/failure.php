<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Your answer is wrong!';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="quest-failure">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>The right answer is: <b><?= Html::encode($right_answer) ?></b></p>
</div>
</div>
