<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'You already answered to this question!';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="quest-alreadyanswered">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
</div>
