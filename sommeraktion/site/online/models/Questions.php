<?php

namespace app\models;

/**
 * This is the model class for table "questions".
 *
 * @property integer $id
 * @property string $question
 * @property string $answer1
 * @property string $answer2
 * @property string $answer3
 * @property integer $right_answer
 * @property string $date
 *
 * @property Participation[] $participations
 * @property Prizes[] $prizes
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question', 'answer1', 'answer2', 'answer3', 'right_answer', 'date'], 'required'],
            [['right_answer'], 'integer'],
            [['date'], 'safe'],
            [['question', 'answer1', 'answer2', 'answer3'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Question',
            'answer1' => 'Answer1',
            'answer2' => 'Answer2',
            'answer3' => 'Answer3',
            'right_answer' => 'Right Answer',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipations()
    {
        return $this->hasMany(Participation::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrizes()
    {
        return $this->hasMany(Prizes::className(), ['question_id' => 'id']);
    }
}
