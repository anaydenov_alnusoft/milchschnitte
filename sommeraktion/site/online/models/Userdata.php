<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "userdata".
 *
 * @property integer $id
 * @property string $data
 * @property string $email_hash
 * @property string $password
 * @property string $joined
 *
 * @property Participation[] $participations
 */
class Userdata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userdata';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data', 'email_hash', 'password'], 'required'],
            [['data', 'email_hash'], 'string'],
            [['joined'], 'safe'],
            [['password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
            'email_hash' => 'Email Hash',
            'password' => 'Password',
            'joined' => 'Joined',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipations()
    {
        return $this->hasMany(Participation::className(), ['user_id' => 'id']);
    }
}
