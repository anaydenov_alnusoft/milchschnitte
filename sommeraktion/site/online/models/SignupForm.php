<?php
namespace online\models;

use yii\base\Model;
use online\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $email_hash;
    public $password;
    public $password_repeat;
    
    public $vorname;
    public $nachname;
    public $geburtsdatum;
    public $strasse;
    public $nr;
    public $plz;
    public $ort;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            ['vorname', 'required'],
            ['nachname', 'required'],
            ['geburtsdatum', 'required'],
            ['geburtsdatum', function ($attribute, $params) {
            
                $err = false;
                
                if ( date("Y-m-d", strtotime("now - 16 years")) < date("Y-m-d", strtotime($this->$attribute)))
                    $err = true;    

                if ($err)
                    $this->addError($attribute, 'Du musst 16 Jahre alt sein um dich zu registrieren.');                
                
            }, 'skipOnEmpty' => false, 'skipOnError' => false],
            ['strasse', 'required'],
            ['nr', 'required'],
            ['plz', 'required'],
            ['ort', 'required'],
            
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email_hash', 'unique', 'targetClass' => '\online\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
                    
            ['password_repeat', 'required'],
            ['password_repeat', 'string', 'min' => 6],        
            
            ['password', 'compare'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        $this->email_hash = md5($this->email);
        
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->email_hash = $this->email_hash;
        //$user->email = $this->email;
        $user->data = serialize($this->getAttributes());
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
