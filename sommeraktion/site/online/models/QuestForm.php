<?php
namespace online\models;

use Yii;
use yii\base\Model;
use online\models\User;
use app\models\Questions;
use app\models\Participation;

/**
 * Signup form
 */
class QuestForm extends Model
{
    public $user_id;
    public $question_id;
    public $answer;
    public $right_answer;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['user_id', 'question_id', 'answer'], 'required'],
        	[['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['question_id' => 'id']],
        	[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        	[['answer', 'right_answer'], 'integer', 'min' => 1, 'max' => 3],
        	[['user_id', 'question_id'], 'unique', 'targetClass' => Participation::className(), 'targetAttribute' => ['user_id', 'question_id']],
        ];
    }

    /**
     * Saves user answer into participation.
     *
     * @return boolean for a right or wrong answer
     */
    public function quest()
    {
        $this->user_id = Yii::$app->user->getId();
        
        if (!$this->validate()) {
            return null;
        }
        
        $part = new Participation();
        $part->user_id = $this->user_id;
        $part->question_id = $this->question_id;
        $part->answer = $this->answer;
        
        if ($part->save()) {
        	return ($this->answer == $this->right_answer);
        }
        return null;
    }
}
