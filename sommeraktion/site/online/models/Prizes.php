<?php

namespace online\models;

use Yii;

/**
 * This is the model class for table "prizes".
 *
 * @property integer $id
 * @property string $prize_name
 * @property string $prize_image
 * @property integer $question_id
 *
 * @property Questions $question
 */
class Prizes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prizes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prize_name', 'prize_image', 'question_id'], 'required'],
            [['question_id'], 'integer'],
            [['prize_name', 'prize_image'], 'string', 'max' => 255],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prize_name' => 'Prize Name',
            'prize_image' => 'Prize Image',
            'question_id' => 'Question ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Questions::className(), ['id' => 'question_id']);
    }
}
