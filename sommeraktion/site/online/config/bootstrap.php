<?php
Yii::setAlias('@common', dirname(dirname(__DIR__)) . '/online/common');
Yii::setAlias('@online', dirname(dirname(__DIR__)) . '/online');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
