$(function() {
    "use strict";
    
    var currentDay = 14;
    
    $('#calendar').find('i.day').each(function() {
        if( $(this).data('day')>currentDay ) {
            $(this).addClass('x-icon');
        }
        
        if( $(this).data('day')==currentDay ) {
            $(this).addClass('o-icon');
        }
    });
    
    var slide = function(e) {
        var self = this;
        var x =  e.pageX - ( $(this).parent().offset().left + ($(this).outerWidth()/2) );
        var position = $(self).position().left;
        position = position<=0 ? 0:position;
        
        if( position>=0 && position<$(self).parent().width() ) {
            $(self).css({"left": Math.abs(x)+"px"});
        }
        else {
            var left = position<5 ? 0:(position<$(self).parent().width()-10 ? '100%':position);
            $('.slider-button').css({left: position}).off('mousemove', slide);
        }
    };
    
    $('.slider-button').on('mousemove dragstart', function(e) {
        e.preventDefault();
        return false;
    });
    
    $('.slider-button').on('mousedown', function (e) {
        $(this).on('mousemove', slide);
        console.log(e.pageX);
    });
    
    $(document).on('mouseup dragend', function(e) {
        $('.slider-button').off('mousemove', slide);
    });
});