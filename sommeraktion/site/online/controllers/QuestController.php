<?php
namespace online\controllers;

use Yii;
use yii\web\Controller;
use online\models\QuestForm;
use app\models\Questions;
use app\models\Participation;

/**
 * Quest controller
 */
class QuestController extends Controller
{
	//const LOGIN_PAGE = '/sommeraktion/site/login';
   
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * User answers the question.
     *
     * @return mixed
     */
    public function actionIndex()
    {
    	if (Yii::$app->user->isGuest) {
    		return $this->render('mustlogin');
    	}
   		$today_date = $this->getTodayDate();
    	
    	$src_model = Questions::find()
    	->where(['date' => $today_date])
    	->one();
    	
    	$check_part = Participation::find()
    	->where(['question_id' => $src_model->id])
    	->andWhere(['user_id' => Yii::$app->user->getId()])
    	->one();
    	
    	if ($check_part) {
    		return $this->render('alreadyanswered');
    	}
    	
        $model = new QuestForm();
        // check that the form is posted
        if (Yii::$app->request->post()) {
        	$model->load(Yii::$app->request->post());
        	$result = $model->quest();
        	
        	if (is_bool($result)) {
        		if ($result) {
        			return $this->render('success');
        		}
        		return $this->render('failure', ['right_answer' => $src_model['answer'.$src_model->right_answer]]);
        	}
        }
        $date_fmt = \Yii::$app->formatter->asDate($today_date, "medium");
        return $this->render('form', [
            'model' => $model,
          	'src_model' => $src_model,
        	'date_fmt' => $date_fmt,
        ]);
    }
    
    function getTodayDate()
    {
    	//return date('Y-m-d');
    	// for tests only
    	return Yii::$app->request->get('date', '2016-06-24');
    }
    
    /**
     * Clear user's answers
     * 
     * THIS FUNCTION IS FOR TESTS ONLY
     * 
     * @return string
     */
    public function actionClear()
    {
    	if (Yii::$app->user->isGuest) {
    		return $this->render('mustlogin');
    	}
    	$today_date = $this->getTodayDate();
    	 
    	$src_model = Questions::find()
    	->where(['date' => $today_date])
    	->one();
    	 
    	$check_part = Participation::find()
    	->where(['question_id' => $src_model->id])
    	->andWhere(['user_id' => Yii::$app->user->getId()])
    	->one();
    	
    	$date_fmt = \Yii::$app->formatter->asDate($today_date, "medium");
    	$cleared = false;
    	if ($check_part) {
    		$check_part->delete();
    		$cleared = true;
    	}
    	return $this->render('clear', ['cleared' => $cleared, 'date_fmt' => $date_fmt,]);
    }
}
