﻿<!doctype html>
<!--[if lt IE 7]> <html lang="de" class="no-js ie6"> <![endif]-->
<!--[if IE 7]> <html lang="de" class="no-js ie7"> <![endif]-->
<!--[if IE 8]> <html lang="de" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="de" class="no-js">
 <!--<![endif]-->


	<meta name="WT.ti" content="Milch-Schnitte - Datenschutz">
	<meta name="WT.cg_n" content="Milch-Schnitte-Datenschutz">
	<meta name="DCS.dcsuri" content="milch-schnitte/datenschutz">

	<meta charset="UTF-8">
	<title>Datenschutz - Milch-Schnitte</title>
	<meta property="og:title" content="Milch-Schnitte">
	<meta property="og:type" content="website">
	<meta property="og:url" content="http://www.milchschnitte.de/">
	<meta property="og:image" content="http://www.milchschnitte.de/images/milch-schnitte_fbshare.jpg">
	<meta property="og:site_name" content="Milch-Schnitte">
	<meta property="og:description" content="Unkompliziert und frisch aus dem Kühlschrank ist Milch-Schnitte der leckere Snack für die kleine Pause im Alltag.">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		var base = location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/";
		document.getElementsByTagName("base")[0].href = +(location.hostname == '10.172.30.25') ? base + "milchschnitte/" : base;
	</script>
	<link id="favicon" rel="shortcut icon" href="/fileadmin/template/icons/favicon.ico">
	<!--[if gte IE 9]><link rel="stylesheet" type="text/css" href="/fileadmin/template/css/popup-font.css" media="screen"><![endif]-->
	<!--[if !IE]><!-->
	<link rel="stylesheet" type="text/css" href="/fileadmin/template/css/popup-font.css" media="screen"><!--><![endif]-->
	<link rel="stylesheet" type="text/css" href="/fileadmin/template/css/popup.css" media="screen">
	<script src="/fileadmin/template/js/modernizr.js" type="text/javascript"></script>

	<script>dataLayer = [];</script><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MJVLXS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MJVLXS');</script></head>

<body class="popup">
<header>
	    <p class="logo">Milch-Schnitte</p>
	    <h1>Datenschutz</h1>
	</header>
	<article>
<?php

/*
This snippet demonstrate the use of Ferrero Legal_Info Webservice in a frontend website
Imprint = http://legal-info.ferrero.de/de/get_imprint'
Data privacy statement = http://legal-info.ferrero.de/de/get_dps_instance

Please don't forget to replace the strings "www.yourdomain.de" in the example code below!!

*/
// get imprint information
$imprint = simplexml_load_file('http://legal-info.ferrero.de/de/get_imprint');
// get dataprivacy information including optional chapter about Google Analytics.
// open http://legal-info.ferrero.de/de/get_dps in a browser to see what chapters are optional and
// what are the IDs of these chapters to be used in the request below.
$dps = simplexml_load_file('http://legal-info.ferrero.de/de/get_dps_instance?googleanalytics=1');


/**
 * Replace placeholders.
 *
 * 1. Replace SITE_URL and SITE_URL_BRIEF
 *    by the values of current site's URL.
 * 2. Replace each occurence of
 *      [[<url>][<url-display>]]
 *    by
 *      <a href="<url>"><url-display</a>
 *
 * @param string $str the string to be modified
 * @return final string with replacements
 */
function replace_special_tokens($str) {
  // replace special tokens;
  $str = str_replace('SITE_URL_BRIEF', 'www.yourdomain.de', $str);
  $str = str_replace('SITE_URL', 'http://www.yourdoamin.de', $str);

  // convert links to html representation of links
  // e.g. [[http://www.ferrero.de][www.ferrero.de]]   =>  <a href="http://www.ferrero.de">www.ferrero.de</a>
  $link_pattern = '/\[\[(.+?)\]\[(.+?)\]\]/s';
  $replacement = '<a href="$1">$2</a>';
  return preg_replace($link_pattern, $replacement, $str);
}
?>
<div class="imprint">
<?php

	// adapt the following snippet to your own layout and style sheets

	echo
		'<p>'.$imprint->introduction.'</p>'.
		'<p><b>Anschrift</b><br />'.
		$imprint->name.'<br />'.
		$imprint->street.'<br />'.
		$imprint->location.'<br />'.
		$imprint->telefon.'<br />'.
		$imprint->fax.'<br />'.
		'<a href="mailto:'.$imprint->email.'">'.$imprint->email.'</a></p>'.
		'<p><b>'.$imprint->management['title'].'</b><br />'.
		$imprint->management.'</p>';
?>
</div>
<div class="output">
<?php
	echo '<h2>'.$dps['title'].'</h2>';
	foreach($dps->chapter as $chapter){
		echo '<h3>'.$chapter['title'].'</h3>';
		foreach($chapter->paragraph as $p){
			$p = replace_special_tokens($p);
			echo "<p>$p</p>";
		}
	}
?>
</div>
</article>
</body>
</html>
