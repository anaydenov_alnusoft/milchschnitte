﻿<?php
require_once("../../../system/includes.php");

$recipeName = basename(dirname(__FILE__));
$likes      = getRecipeLikes($recipeName);
?>
<!doctype html>

<!--[if lt IE 7]> <html lang="de" class="no-js ie6"> <![endif]-->
<!--[if IE 7]> <html lang="de" class="no-js ie7"> <![endif]-->
<!--[if IE 8]> <html lang="de" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="de" class="no-js"> <!--<![endif]-->

<head>



    <meta charset="UTF-8">
    <title>Lecker Backen mit Milch-Schnitte&reg;</title>
    <meta property="og:title" content="Lecker Backen mit Milch-Schnitte&reg;">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://www.milchschnitte.de/die-milch-schnitte/backen-mit-milch-schnitte/rezept-tiramisu/">
    <meta property="og:image" content="http://www.milchschnitte.de/images/rezept-tiramisu.jpg">
    <meta property="og:site_name" content="Milch-Schnitte">
    <meta property="og:description" content="Die leckere Nachspeise aus Italien neu interpretiert. Schnell, einfach und so lecker – Tiramisu mit Milch-Schnitte® . Ein Rezept für absoluten Genuss.">

    <meta name="description" content="Die leckere Nachspeise aus Italien neu interpretiert. Schnell, einfach und so lecker – Tiramisu mit Milch-Schnitte®. Ein Rezept für absoluten Genuss." />
    <meta name="keywords" content="Milch-Schnitte, Milchschnitte, Ferrero, Backen mit Milch-Schnitte, Rezeptideen mit Milch-Schnitte, Dessert, Tiramisu" />

    <meta name="WT.ti" content="Milch-Schnitte - Die Milchschnitte">
    <meta name="WT.cg_n" content="Milch-Schnitte-Die-Milchschnitte">
    <meta name="DCS.dcsuri" content="milch-schnitte/die-milchschnitte">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script type="text/javascript">
            var base = location.protocol + "//" + location.hostname +    (location.port && ":" + location.port) + "/";
            document.getElementsByTagName("base")[0].href =  + (location.hostname == '10.172.30.25')?base+"milchschnitte/":base;
    </script>
    <link id="favicon" rel="shortcut icon" href="/fileadmin/template/icons/favicon.ico">
    <link rel="apple-touch-icon" href="/fileadmin/template/icons/57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fileadmin/template/icons/72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fileadmin/template/icons/114.png">

    <link rel="stylesheet" type="text/css" href="/fileadmin/template/css/bootstrap.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/fileadmin/template/fancybox/source/jquery.fancybox.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/fileadmin/template/mediaelement-js/mediaelementplayer.css">
    <link rel="stylesheet" type="text/css" href="/fileadmin/template/css/layout.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/fileadmin/template/css/shariff.min.css">
    <link rel="stylesheet" type="text/css" href="/fileadmin/template/css/font-awesome.min.css">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/fileadmin/template/mediaelement-js/mediaelement-and-player.min.js"></script>
    <script src="/fileadmin/template/js/shariff.min.js" type="text/javascript"></script>
    <script src="/fileadmin/template/js/modernizr.js" type="text/javascript"></script>
    <script type="text/javascript" src="/fileadmin/template/js/picturefill.min.js"></script>

<script>dataLayer = [];</script><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MJVLXS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MJVLXS');</script></head>


<body class="backen backen--detail">

<div id="wrapper">

        <!-- start header -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="container container--logo">
                            <h1 class="logo"><a href="/" title="Milch-Schnitte">Milch-Schnitte</a></h1>
                            <img class="kinder_signatur" src="/fileadmin/template/media/kinder_signatur.png" title="Kinder">
                        </div>
                        <div id="nav">
                            <ul>
                                <li class="first product active">
                                    <a href="/die-milch-schnitte/">Milch-Schnitte</a>
                                    <ul class="sub-menu">
                                        <li><a href="/die-milch-schnitte/">Die Milch-Schnitte</a></li>
                                        <li class="active"><a href="/die-milch-schnitte/backen-mit-milch-schnitte">Backen mit Milch-Schnitte</a></li>
                                        <li><a href="/tv-spots/">TV-Spot</a></li>
                                        <li><a href="/die-milch-schnitte/meine-pause/galerie/">Meine Pause</a></li>
                                    </ul>
                                </li>
                                <li class="family-life">
                                    <a href="/mein-familienalltag/">Familienalltag</a>
                                    <ul class="sub-menu">
                                        <li class="tablet-only"><a href="/mein-familienalltag/">Übersicht</a></li>
                                        <li><a href="/mein-familienalltag/bastelspass/">Bastelspass</a></li>
                                        <li><a href="/mein-familienalltag/spielefinder/">Spielefinder</a></li>
                                        <li><a href="/mein-familienalltag/inselfinder/">Inselfinder</a></li>
                                        <li><a href="/mein-familienalltag/inseltipps/">Inseltipps</a></li>
                                    </ul>
                                </li>
                                <li class="study">
                                    <a href="/alltagsstudie/alltag-in-deutschland/">Alltagsstudie</a>
                                    <ul class="sub-menu">
                                        <li><a href="/alltagsstudie/alltag-in-deutschland/">Alltag in Deutschland</a></li>
                                        <li><a href="/alltagsstudie/alltagsstrategien/">Alltagsstrategien</a></li>
                                        <li><a href="/alltagsstudie/alltagsmanagertypen/">Alltagsmanagertypen</a></li>
                                        <li><a href="/alltagsstudie/alltagstipps/">Alltagstipps</a></li>
                                        <li><a href="/alltagsstudie/service/">Service</a></li>
                                    </ul>
                                </li>
                                <li class="last survey">
                                    <a href="/alltagstypentest/">Typentest</a>
                                    <ul class="sub-menu">
                                        <li class="tablet-only"><a href="/alltagstypentest/">Alltagstypentest</a></li>
                                        <!-- <li><a href="#">Welche Schnitte bist du?</a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- end header -->

    <article>
        <container class="container--main-teaser">
            <div class="box box--sub-menu"></div>
        </container>

        <div class="container">

    <div class="row">
        <div class="span12">
            <section class="link-list--header">
                <a href="/die-milch-schnitte/backen-mit-milch-schnitte/rezept-pfirsich_cup"><span class="arrow arrow--left arrow--blue"></span>vorheriges Rezept</a>
                <a class="link-list__icon left" href="/die-milch-schnitte/backen-mit-milch-schnitte/rezept-pfirsich_cup"><span class="arrow arrow--left arrow--big"></span></a>

                <a href="/die-milch-schnitte/backen-mit-milch-schnitte"><span class="arrow arrow--left arrow--blue"></span>Zurück zur Übersicht</a>

                <a class="link-list__icon right" href="/die-milch-schnitte/backen-mit-milch-schnitte/rezept-beeren_charlotte"><span class="arrow arrow--big"></span></a>
                <a href="/die-milch-schnitte/backen-mit-milch-schnitte/rezept-beeren_charlotte"><span class="arrow arrow--blue"></span>nächstes Rezept</a>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="span12">
            <section class="box product-teaser teaser-xlarge">
                    <div class="rezept-image-mobile-wrapper">
                        <picture>
                            <!--[if IE 9]><video style="display: none;"><![endif]-->
                            <source srcset="/fileadmin/media/die-milch-schnitte/backen/rezept-tiramisu-detail_768.jpg" media="(max-width: 767px)">
                            <source srcset="/fileadmin/media/die-milch-schnitte/backen/rezept-tiramisu-detail.jpg">

                            <!--[if IE 9]></video><![endif]-->
                            <img title="Lecker Backen mit Milch-Schnitte®" alt="Tiramisu mit Milch-Schnitte®" src="/fileadmin/media/die-milch-schnitte/backen/rezept-tiramisu-detail.jpg">
                        </picture>
                    </div>
                    <div class="product-teaser__content-wrapper left">
                        <h2>Tiramisu mit <span class="text-nowrap">Milch-Schnitte<sup>&reg;</sup></span></h2>

                        <p>Die leckere Nachspeise aus Italien neu interpretiert: Tiramisu mit <span class="text-nowrap">Milch-Schnitte<sup>&reg;</sup></span>. Ein Rezept mit fein geschichteter Milch-Schnitte und mit einem Spritzer  Orange sorgt für absoluten Genuss.</p>

                        <ul class="row-fluid-2 list-cooking-times">
                            <li class="span4"><span class="icon time"></span> 25 min</li>
                            <li class="span4"><span class="icon freeze"></span> 2 h</li>
                            <li class="span4 mobile-full-width">
                                <span class="mag_ich_anzahl">
                                    <i class="fa fa-heart"></i>
                                    <span class="mag_ich_anzahl_inner"><?php echo $likes ?></span>
                                </span>
                            </li>
                        </ul>
                    </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="span12">

            <div class="social-wrapper">
                <div class="cta-btn social-like">
                    <div class="text">
                        <i class="fa fa-heart"></i>
                        Dieses Rezept mag ich.
                    </div>
                </div>
                <div class="cta-btn social-share">
                    <div class="text">
                        <i class="fa fa-share-alt"></i>
                        Rezept teilen
                    </div>
                    <div class="shariff"></div>
                </div>
            </div>

            <div class="tabs filter">
                <ul>
                    <li class="active"><a href="#ingredients">Zutaten </a></li>
                    <li><a href="#instruction">Anleitung </a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="span12">
            <section class="recipe-detail filter--target filter--ingredients">
                <ul>
                    <li>18 Stück <span class="text-nowrap">Milch-Schnitte<sup>&reg;</sup></span></li>
                    <li>Saft 1 Orange</li>
                    <li>400 g Mascarpone</li>

                    <li>2 EL Mandelsirup</li>
                    <li>Mark einer Vanilleschote</li>
                    <li>1 Becher Sahne (= 200 ml)</li>

                    <li>2 Eigelbe</li>
                    <li>1 EL Zucker</li>
                    <li>2 EL Kakaopulver</li>
                </ul>
            </section>
        </div>

        <div class="span12 hide">
            <section class="recipe-detail filter--target filter--instruction">
                <ul>
                    <li>
                        <div class="enumeration">1</div>
                        <p>Einen hohen Backrahmen (ca. 18 x 18 cm) mit 9 Stück <span class="text-nowrap">Milch-Schnitte<sup>&reg;</sup></span> auslegen. Boden aus <span class="text-nowrap">Milch-Schnitte<sup>&reg;</sup></span> mit der Hälfte des Orangensafts beträufeln. Mascarpone mit Mandelsirup und Vanillemark glatt rühren.</p>
                    </li>
                    <li>
                        <div class="enumeration">2</div>
                        <p>Sahne steif schlagen. Eigelbe mit Zucker dickschaumig schlagen und Mascarponecreme nach und nach unterrühren. Sahne vorsichtig unterheben. Die Hälfte der Creme auf dem Boden verteilen, mit restlichen 9 Stücken <span class="text-nowrap">Milch-Schnitte<sup>&reg;</sup></span> belegen und mit restlichem Orangensaft beträufeln.</p>
                    </li>
                    <li>
                        <div class="enumeration">3</div>
                        <p>Restliche Mascarponecreme auf dem Tiramisu verstreichen. Tiramisu ca. 2 Stunden kalt stellen, in Stücke (ca. 6 x 4,5 cm) schneiden. Mit Kakaopulver bestäuben und nach Wunsch mit Orangenfilets garniert servieren.</p>
                    </li>
                </ul>
            </section>
        </div>

    </div>

    <div class="row">
        <div class="span12">
            <section class="link-list--footer">
                <a href="/die-milch-schnitte/backen-mit-milch-schnitte"><span class="arrow arrow--left arrow--blue"></span>Zurück zur Übersicht</a>
                <a href="/die-milch-schnitte/backen-mit-milch-schnitte/rezept-tiramisu/rezept-tiramisu.pdf" target="_blank" download="Rezepte-Tiramisu.pdf"><span class="download"></span>Rezept herunterladen</a>
            </section>
        </div>
    </div>

</div>  </article>

    <!-- push for sticky footer -->
    <div id="push"></div>
</div>

<!-- sticky footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="span12">
                <ul class="nav">
                    <li><a href="/impressum/" class="popup" target="_blank">Impressum</a></li>
                    <li><a href="/datenschutz/" class="popup" target="_blank">Datenschutz</a></li>
                    <li><a href="/faq/" class="popup" target="_blank">FAQ</a></li>
                    <li class="last"><a href="http://www.ferrero.de" target="_blank">Ferrero</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div id="ie-bgr">
    <img src="/fileadmin/template/media/bgr.png">
</div>

<script src="/fileadmin/template/js/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="/fileadmin/template/js/responsive-nav.min.js" type="text/javascript"></script>
<script src="/fileadmin/template/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="/fileadmin/template/js/shared.js" type="text/javascript"></script>

<?php echo getRecipeLikeJavaScript($recipeName); ?>

</body>
</html>
