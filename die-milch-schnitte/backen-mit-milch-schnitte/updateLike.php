<?php
require_once("../../system/includes.php");

$recipe = $_GET['recipe'];

$recipeMd5 = md5($recipe);

if (isset($_COOKIE['milch-schnitte-likes'][$recipeMd5])) {
    exit();
}

$likeData = $connection->dbSelect("*", "rezeptLikes", "name = '" . $connection->avoidInjection($recipe) . "'");

if ($likeData !== false) {
    $dbUpdate          = array();
    $dbUpdate["likes"] = $likeData["likes"] + 1;
    $connection->dbUpdate(
        "likes", "rezeptLikes", "id='" . $connection->avoidInjection((int)$likeData["id"]) . "'", $dbUpdate
    );

    setcookie('milch-schnitte-likes[' . $recipeMd5 . ']', 'true', mktime(0, 0, 0, 12, 31, 2030));

    echo $dbUpdate["likes"];
}