<!doctype html>

<!--[if lt IE 7]> <html lang="de" class="no-js ie6"> <![endif]-->
<!--[if IE 7]> <html lang="de" class="no-js ie7"> <![endif]-->
<!--[if IE 8]> <html lang="de" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="de" class="no-js"> <!--<![endif]-->

<head>

	<meta charset="UTF-8">
	<title>Meine Pause - Milch-Schnitte</title>
	<meta property="og:type" content="website">
    <meta property="og:title" content="Milch-Schnitte">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://www.milchschnitte.de/">
    <meta property="og:image" content="http://www.milchschnitte.de/images/milch-schnitte_fbshare.jpg">
    <meta property="og:site_name" content="Milch-Schnitte">
    <meta property="og:description" content="Unkompliziert und frisch aus dem Kühlschrank ist Milch-Schnitte der leckere Snack für die kleine Pause im Alltag.">


	<meta name="description" content="Unkompliziert und lecker: kühle Milchcreme zwischen zwei lockeren Schnitten. Milch-Schnitte, die kleine Pause im Alltag." />
	<meta name="keywords" content="Milch-Schnitte, Milchschnitte, Alltagstypen, Alltagstypentest, Milch-Schnitte-Zutaten, Milch-Schnitte-Wallpaper, Milch-Schnitte-Downloads, Ferrero" />

	<meta name="WT.ti" content="Milch-Schnitte - Die Milchschnitte">
	<meta name="WT.cg_n" content="Milch-Schnitte-Die-Milchschnitte">
	<meta name="DCS.dcsuri" content="milch-schnitte/die-milchschnitte">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link id="favicon" rel="shortcut icon" href="/fileadmin/template/icons/favicon.ico">
	<link rel="apple-touch-icon" href="/fileadmin/template/icons/57.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/fileadmin/template/icons/72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/fileadmin/template/icons/114.png">

	<link rel="stylesheet" type="text/css" href="/fileadmin/template/css/bootstrap.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/fileadmin/template/fancybox/source/jquery.fancybox.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/fileadmin/template/mediaelement-js/mediaelementplayer.css">
	<link rel="stylesheet" type="text/css" href="/fileadmin/template/css/layout.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/fileadmin/template/css/my-break-contest.css" media="screen">

	<link rel="stylesheet" type="text/css" href="/fileadmin/template/css/shariff.min.css">
	<script src="/fileadmin/template/js/shariff.min.js" type="text/javascript"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/fileadmin/template/mediaelement-js/mediaelement-and-player.min.js"></script>
	<script src="/fileadmin/template/js/modernizr.js" type="text/javascript"></script>

<script>dataLayer = [];</script><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MJVLXS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MJVLXS');</script></head>


<body class="break-contest">

	<div id="wrapper">

		<!-- start header -->
		<header>
			<div class="container">
				<div class="row">
					<div class="span12">
						<div class="container container--logo">
							<h1 class="logo"><a href="/" title="Milch-Schnitte">Milch-Schnitte</a></h1>
							<img class="kinder_signatur" src="/fileadmin/template/media/kinder_signatur.png" title="Kinder">
						</div>
						<div id="nav">
							<ul>
								<li class="first product active">
									<a href="/die-milch-schnitte">Milch-Schnitte</a>
									<ul class="sub-menu">
										<li><a href="/die-milch-schnitte">Die Milch-Schnitte</a></li>
										<li><a href="/die-milch-schnitte/backen-mit-milch-schnitte">Backen mit Milch-Schnitte</a></li>
										<li><a href="/tv-spots">TV-Spot</a></li>
										<li class="active"><a href="/die-milch-schnitte/meine-pause">Meine Pause</a></li>
									</ul>
								</li>
								<li class="family-life">
									<a href="/mein-familienalltag">Familienalltag</a>
									<ul class="sub-menu">
										<li class="tablet-only"><a href="/mein-familienalltag">Übersicht</a></li>
										<li><a href="/mein-familienalltag/bastelspass">Bastelspass</a></li>
										<li><a href="/mein-familienalltag/spielefinder">Spielefinder</a></li>
										<li><a href="/mein-familienalltag/inselfinder">Inselfinder</a></li>
										<li><a href="/mein-familienalltag/inseltipps">Inseltipps</a></li>
									</ul>
								</li>
								<li class="study">
									<a href="/alltagsstudie/alltag-in-deutschland">Alltagsstudie</a>
									<ul class="sub-menu">
										<li><a href="/alltagsstudie/alltag-in-deutschland">Alltag in Deutschland</a></li>
										<li><a href="/alltagsstudie/alltagsstrategien">Alltagsstrategien</a></li>
										<li><a href="/alltagsstudie/alltagsmanagertypen">Alltagsmanagertypen</a></li>
										<li><a href="/alltagsstudie/alltagstipps">Alltagstipps</a></li>
										<li><a href="/alltagsstudie/service">Service</a></li>
									</ul>
								</li>
								<li class="last survey">
									<a href="/alltagstypentest">Typentest</a>
									<ul class="sub-menu">
										<li class="tablet-only"><a href="/alltagstypentest">Alltagstypentest</a></li>
										<!-- <li><a href="#">Welche Schnitte bist du?</a></li> -->
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- end header -->

		<article>
			<div class="container">

				<div class="row">
					<div class="span12">
						<div class="contest-box">

							<!-- tab navigation -->
							<div class="tabs tabs--in-box tabs--narrow-select btn-shadow-wrapper">
								<ul>
									<li>
										<a href="/die-milch-schnitte/meine-pause">Gewinnspiel <span class="arrow arrow--down"></span></a>
									</li>
									<li class="active">
										<a href="/die-milch-schnitte/meine-pause/galerie">Galerie <span class="arrow arrow--down"></span></a>
									</li>
									<li>
										<a href="/die-milch-schnitte/meine-pause/preise/">Preise <span class="arrow arrow--down"></span></a>
									</li>
								</ul>
							</div>

							<!-- main content box -->
							<section class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-submission">
										<div class="row-fluid">
											<div class="span12">
												<div class="text-box text-box--centered text-box--key-color">
													<div class="contest-gallery-headline">
														<div class="contest-gallery-headline__inner">
															<h2>Galerie vom <?php echo str_replace("-", ".", $pageArray[0]); ?>.2016</h2>
															<?php
															$prevGallery = checkGallery($pageArray[0], 'prev');
															$nextGallery = checkGallery($pageArray[0], 'next');
															if (isset($prevGallery) && $prevGallery != -1) {
																echo "<a class=\"contest-gallery-headline__prev fa fa-chevron-left\" href=\"" . $galleryPath . $prevGallery . "\">
																		<span>Vorherige Galerie</span>
																	  </a>";
															}

															if (isset($nextGallery) && $nextGallery != -1) {
																echo "<a class=\"contest-gallery-headline__next fa fa-chevron-right\" href=\"" . $galleryPath . $nextGallery . "\">
																		<span>Nächste Galerie</span>
																	  </a>";
															}
															?>
														</div>
													</div>

													<ul class="btn-group btn-group--centered">
														<li>
															<a class="text-btn fa fa-chevron-left" href="/die-milch-schnitte/meine-pause/galerie">
																Zurück zur Übersicht
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="contest-gallery-listing" data-items-api="/api/?action=detailGallery&page=<?php echo $pageArray[0]; ?>">
										<ul>
											<!-- items from data-items-api get loaded into here -->
										</ul>
									</div>

								</div><!-- .content-box__inner -->
							</section>
						</div><!-- contest-box -->

					</div>
				</div>

			</div>
		</article>

		<!-- push for sticky footer -->
		<div id="push"></div>
	</div>

	<!-- sticky footer -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="span12">
					<ul class="nav">
						<li><a href="/impressum/" class="popup" target="_blank">Impressum</a></li>
						<li><a href="/datenschutz/" class="popup" target="_blank">Datenschutz</a></li>
						<li><a href="/faq/" class="popup" target="_blank">FAQ</a></li>
						<li class="last"><a href="http://www.ferrero.de" target="_blank">Ferrero</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

	<div id="ie-bgr">
		<img src="/fileadmin/template/media/bgr.png">
	</div>

	<script src="/fileadmin/template/js/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="/fileadmin/template/js/responsive-nav.min.js" type="text/javascript"></script>
	<script src="/fileadmin/template/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
	<script src="/fileadmin/template/js/shared.js" type="text/javascript"></script>

	<!-- myBreakContest -->
	<script src="/fileadmin/template/js/myBreakContest.js" type="text/javascript"></script>
	<script src="/fileadmin/template/js/modules/infiniteScroll.js" type="text/javascript"></script>
	<script src="/fileadmin/template/js/modules/narrowSelect.js" type="text/javascript"></script>
	<script>
		(function($){
			$(document).ready(function() {
				$('.tabs--narrow-select').narrowSelect();
				$('.contest-gallery-listing').infiniteScroll({
					template: function( item ){
						var categoryCSS = '';
						if (item.category == "2") {
							categoryCSS = "winner-label winner-label-lottery";
						}
						if (item.category == "1") {
							categoryCSS = "winner-label winner-label-jury";
						}
						var itemTemplate =
							'<li class="contest-gallery-item">' +
								'<a 	href="'+ item.href +'" data-fancybox-group="daily-gallery" class="contest-gallery-item__inner ' + categoryCSS + '">' +
									'<img class="" src="'+ item.imageSrc +'" alt="" />' +
								'</a>' +
							'</li>'
						;
						return itemTemplate
					},
					itemsPerShow: function(){
						if(window.innerWidth <= 980) {
							return 2
						} else {
							return 4;
						}
					},
					callbacks: {
						willAdd: function(elements){
							elements.addClass('contest-gallery-item--will-appear');
						},
						didAdd: function(elements){
							elements.each(function(i, _elementAdded){
								var elementAdded = $(_elementAdded);
								//delayed for animations to happen
								setTimeout(function(){
									elementAdded.removeClass('contest-gallery-item--will-appear');
								}, 100 + Math.max(Math.random() * 300, 50) )
							});
						}
					}
				});
			});
		}(jQuery))
	</script>
</body>
</html>
