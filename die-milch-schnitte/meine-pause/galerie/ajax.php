<?php
$pictureDataDecrypted = unserialize($picture_data["pictureData"]);

$cssClass = 'contest-composed-image__inner';

if ($picture_data["category"] == 2) {
	$cssClass = 'contest-composed-image__inner winner-label winner-label-lottery';
}
if ($picture_data["category"] == 1) {
	$cssClass = 'contest-composed-image__inner winner-label winner-label-jury';
}
?>
<!-- image ajax content -->
<section class="content-box">
	<div class="content-box__inner content-box__inner--vertical-padding">
		<div class="contest-detailview">
			<div class="row-fluid">
				<div class="span6">
					<div class="contest-detailview__image-wrapper">
						<div class="contest-composed-image">
							<div class="<?php echo $cssClass; ?>">
								<img class="" src="<?php echo $pictureDataDecrypted["size354"]; ?>" alt="" />
							</div>
						</div>
					</div>
				</div>

				<div class="span6">
					<div class="contest-headline contest-headline--smaller">
						<h2>Meine Pause mit <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span></h2>
						<img src="/fileadmin/media/die-milch-schnitte/contest/meine-pause-mit-milchschnitte.png" alt="">
					</div>

					<h2 class="contest-detailview__headline">
						<em><?php
							if (isset($pictureDataDecrypted["firstname"])) {
								echo $pictureDataDecrypted["firstname"];
								if (substr($pictureDataDecrypted["firstname"], -1) == "ß" || substr($pictureDataDecrypted["firstname"], -1) == "x" || substr($pictureDataDecrypted["firstname"], -1) == "s") {
									echo "'";
								} else {
									echo "s";
								}
							}
							?></em>
						Pausen-Moment
					</h2>
					<div class="pictureCaption hidden"><?php echo $pictureDataDecrypted["caption"]; ?></div>
					<div class="cta-btn cta-btn--tertiary social-share-btn">
						<div class="text">
							<i class="fa fa-share-alt"></i>
							Foto teilen
						</div>
						<div class="shariff" data-services='["facebook","twitter","googleplus","mail","whatsapp"]'></div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>