<?php

$itemIdArray = explode("-", $pageArray[1]);


$picture_data = $connection->dbSelect("*","MeinePause","id = '".$connection->avoidInjection($itemIdArray[1])."' && status='2'");
if (isset($picture_data["id"])) {
	//fake backend for detail view
	//TODO: replace with real code later


	if ($ajax) {

		//if requested with XHR, return only the single detail view snipped...
		include('ajax.php');

	} else {

		//... for normal requests (and social media crawlers) return daily gallery
		// with open graph tags of the detail view and render additional information
		// to open detail view immediately
		include('gallery.php');
		?>
		<script>
			(function () {
				//get/create cfg object
				var ms = window.ms || (window.ms = {});
				var MyBreakContestCfg = ms.MyBreakContestCfg || (ms.MyBreakContestCfg = {});

				//set href on initialOpenLightbox, to immediately open lightbox with detail view
				MyBreakContestCfg.initialOpenLightbox = {
					href: location.href, //TODO: BE -> replace with real url from detailview
					hrefAfterClose: '<?php echo $galleryPath . $pageArray[0]; ?>' //url from gallery
				}
			}());

			$(document).ready(function () {
				$('.tabs--narrow-select').narrowSelect();
			});
		</script>
		<?php
	}
}
?>
