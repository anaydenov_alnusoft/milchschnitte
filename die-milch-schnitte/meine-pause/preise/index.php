<!doctype html>

<!--[if lt IE 7]> <html lang="de" class="no-js ie6"> <![endif]-->
<!--[if IE 7]> <html lang="de" class="no-js ie7"> <![endif]-->
<!--[if IE 8]> <html lang="de" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="de" class="no-js"> <!--<![endif]-->

<head>

	<meta charset="UTF-8">
	<title>Meine Pause - Milch-Schnitte</title>
    <meta property="og:title" content="Milch-Schnitte">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://www.milchschnitte.de/">
    <meta property="og:image" content="http://www.milchschnitte.de/images/milch-schnitte_fbshare.jpg">
    <meta property="og:site_name" content="Milch-Schnitte">
    <meta property="og:description" content="Unkompliziert und frisch aus dem Kühlschrank ist Milch-Schnitte der leckere Snack für die kleine Pause im Alltag.">

	<meta name="description" content="Meine Pause mit Milch-Schnitte. Über 400 tolle Sachpreise gibt es für die Teilnehmer des Foto-Gewinnspiels über die Verlosung und die Jury." />
	<meta name="keywords" content="Milch-Schnitte, Milchschnitte, Alltagstypen, Alltagstypentest, Milch-Schnitte-Zutaten, Milch-Schnitte-Wallpaper, Milch-Schnitte-Downloads, Ferrero" />

	<meta name="WT.ti" content="Milch-Schnitte - Die Milchschnitte">
	<meta name="WT.cg_n" content="Milch-Schnitte-Die-Milchschnitte">
	<meta name="DCS.dcsuri" content="milch-schnitte/die-milchschnitte">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link id="favicon" rel="shortcut icon" href="/fileadmin/template/icons/favicon.ico">
	<link rel="apple-touch-icon" href="/fileadmin/template/icons/57.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/fileadmin/template/icons/72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/fileadmin/template/icons/114.png">

	<link rel="stylesheet" type="text/css" href="/fileadmin/template/css/bootstrap.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/fileadmin/template/fancybox/source/jquery.fancybox.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/fileadmin/template/mediaelement-js/mediaelementplayer.css">
	<link rel="stylesheet" type="text/css" href="/fileadmin/template/css/layout.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/fileadmin/template/css/my-break-contest.css" media="screen">

	<link rel="stylesheet" type="text/css" href="/fileadmin/template/css/shariff.min.css">
	<script src="/fileadmin/template/js/shariff.min.js" type="text/javascript"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/fileadmin/template/mediaelement-js/mediaelement-and-player.min.js"></script>
	<script src="/fileadmin/template/js/modernizr.js" type="text/javascript"></script>

<script>dataLayer = [];</script><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MJVLXS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MJVLXS');</script></head>


<body class="break-contest">

	<div id="wrapper">

		<!-- start header -->
		<header>
			<div class="container">
				<div class="row">
					<div class="span12">
						<div class="container container--logo">
							<h1 class="logo"><a href="/" title="Milch-Schnitte">Milch-Schnitte</a></h1>
							<img class="kinder_signatur" src="/fileadmin/template/media/kinder_signatur.png" title="Kinder">
						</div>
						<div id="nav">
							<ul>
								<li class="first product active">
									<a href="/die-milch-schnitte">Milch-Schnitte</a>
									<ul class="sub-menu">
										<li><a href="/die-milch-schnitte">Die Milch-Schnitte</a></li>
										<li><a href="/die-milch-schnitte/backen-mit-milch-schnitte">Backen mit Milch-Schnitte</a></li>
										<li><a href="/tv-spots">TV-Spot</a></li>
										<li class="active"><a href="/die-milch-schnitte/meine-pause">Meine Pause</a></li>
									</ul>
								</li>
								<li class="family-life">
									<a href="/mein-familienalltag">Familienalltag</a>
									<ul class="sub-menu">
										<li class="tablet-only"><a href="/mein-familienalltag">Übersicht</a></li>
										<li><a href="/mein-familienalltag/bastelspass">Bastelspass</a></li>
										<li><a href="/mein-familienalltag/spielefinder">Spielefinder</a></li>
										<li><a href="/mein-familienalltag/inselfinder">Inselfinder</a></li>
										<li><a href="/mein-familienalltag/inseltipps">Inseltipps</a></li>
									</ul>
								</li>
								<li class="study">
									<a href="/alltagsstudie/alltag-in-deutschland">Alltagsstudie</a>
									<ul class="sub-menu">
										<li><a href="/alltagsstudie/alltag-in-deutschland">Alltag in Deutschland</a></li>
										<li><a href="/alltagsstudie/alltagsstrategien">Alltagsstrategien</a></li>
										<li><a href="/alltagsstudie/alltagsmanagertypen">Alltagsmanagertypen</a></li>
										<li><a href="/alltagsstudie/alltagstipps">Alltagstipps</a></li>
										<li><a href="/alltagsstudie/service">Service</a></li>
									</ul>
								</li>
								<li class="last survey">
									<a href="/alltagstypentest">Typentest</a>
									<ul class="sub-menu">
										<li class="tablet-only"><a href="/alltagstypentest">Alltagstypentest</a></li>
										<!-- <li><a href="#">Welche Schnitte bist du?</a></li> -->
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- end header -->

		<article>
			<div class="container">

				<div class="row">
					<div class="span12">
						<div class="contest-box">
							<section class="content-box contest-stage">
								<div class="contest-stage__media">
								</div>
								<div class="content-box__inner">
									<div class="contest-stage__inner">
										<div class="text-box text-box--key-color text-box--centered">
											<div class="contest-headline">
												<h2>Meine Pause mit Milch-Schnitte</h2>
												<img src="/fileadmin/media/die-milch-schnitte/contest/meine-pause-mit-milchschnitte-vertical-400w.png" alt="">
											</div>
										</div>

										<ul class="btn-group text-right btn-group--full-width-mobile">
											<li class="util-add-padding-top btn-shadow-wrapper">
												<a class="cta-btn cta-btn--secondary" href="/die-milch-schnitte/meine-pause/galerie/">
													Zur Galerie
													<span class="fa fa-chevron-right"></span>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</section>
						</div>
						<div class="contest-box">

							<!-- tab navigation -->
							<div class="tabs tabs--in-box tabs--narrow-select btn-shadow-wrapper">
								<ul>
									<li>
										<a href="/die-milch-schnitte/meine-pause">Gewinnspiel <span class="arrow arrow--down"></span></a>
									</li>
									<li>
										<a href="/die-milch-schnitte/meine-pause/galerie">Galerie <span class="arrow arrow--down"></span></a>
									</li>
									<li class="active">
										<a href="/die-milch-schnitte/meine-pause/preise/">Preise <span class="arrow arrow--down"></span></a>
									</li>
								</ul>
							</div>

							<!-- main content box -->
							<section class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-submission">
										<div class="row-fluid">
											<div class="span12">
												<div class="text-box text-box--centered text-box--key-color">
													<h2>Über 400 tolle Preise</h2>
													<p>
														Alle Teilnehmer hatten die Chance auf einen der tollen Preise. Eine Jury prämierte darüber hinaus die schönsten, lustigsten und außergewöhnlichsten Pausen mit <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> mit den Plätzen 1 bis 32.
													</p>
												</div>
											</div>
										</div>
									</div>

									<div class="contest-prizes contest-prizes--desktop">
										<div class="prizes-map">
											<img src="/fileadmin/media/die-milch-schnitte/contest/prizes-desktop.png" alt="Über 400 tolle Preise">

											<ul class="contest-markers">
												<!-- Lottery Prizes -->
												<li>
													<a 	style="top: 65%; left: 82%;" href="#prize-view-1"
														class="prize-opener prize-marker prize-marker--big prize-marker--small-font" rel="prices-group">
														<i>25x</i>
													</a>
												</li>
												<li>
													<a 	style="top: 53%; left: 30%;" href="#prize-view-2"
														class="prize-opener prize-marker prize-marker--big" rel="prices-group">
														<i>50x</i>
													</a>
												</li>
												<li>
													<a 	style="top: 79%; left: 21%;" href="#prize-view-3"
														class="prize-opener prize-marker prize-marker--big" rel="prices-group">
														<i>30x</i>
													</a>
												</li>
												<li>
													<a 	style="top: 59%;left: 58%;" href="#prize-view-4"
														class="prize-opener prize-marker prize-marker--big" rel="prices-group">
														<i>50x</i>
													</a>
												</li>
												<li>
													<a 	style="top: 53%;left: 68%;" href="#prize-view-5"
														class="prize-opener prize-marker prize-marker--big" rel="prices-group">
														<i>50x</i>
													</a>
												</li>
												<li>
													<a 	style="top: 10%;left: 81%;" href="#prize-view-6"
														class="prize-opener prize-marker prize-marker--big" rel="prices-group">
														<i>50x</i>
													</a>
												</li>
												<li>
													<a 	style="top: 55%;left: 80%;" href="#prize-view-7"
														class="prize-opener prize-marker prize-marker--big" rel="prices-group">
														<i>60x</i>
													</a>
												</li>
												<li>
													<a 	style="top: 84%;left: 71%;" href="#prize-view-8"
														class="prize-opener prize-marker prize-marker--big" rel="prices-group">
														<i>60x</i>
													</a>
												</li>

												<!-- Jury Prices -->
												<li>
													<a 	style="top: 8%; left: 37%;" rel="prices-group" href="#jury-prize-1"
														class="prize-opener prize-marker prize-marker--red prize-marker--big">
														<i>1x</i>
													</a>
												</li>
												<li>
													<a 	style="top: 25%; left: 8%;" rel="prices-group" href="#jury-prize-2"
														class="prize-opener prize-marker prize-marker--red prize-marker--big">
														<i>6x</i>
													</a>
												</li>
												<li>
													<a 	style="top: 67%; left: 36%" rel="prices-group" href="#jury-prize-3"
														class="prize-opener prize-marker prize-marker--red prize-marker--big">
														<i>25x</i>
													</a>
												</li>
											</ul>

										</div>

										<h2>Preise und Verlosung</h2>

										<div class="row-fluid">
											<div class="span6">
												<div class="contest-prizes__text text-box text-box--centered text-box--key-color">
													<h3>
														<span class="prize-marker prize-marker--info"></span>
														Verlosungs-Preise
													</h3>
													<p>
														Die Verlosungs-Preise sind am Bild mit blauen Labels versehen. Bei der Verlosung hatte jeder Teilnehmer die Chance auf einen der tollen Verlosungs-Preise.
													</p>
												</div>
											</div>
											<div class="span6">
												<div class="contest-prizes__text text-box text-box--centered text-box--key-color">
													<h3>
														<span class="prize-marker prize-marker--red prize-marker--info"></span>
														Jury-Preise
													</h3>
													<p>
														Die Hauptgewinne sind mit roten Labels markiert. Sie wurden von der Milch-Schnitte® Jury an die schönsten, lustigsten, abgefahrensten, lässigsten, einfach die schnittigsten Pausen mit <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> vergeben.
													</p>
												</div>
											</div>
										</div>
									</div>
								</div><!-- .content-box__inner -->
							</section>
						</div><!-- contest-box -->

						<div class="contest-box contest-prizes--smartphone">
							<section class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-submission">
										<div class="text-box text-box--centered text-box--key-color">
											<h2>Preise und Verlosung</h2>
											<p>Die Hauptpreise werden von unserer <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> Jury an die tollsten Fotos vergeben. Mit etwas Glück hat aber jeder Teilnehmer die Chance auf einen Preis.</p>
										</div>
									</div>

									<div class="row-fluid">
										<div class="span6">
											<div class="contest-prizes__text text-box text-box--centered text-box--key-color">
												<h2>
													Verlosungs-Preise
												</h2>
												<div class="contest-image__container">
													<a class="prize-group-opener" href="#prize-view-1">
														<img src="/fileadmin/media/die-milch-schnitte/contest/prizes-lottery.png" alt="Preise der Verlosung">
													</a>
												</div>
												<p>
													Die Verlosungs-Preise sind am Bild mit blauen Labels versehen.
													<br>
													Bei der Verlosung hat jeder Teilnehmer die Chance auf einen der tollen Verlosungs-Preis.
												</p>
												<ul class="btn-group btn-group--min-width">
													<li class="btn-shadow-wrapper">
														<li>
															<a href="#prize-view-1" class="cta-btn cta-btn--secondary prize-group-opener">
																Preise ansehen
																<span class="fa fa-chevron-right"></span>
															</a>
														</li>
													</li>
												</ul>
											</div>
										</div>
										<div class="span6">
											<div class="contest-prizes__text text-box text-box--centered text-box--key-color">
												<h2>
													Jury-Preise
												</h2>
												<div class="contest-image__container">
													<a class="prize-group-opener" href="#jury-prize-1">
														<img src="/fileadmin/media/die-milch-schnitte/contest/prizes-jury.png" alt="Preise der Verlosung">
													</a>
												</div>
												<p>
													Die Hauptgewinne sind mit roten Labels markiert. Sie werden von der <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> Jury vergeben. An die schönsten, lustigsten, abgefahrensten, lässigsten, einfach die schnittigsten Pausen mit <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span>.
												</p>
												<ul class="btn-group btn-group--min-width">
													<li class="btn-shadow-wrapper">
														<li>
															<a href="#jury-prize-1" class="cta-btn prize-group-opener">
																Preise ansehen
																<span class="fa fa-chevron-right"></span>
															</a>
														</li>
													</li>
												</ul>
											</div>
										</div>
										<span class="span12">
											<div class="text-box text-box--key-color text-box--centered">
												<p>
													Die Verlosung der Preise und Vergabe der Hauptpreise durch unsere <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> Jury finden nach dem Aktionszeitraum statt.
												</p>
											</div>
										</span>
									</div>
								</div><!-- .content-box__inner -->
							</section>
						</div><!-- contest-box -->

						<div class="contest-box contest-box--lightbox-contents">

							<!-- lightbox contents -->
							<section id="jury-prize-1" class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-detailview">
										<img class="prize__image" src="/fileadmin/media/die-milch-schnitte/contest/prizes/Kuehlschrank.png" alt="">
										<div class="text-box text-box--key-color text-box--centered">
											<h2 class="prize-headline prize-red">
												<span class="prize-headline__amount">1</span>x Toller SMEG-Kühlschrank
											</h2>
											<p>
												Der kultige SMEG-Kühlschrank im 50er Jahre Stil sieht nicht nur toll aus, auch die inneren Werte überzeugen: Mit 229 Liter Fassungsvermögen im Kühlteil, 75 Liter im **** Gefrierteil sowie Energieeffizienzklasse A++ und NO FROST Technologie hält der Smeg nicht nur den <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span>  Vorrat effizient auf bester Verzehrtemperatur.
											</p>
										</div>
									</div>
								</div>
							</section>

							<section id="jury-prize-2" class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-detailview">
										<img class="prize__image" src="/fileadmin/media/die-milch-schnitte/contest/prizes/Kurztrip-Hamburg.png" alt="">
										<div class="text-box text-box--key-color text-box--centered">
											<h2 class="prize-headline prize-red">
												<span class="prize-headline__amount">6</span>x Kurztrip nach Hamburg für 4 Personen
											</h2>
											<p>
												Mit dem Reisegutschein im Wert von 2000 Euro geht es für dich und drei Personen deiner Wahl für drei Tage nach Hamburg. Zwei Hotel-Übernachtungen mit Frühstück und ein Musical-Besuch bei "Disneys König der Löwen" sind mit drin. Alle weiteren Leistungen können in Absprache mit DERTOUR im Rahmen des Gutscheinwertes ausgewählt werden.
											</p>
										</div>
									</div>
								</div>
							</section>

							<section id="jury-prize-3" class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-detailview">
										<img class="prize__image" src="/fileadmin/media/die-milch-schnitte/contest/prizes/Lautsprecher.png" alt="">
										<div class="text-box text-box--key-color text-box--centered">
											<h2 class="prize-headline prize-red">
												<span class="prize-headline__amount">25</span>x kabelloser Philips Lautsprecher
											</h2>
											<p>
												Dieser Lautsprecher ist genauso unkompliziert wie <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> . Der robuste Lautsprecher ist sogar wasserfest. Er verfügt über einen Akku und lässt sich kabellos via Blutooth verbinden. So ist er überall mit von der Partie, egal ob beim Sonnenbaden, im Pool oder beim Wandern. Mit der integrierten Anti-Übersteuerungs-Funktion bietet der Philips dabei in jeder Lage tollen Sound.
											</p>
										</div>
									</div>
								</div>
							</section>


							<section id="prize-view-1" class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-detailview">
										<img class="prize__image" src="/fileadmin/media/die-milch-schnitte/contest/prizes/Kopfhoerer.png" alt="">
										<div class="text-box text-box--key-color text-box--centered">
											<h2 class="prize-headline">
												<span class="prize-headline__amount">25</span>x Philips Kopfhörer
											</h2>
											<p>
												Der hochwertige Philips On-Ear Kopfhörer erlaubt ungestörten Musik- und <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> -Genuss. Der Kopfhörer überzeugt nicht nur mit seinem klaren und kraftvollen Bass. Die weichen, drehbaren Ohrmuscheln lassen sich einklappen, so ist der Philips auch auf Reisen ein perfekter Begleiter.
											</p>
										</div>
									</div>
								</div>
							</section>

							<section id="prize-view-2" class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-detailview">
										<img class="prize__image" src="/fileadmin/media/die-milch-schnitte/contest/prizes/Handyhuelle.png" alt="">
										<div class="text-box text-box--key-color text-box--centered">
											<h2 class="prize-headline">
												<span class="prize-headline__amount">50</span>x Personalisierte Smartphone-Hülle
											</h2>
											<p>
												Mit dem individuellen <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> Handy-Cover ist dein Handy nicht nur gut geschützt sondern sieht auch besonders schick aus. Die Hülle wird mit deinem Wunschnamen personalisiert, so dass draufsteht, was drin ist: z.B. "Vanessa-Schnitte" oder "Tobias-Schnitte". Die Smartphone-Hüllen gibt es für viele aktuelle Smartphone-Modelle, z.B. iPhone 6, iPhone 5, iPhone 4, Samsung Galaxy S6, Samsung Galaxy S4, Sony Xperia Z3, HTC One, LG G4, u.s.w.
											</p>
										</div>
									</div>
								</div>
							</section>

							<section id="prize-view-3" class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-detailview">
										<img class="prize__image" src="/fileadmin/media/die-milch-schnitte/contest/prizes/backset.png" alt="">
										<div class="text-box text-box--key-color text-box--centered">
											<h2 class="prize-headline">
												<span class="prize-headline__amount">30</span>x <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> Backset
											</h2>
											<p>
												Jetzt wird’s lecker. Mit dem Backset, bestehend aus der <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span>  Backschürze, dem Rezeptbuch mit 15 leckeren <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span>  Rezepten und zwei 10er Packungen <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> direkt zum Backen oder einfach für zwischendurch. Guten Appetit!
											</p>
										</div>
									</div>
								</div>
							</section>

							<section id="prize-view-4" class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-detailview">
										<img class="prize__image" src="/fileadmin/media/die-milch-schnitte/contest/prizes/Jute-1.png" alt="">
										<div class="text-box text-box--key-color text-box--centered">
											<h2 class="prize-headline">
												<span class="prize-headline__amount">50</span>x Jutebeutel mit Spruch
											</h2>
											<p>
												Nicht nur nützlich, sondern auch schick. Die praktische und robuste Tragetasche punktet mit einem tollen <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> Design. Mit diesem Jutebeutel fällst du garantiert auf.
											</p>
										</div>
									</div>
								</div>
							</section>

							<section id="prize-view-5" class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-detailview">
										<img class="prize__image" src="/fileadmin/media/die-milch-schnitte/contest/prizes/Jute-2.png" alt="">
										<div class="text-box text-box--key-color text-box--centered">
											<h2 class="prize-headline">
												<span class="prize-headline__amount">50</span>x Jutebeutel mit Spruch
											</h2>
											<p>
												Nicht nur nützlich, sondern auch schick. Die praktische und robuste Tragetasche punktet mit einem tollen <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> Design. Mit diesem Jutebeutel fällst du garantiert auf.
											</p>
										</div>
									</div>
								</div>
							</section>

							<section id="prize-view-6" class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-detailview">
										<img class="prize__image" src="/fileadmin/media/die-milch-schnitte/contest/prizes/fan-set.png" alt="">
										<div class="text-box text-box--key-color text-box--centered">
											<h2 class="prize-headline">
												<span class="prize-headline__amount">50</span>x <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span>  Fan-Set
											</h2>
											<p>
												Die perfekte Ausstattung für jeden <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span>  Fan: Ein kuschliges Kissen, das <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span>  Notizbuch samt passendem Kugelschreiber, eine Tasse, drei witzige Postkarten und ein hochwertiger Knirps Regenschirm.
											</p>
										</div>
									</div>
								</div>
							</section>

							<section id="prize-view-7" class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-detailview">
										<img class="prize__image" src="/fileadmin/media/die-milch-schnitte/contest/prizes/Kino-Gutschein.png" alt="">
										<div class="text-box text-box--key-color text-box--centered">
											<h2 class="prize-headline">
												<span class="prize-headline__amount">60</span>x Kinogutschein
											</h2>
											<p>
												Wo kann man besser Pause vom Alltag machen als im Kino? Lehn dich zurück und geniesse mit einem unserer Kinogutscheine einen Film deiner Wahl. Die Gutscheine lassen sich in allen Kinos von CinemaxX, Cineplex, Cinestar, UCI,  Kinopolis und in vielen inhabergeführten Kinos einlösen. Film ab.
											</p>
										</div>
									</div>
								</div>
							</section>

							<section id="prize-view-8" class="content-box">
								<div class="content-box__inner content-box__inner--vertical-padding">
									<div class="contest-detailview">
										<img class="prize__image" src="/fileadmin/media/die-milch-schnitte/contest/prizes/Handtuch.png" alt="">
										<div class="text-box text-box--key-color text-box--centered">
											<h2 class="prize-headline">
												<span class="prize-headline__amount">60</span>x <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span>  Badetuch
											</h2>
											<p>
												Egal ob im Schwimmbad, am Badesee oder am Strand. Das hochwertige <span class="text-nowrap">Milch-Schnitte<sup>®</sup></span> Badetuch von MÖVE ist eine super-gemütliche Unterlage zum Chillen.
											</p>
										</div>
									</div>
								</div>
							</section>

						</div>

					</div>
				</div>

			</div>
		</article>

		<!-- push for sticky footer -->
		<div id="push"></div>
	</div>

	<!-- sticky footer -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="span12">
					<ul class="nav">
						<li><a href="/impressum/" class="popup" target="_blank">Impressum</a></li>
						<li><a href="/datenschutz/" class="popup" target="_blank">Datenschutz</a></li>
						<li><a href="/faq/" class="popup" target="_blank">FAQ</a></li>
						<li class="last"><a href="http://www.ferrero.de" target="_blank">Ferrero</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

	<div id="ie-bgr">
		<img src="/fileadmin/template/media/bgr.png">
	</div>

	<script src="/fileadmin/template/js/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="/fileadmin/template/js/responsive-nav.min.js" type="text/javascript"></script>
	<script src="/fileadmin/template/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
	<script src="/fileadmin/template/js/shared.js" type="text/javascript"></script>

	<!-- myBreakContest -->
	<script src="/fileadmin/template/js/myBreakContest.js" type="text/javascript"></script>
	<script src="/fileadmin/template/js/modules/infiniteScroll.js" type="text/javascript"></script>
	<script src="/fileadmin/template/js/modules/narrowSelect.js" type="text/javascript"></script>
	<script>
		(function($){
			$(document).ready(function() {
				$('.tabs--narrow-select').narrowSelect();
			});
		}(jQuery))
	</script>
</body>
</html>
