<?php
session_start();

if (isset($_SERVER['DOCUMENT_ROOT']) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/../conf/config.php')) {
    $dbConnection = require $_SERVER['DOCUMENT_ROOT'] . '/../conf/config.php';
} elseif (file_exists(dirname(__FILE__) . '/../conf/config.php')) {
    $dbConnection = require dirname(__FILE__) . '/../conf/config.php';
}

// Include benötigter Classes
require_once("classes/DatabaseClass.php");

// Include Systemfile
require_once("config.inc.php");

require_once("global_functions.inc.php");