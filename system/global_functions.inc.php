<?php

function cropPicture($sourceFolder,
                     $destinationFolder,
                     $filename,
                     $width,
                     $height,
                     $quality,
                     $scale,
                     $scaleWidth,
                     $scaleHeight,
                     $angle,
                     $offsetX,
                     $offsetY) {

    global $photoPreviewSize;



    // Build complete image path
    $source = $sourceFolder . $filename;

    // Get dimensions of the original image
    list($currentWidth, $currentHeight) = @getimagesize($source);

    $resizeArray = @resizePictureDimension($source, $photoPreviewSize[0], $photoPreviewSize[1]);


    $extArray = @explode(".",$filename);
    $ext = end($extArray);

    switch($ext){
        case "jpg": {
            $currentImage = @imagecreatefromjpeg($source);
            break;
        }

        case "jpeg": {
            $currentImage = @imagecreatefromjpeg($source);
            break;
        }

        case "png": {
            $currentImage = @imagecreatefrompng($source);
            if($quality == 10) {
                $quality = 9;
            }
            break;
        }

        case "gif": {
            $currentImage = @imagecreatefromgif($source);
            break;
        }
    }

    $destination = $destinationFolder . $extArray[0] . ".png";

    // Resample the image
    $canvas = @imagecreatetruecolor($width, $height);

    $white = @imagecolorallocate($canvas,255,255,255);
    @imagefill($canvas, 0, 0, $white);

    $scaleUpWidth = round($currentWidth / $resizeArray[0], 2);
    $scaleUpHeight = round($currentHeight / $resizeArray[1], 2);

    $scale = 1 / $scale;

    $x1 = $offsetX * $scale * $scaleUpWidth;
    $y1 = $offsetY * $scale * $scaleUpHeight;

    $x2 = $scaleWidth * $scale * $scaleUpWidth;
    $y2 = $scaleHeight * $scale * $scaleUpHeight;

    // Rotate Picture
    if ($angle > 0) {
        $currentImage = imagerotate($currentImage, 360 - $angle, 0);
    }


    @imagecopyresampled($canvas, $currentImage, 0,0, $x1, $y1, $width, $height, $x2, $y2);

    /* rounded corner */
    $radius = 5;

    $ghost_color = imagecolorallocate($canvas, 239, 242, 246);

    imagearc($canvas, $radius-1, $radius-1, $radius*2, $radius*2, 180, 270, $ghost_color);
    imagefilltoborder($canvas, 0, 0, $ghost_color, $ghost_color);
    imagearc($canvas, $width-$radius, $radius-1, $radius*2, $radius*2, 270, 0, $ghost_color);
    imagefilltoborder($canvas, $width-1, 0, $ghost_color, $ghost_color);
    imagearc($canvas, $radius-1, $height-$radius, $radius*2, $radius*2, 90, 180, $ghost_color);
    imagefilltoborder($canvas, 0, $height-1, $ghost_color, $ghost_color);
    imagearc($canvas, $width-$radius, $height-$radius, $radius*2, $radius*2, 0, 90, $ghost_color);
    imagefilltoborder($canvas, $width-1, $height-1, $ghost_color, $ghost_color);

    imagecolortransparent($canvas, $ghost_color);

    if (!is_dir($destinationFolder)) {
        mkdir($destinationFolder);
    }

    @imagepng($canvas, $destination, $quality);
}


function addLabelsOnPicture($sourceFolder,
                     $destinationFolder,
                     $filename,
                     $width,
                     $height,
                     $quality,
                     $imageOffsetX = 0,
                     $logoWidth = 0,
                     $logoOffsetX = 0,
                     $logoOffsetY = 0,
                     $captionWidthOneLine = 0,
                     $captionWidthTwoLine = 0,
                     $captionOffsetX = 0,
                     $captionOffsetY = 0,
                     $containerWidth = 0,
                     $containerHeightOneLineCaption = 0,
                     $containerOneLineOffsetX = 0,
                     $containerOneLineOffsetY = 0,
                     $containerHeightTwoLineCaption = 0,
                     $containerTwoLineOffsetX = 0,
                     $containerTwoLineOffsetY = 0,
                     $containerFontSize = 0,
                     $containerFontAngle = 0,
                     $captionText = "") {

    global $absoluteUploadDirectory;

    // Build complete image path
    $source = $sourceFolder . $filename;
    $destination = $destinationFolder . $filename;

    $currentImage = @imagecreatefrompng($source);

    // Resample the image
    $canvas = @imagecreatetruecolor($width, $height);

    $white = @imagecolorallocate($canvas,255,255,255);
    @imagefill($canvas, 0, 0, $white);

    @imagecopyresampled($canvas, $currentImage, 0,0, 0, 0, $width, $height, $width, $height);


    if($quality == 10) {
        $quality = 9;
    }

    if ($logoWidth > 0) {
        $logoOverlayImage = $absoluteUploadDirectory . "upload-overlay-logo-big.png";
        $logoOverlay = imagecreatefrompng($logoOverlayImage);

        // Get dimensions of the original image
        list($currentLogoWidth, $currentLogoHeight) = @getimagesize($logoOverlayImage);

        $newLogoDimensions = @resizePictureDimension($logoOverlayImage, $logoWidth, $currentLogoHeight);

        $logoOverlayCanvas = @imagecreatetruecolor($newLogoDimensions[0], $newLogoDimensions[1]);
        imagealphablending($logoOverlayCanvas, false);
        imagesavealpha($logoOverlayCanvas, true);
        @imagecopyresampled($logoOverlayCanvas, $logoOverlay, 0,0, 0, 0, $newLogoDimensions[0], $newLogoDimensions[1], $currentLogoWidth, $currentLogoHeight);

        // Copy and merge
        imagecopymerge_alpha($canvas, $logoOverlayCanvas, $logoOffsetX, $logoOffsetY, 0, 0, $newLogoDimensions[0], $newLogoDimensions[1], 100);
    }

    // Add caption on the image

    if ($captionWidthOneLine > 0) {

        if (strlen($captionText) <= 20) {
            $captionContainerCanvas = imagecreatetruecolor($containerWidth, $containerHeightOneLineCaption);
            $captionOverlayImage = $absoluteUploadDirectory . "image-caption-singleLine.png";

            // Get dimensions of the original image
            list($currentCaptionWidth, $currentCaptionHeight) = @getimagesize($captionOverlayImage);
            $newCaptionDimensions = @resizePictureDimension($captionOverlayImage, $captionWidthOneLine, $currentCaptionHeight);
        } else {
            $captionContainerCanvas = imagecreatetruecolor($containerWidth, $containerHeightTwoLineCaption);
            $captionOverlayImage = $absoluteUploadDirectory . "image-caption-doubleLine.png";

            // Get dimensions of the original image
            list($currentCaptionWidth, $currentCaptionHeight) = @getimagesize($captionOverlayImage);
            $newCaptionDimensions = @resizePictureDimension($captionOverlayImage, $captionWidthTwoLine, $currentCaptionHeight);
        }

        imagesavealpha($captionContainerCanvas, true);
        $color = imagecolorallocatealpha($captionContainerCanvas, 255, 255, 255, 127);
        imagefill($captionContainerCanvas, 0, 0, $color);


        @imagecopyresampled($captionContainerCanvas, $canvas, $imageOffsetX, 0, 0, 0, $width, $height, $width, $height);

        $canvas = $captionContainerCanvas;

        $captionOverlay = imagecreatefrompng($captionOverlayImage);

        $captionOverlayCanvas = @imagecreatetruecolor($newCaptionDimensions[0], $newCaptionDimensions[1]);
        imagealphablending($captionOverlayCanvas, false);
        imagesavealpha($captionOverlayCanvas, true);
        @imagecopyresampled($captionOverlayCanvas, $captionOverlay, 0,0, 0, 0, $newCaptionDimensions[0], $newCaptionDimensions[1], $currentCaptionWidth, $currentCaptionHeight);

        // Copy and merge
        imagecopymerge_alpha($canvas, $captionOverlayCanvas, $captionOffsetX, $captionOffsetY, 0, 0, $newCaptionDimensions[0], $newCaptionDimensions[1], 100);

        if (strlen($captionText) <= 20) {
            imagettftextSp($canvas, $containerFontSize, $containerFontAngle, $containerOneLineOffsetX, $containerOneLineOffsetY, $white, $absoluteUploadDirectory . "HelveticaNeue-CondensedBold.ttf", convertTextToUnicode("\" " . $captionText . "\""),3);
        } else {

            $captionTextArray = explode(" ", $captionText);

            $firstCaptionLine = "";
            $secondCaptionLine = "";

            $currentCaptionElement = 0;

            while(true) {
                if (strlen($firstCaptionLine . $captionTextArray[$currentCaptionElement]) <= 20) {
                    $firstCaptionLine .= $captionTextArray[$currentCaptionElement] . " ";
                    $currentCaptionElement++;
                } else {
                    break;
                }

            }

            for ($i = $currentCaptionElement; $i < count($captionTextArray); $i++) {
                $secondCaptionLine .= " " . $captionTextArray[$i];
            }

            imagettftextSp($canvas, $containerFontSize, $containerFontAngle, $containerOneLineOffsetX, $containerOneLineOffsetY, $white, $absoluteUploadDirectory . "HelveticaNeue-CondensedBold.ttf", convertTextToUnicode("\" " . $firstCaptionLine), 3);
            imagettftextSp($canvas, $containerFontSize, $containerFontAngle, $containerTwoLineOffsetX, $containerTwoLineOffsetY, $white, $absoluteUploadDirectory . "HelveticaNeue-CondensedBold.ttf", convertTextToUnicode($secondCaptionLine . "\""), 3);
        }
    }

    if (!is_dir($destinationFolder)) {
        mkdir($destinationFolder);
    }

    @imagepng($canvas, $destination, $quality);
}

function convertTextToUnicode($text) {
    $text = mb_convert_encoding($text, 'HTML-ENTITIES',"UTF-8");

    // Convert HTML entities into ISO-8859-1
    $text = html_entity_decode($text,ENT_NOQUOTES, "ISO-8859-1");

    // Convert characters > 127 into their hexidecimal equivalents
    $out = "";
    for($i = 0; $i < strlen($text); $i++) {
        $letter = $text[$i];
        $num = ord($letter);
        if($num>127) {
            $out .= "&#$num;";
        } else {
            $out .=  $letter;
        }
    }
    return $text;
}

function imagettftextSp($image, $size, $angle, $x, $y, $color, $font, $text, $spacing = 0)
{
    if ($spacing == 0)
    {
        imagettftext($image, $size, $angle, $x, $y, $color, $font, $text);
    }
    else
    {
        $temp_x = $x;
        $temp_y = $y;
        for ($i = 0; $i < strlen($text); $i++)
        {
            imagettftext($image, $size, $angle, $temp_x, $temp_y, $color, $font, $text[$i]);
            $bbox = imagettfbbox($size, 0, $font, $text[$i]);
            $temp_x += cos(deg2rad($angle)) * ($spacing + ($bbox[2] - $bbox[0]));
            $temp_y -= sin(deg2rad($angle)) * ($spacing + ($bbox[2] - $bbox[0]));
        }
    }
}

function resizePicture($sourceFolder, $destinationFolder, $filename, $width, $height, $quality) {

    // Build complete image path
    $source = $sourceFolder . $filename;
    $destination = $destinationFolder . $filename;

    // Get dimensions of the original image
    list($currentWidth, $currentHeight) = @getimagesize($source);



    // Get the extension of the image
    $extArray = @explode(".", $source);
    $ext = end($extArray);

    // Load image function for the specific extension
    switch($ext){
        case "jpg": {
            $currentImage = @imagecreatefromjpeg($source);
            break;
        }

        case "jpeg": {
            $currentImage = @imagecreatefromjpeg($source);
            break;
        }

        case "png": {
            $currentImage = @imagecreatefrompng($source);
            if($quality == 10) {
                $quality = 9;
            }
            break;
        }

        case "gif": {
            $currentImage = @imagecreatefromgif($source);
            break;
        }
    }

    // Reduze image dimensions proportially
    $newImageDimensions = resizePictureDimension($source, $width, $height);

    // Create canvas for image resizing
    $canvas = @imagecreatetruecolor($newImageDimensions[0], $newImageDimensions[1]);

    if (!is_dir($destinationFolder)) {
        mkdir($destinationFolder);
    }

    // Resize the image and save it to the destination path
    @imagecopyresampled($canvas, $currentImage, 0, 0, 0, 0, $newImageDimensions[0], $newImageDimensions[1], $currentWidth, $currentHeight);
    switch($ext){       
        case "jpg": {
            @imagejpeg($canvas, $destination, $quality * 10);
            break;
        }
        
        case "jpeg": {
            @imagejpeg($canvas, $destination, $quality * 10);
            break;
        }
        
        case "png": {
            @imagepng($canvas, $destination, $quality);
            break;
        }
        
        case "gif": {
            @imagegif($canvas, $destination);
            break;
        }
    }
}

function resizePictureDimension($filename, $picMaxWidth, $picMaxHeight) {
    $size = @getimagesize($filename);
    $srcWidth = $size[0];
    $srcHeight = $size[1];
    
    if ($srcWidth > $picMaxWidth || $srcHeight > $picMaxHeight)
    {
        $proportion = $srcWidth / $srcHeight;
        $dstWidth = 0;
        $dstHeight = 0;
        
        if ($srcWidth > $picMaxWidth && $srcHeight > $picMaxHeight) {
            
            if ($srcWidth > $srcHeight) {
                $dstWidth = $picMaxWidth;
                $dstHeight = round($dstWidth * 1 / $proportion, 0);
                
                if ($dstHeight > $picMaxHeight) {
                    $dstHeight = $picMaxHeight;
                    $dstWidth = round(($dstHeight / $srcHeight) * $srcWidth);
                }
            } else {
                $dstHeight = $picMaxHeight;
                $dstWidth = $dstHeight * $proportion;
                if ($dstWidth > $picMaxWidth) {
                    $dstWidth = $picMaxWidth;
                    $dstHeight = round($dstWidth * 1 / $proportion,0);
                }
            }
        } elseif ($srcWidth > $picMaxWidth) {
            $dstWidth = $picMaxWidth;
            $dstHeight = round($dstWidth * 1 / $proportion, 0);
        } else {
            $dstHeight = $picMaxHeight;
            $dstWidth = $dstHeight * $proportion;
            $dstHeight = round($dstWidth * 1 / $proportion, 0);
        }
    }
    
    if (!$dstWidth) {
        $dstWidth = $srcWidth;
    }
    
    if (!$dstHeight) {
        $dstHeight = $srcHeight;
    }

    return array($dstWidth, $dstHeight);
}


function check_email($email)
{
    // Email Regular Expression
    $nonascii      = "\x80-\xff"; # Non-ASCII-Chars are not allowed

    $nqtext        = "[^\\\\$nonascii\015\012\"]";
    $qchar         = "\\\\[^$nonascii]";

    $protocol      = '(?:mailto:)';

    $normuser      = '[a-zA-Z0-9][a-zA-Z0-9_.-]*';
    $quotedstring  = "\"(?:$nqtext|$qchar)+\"";
    $user_part     = "(?:$normuser|$quotedstring)";

    $dom_mainpart  = '[a-zA-Z0-9][a-zA-Z0-9._-]*\\.';
    $dom_subpart   = '(?:[a-zA-Z0-9][a-zA-Z0-9._-]*\\.)*';
    $dom_tldpart   = '[a-zA-Z]{2,5}';
    $domain_part   = "$dom_subpart$dom_mainpart$dom_tldpart";

    $regex         = "$protocol?$user_part\@$domain_part";

    return preg_match("/^$regex$/",$email);
}

function linkURLs($str, $attributes=array()) {
    $attrs = '';
    foreach ($attributes as $attribute => $value) {
        $attrs .= " {$attribute}=\"{$value}\"";
    }
    $str = ' ' . $str;
    $str = preg_replace(
      '`([^"=\'>])(((http|https|ftp)://|www.)[^\s<]+[^\s<\.)])`i',
      '$1<a href="$2"'.$attrs.' target="_blank">$2</a>',
      $str
    );
    $str = substr($str, 1);
    $str = preg_replace('`href=\"www`','href="http://www',$str);
    // fügt http:// hinzu, wenn nicht vorhanden
    return $str;
}

function replaceUmlaute($string) {
    $string = str_replace("ä","ae",$string);
    $string = str_replace("ö","oe",$string);
    $string = str_replace("ü","ue",$string);
    $string = str_replace("Ä","Ae",$string);
    $string = str_replace("Ö","Oe",$string);
    $string = str_replace("Ü","Ue",$string);
    $string = str_replace("ß","ss",$string);
    return $string;
}
function restoreUmlaute($string) {
    $string = str_replace("ae","ä",$string);
    $string = str_replace("oe","ö",$string);
    $string = str_replace("ue","ü",$string);
    $string = str_replace("Ae","Ä",$string);
    $string = str_replace("Oe","Ö",$string);
    $string = str_replace("Ue","Ü",$string);
    $string = str_replace("ss","ß",$string);
    return $string;
}


/**
 * PNG ALPHA CHANNEL SUPPORT for imagecopymerge();
 * This is a function like imagecopymerge but it handle alpha channel well!!!
 **/

// A fix to get a function like imagecopymerge WITH ALPHA SUPPORT
// Main script by aiden dot mail at freemail dot hu
// Transformed to imagecopymerge_alpha() by rodrigo dot polo at gmail dot com
function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
    if(!isset($pct)){
        return false;
    }
    $pct /= 100;
    // Get image width and height
    $w = imagesx( $src_im );
    $h = imagesy( $src_im );
    // Turn alpha blending off
    imagealphablending( $src_im, false );
    // Find the most opaque pixel in the image (the one with the smallest alpha value)
    $minalpha = 127;
    for( $x = 0; $x < $w; $x++ )
        for( $y = 0; $y < $h; $y++ ){
            $alpha = ( imagecolorat( $src_im, $x, $y ) >> 24 ) & 0xFF;
            if( $alpha < $minalpha ){
                $minalpha = $alpha;
            }
        }
    //loop through image pixels and modify alpha for each
    for( $x = 0; $x < $w; $x++ ){
        for( $y = 0; $y < $h; $y++ ){
            //get current alpha value (represents the TANSPARENCY!)
            $colorxy = imagecolorat( $src_im, $x, $y );
            $alpha = ( $colorxy >> 24 ) & 0xFF;
            //calculate new alpha
            if( $minalpha !== 127 ){
                $alpha = 127 + 127 * $pct * ( $alpha - 127 ) / ( 127 - $minalpha );
            } else {
                $alpha += 127 * $pct;
            }
            //get the color index with new alpha
            $alphacolorxy = imagecolorallocatealpha( $src_im, ( $colorxy >> 16 ) & 0xFF, ( $colorxy >> 8 ) & 0xFF, $colorxy & 0xFF, $alpha );
            //set pixel with the new color + opacity
            if( !imagesetpixel( $src_im, $x, $y, $alphacolorxy ) ){
                return false;
            }
        }
    }
    // The image copy
    imagecopy($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h);
}

function checkGallery($currentGallery, $direction) {

    global $connection;

    $dateArray = array();
    $returnValue = -1;

    $result = $connection->dbRow("DISTINCT DATE_FORMAT(created, '%m-%d') as created","MeinePause","status = '2'","created desc");
    while ($row = fetch($result)) {
        $dateRecordArray = explode("-", $row["created"]);
        array_push($dateArray, $dateRecordArray[1] . "-" . $dateRecordArray[0]);
    }

    $currentGalleryIndex = array_search($currentGallery, $dateArray);

    if ($direction == "prev" && $currentGalleryIndex > 0) {
        $returnValue = $dateArray[$currentGalleryIndex - 1];
    }
    if ($direction == "next" && $currentGalleryIndex < count($dateArray)) {
        $returnValue = $dateArray[$currentGalleryIndex + 1];
    }

    return $returnValue;


}

/**
 * @param $recipeName
 *
 * @return int
 */
function getRecipeLikes($recipeName)
{
    global $connection;

    $likes = 0;

    $likeData = $connection->dbSelect("*", "rezeptLikes", "name = '".$connection->avoidInjection($recipeName)."'");
    if ($likeData) {
        $likes = $likeData['likes'];
    } else {
        $connection->dbInsert("name,likes", "rezeptLikes", array('name' => $connection->avoidInjection($recipeName), 'likes' => 0));
    }

    return $likes;
}

/**
 * @param $recipeName
 *
 * @return string
 */
function getRecipeLikeJavaScript($recipeName)
{
    return <<<RETURN
    <script>
        jQuery(document).ready(function(){
            $('.social-like:not(.disabled)').click(function(){
                jQuery.ajax({
                    method: 'get',
                    url: '/die-milch-schnitte/backen-mit-milch-schnitte/updateLike.php',
                    data: {
                        recipe: '{$recipeName}'
                    }
                }).
                done(function(msg) {
                    $('.mag_ich_anzahl_inner').text(msg);
                });
            });
        });
    </script>
RETURN;
}