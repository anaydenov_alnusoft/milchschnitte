<?php

$documentRoot = $_SERVER["DOCUMENT_ROOT"];
if (substr($documentRoot, -1) != "/") {
    $documentRoot .= "/";
}

$uploadDirectory = "/fileadmin/media/die-milch-schnitte/meine-pause/";              // Upload directory for user images
$absoluteUploadDirectory = $documentRoot . $uploadDirectory;

$urlPath = "/die-milch-schnitte/meine-pause/";
$contestPath = $urlPath . "mitmachen/";
$galleryPath = $urlPath . "galerie/";

$galleryYear = 2016;

// Default crop size for preview during upload process
$photoPreviewSize = array(800, 800);

$admin_mail = "XXXX@XXXXX.de";                                  // Mail Address Administrator

$errorMessages = array();
$errorMessages["processingError"] = "Dein Bild konnte leider nicht verarbeitet werden, bitte versuch es nochmal oder mit einem anderen.";


/////////////////////// DO NOT CHANGE ANYTHING BELOW THIS LINE //////////////////////////////////

// Connect to database
$connection = new DatabaseClass($dbConnection["db"]["host"], $dbConnection["db"]["username"], $dbConnection["db"]["password"], $dbConnection["db"]["db"]);


// Filter external variables
if (isset($_GET["id"])) if (is_numeric($_GET["id"])) $id = $_GET["id"];
if (isset($_GET["action"])) $action = trim(preg_replace("/[^0-9a-zA-ZöäüÖÄÜß _.@-]+/","",$_GET["action"]));
if (isset($_GET["game"])) $game = trim(preg_replace("/[^0-9a-zA-Z_-]+/","",$_GET["game"]));
if (isset($_GET["page"])) $page = trim(preg_replace("/[^0-9a-zA-Z_-]\\/+/","",$_GET["page"]));

if (isset($_GET["ajax"])) $ajax = trim(preg_replace("/[^0-9a-zA-Z_-]+/","",$_GET["ajax"]));

// Image Processing

if (isset($_POST["scale"])) if (is_numeric($_POST["scale"])) $scale = $_POST["scale"];
if (isset($_POST["width"])) if (is_numeric($_POST["width"])) $width = $_POST["width"];
if (isset($_POST["height"])) if (is_numeric($_POST["height"])) $height = $_POST["height"];
if (isset($_POST["angle"])) if (is_numeric($_POST["angle"])) $angle = $_POST["angle"];
if (isset($_POST["x"])) if (is_numeric($_POST["x"])) $x = $_POST["x"];
if (isset($_POST["y"])) if (is_numeric($_POST["y"])) $y = $_POST["y"];

if (isset($_POST["captiontext"])) $captionText = trim(preg_replace("/[^0-9a-zA-ZöäüÖÄÜß _.@-]+/","",$_POST["captiontext"]));