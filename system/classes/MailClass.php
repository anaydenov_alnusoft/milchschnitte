<?php

/**
 Mail Klasse zur Generierung von E-Mails
 Copyright 2012 Friedemann Ehrke
**/

require_once ("class.phpmailer.php");

class MailClass extends PHPMailer
{
	private $error_array = array();
    private $mailItem = array();
    private $mailType = 0;
    private $mailAddress = "";
    private $mailContentData = array();
    private $mailMemberID = -1;
    private $mailCC = "";
    private $mailBCC = "";
    private $mailSender = "";
    private $mailSenderName = "";
    private $mailSubject = "";
    private $mailBody = "";
    private $mailAltBody = "";
    
    private $connection = "";
    
	public function __construct($connection, $mailAddress, $mailContentData = NULL, $mailMemberID = NULL, $mailSubject = NULL, $mailBody = NULL, $mailAltBody = NULL, $mailCC = NULL, $mailBCC = NULL, $mailSender = NULL, $mailSenderName = NULL, $mailType = NULL)
	{
        global $main_url;
        $mailContentData["MAIN_URL"] = $main_url;
        
        $this->connection = $connection;
        $this->mailType = $mailType;
        $this->mailAddress = $mailAddress;
        $this->mailContentData = $mailContentData;
        $this->mailMemberID = $mailMemberID;
        $this->mailCC = $mailCC;
        $this->mailBCC = $mailBCC;
        $this->mailSender = $mailSender;
        $this->mailSenderName = $mailSenderName;
        $this->mailSubject = $mailSubject;
        $this->mailBody = $mailBody;
        $this->mailAltBody = $mailAltBody;
        $this->createMessage();
	}
    
		
	public function createMessage()
	{
        
        if ($this->mailSender == "") {
            $sender_data = $this->connection->dbSelect("*","system","id='1'");
            $this->mailSender = utf8_decode($sender_data["mailSender"]);
            $this->mailSenderName = utf8_decode($sender_data["mailSenderName"]);
        }
        
		$this->mailItem = new PHPMailer();
        $this->mailItem->Priority     = "3";
        $this->mailItem->From         = $this->mailSender;
        $this->mailItem->FromName     = $this->mailSenderName;
        $this->mailItem->Host         = "localhost";
        $this->mailItem->Mailer       = "mail";
        $this->mailItem->Sender       = $this->mailSender;
        $this->mailItem->Subject      = $this->mailSubject;
        
        if ($this->mailAddress != "") {
            if (strpos($this->mailAddress,",") > 0) {
                $AddressArray = explode(",",$this->mailAddress);
                foreach ($AddressArray AS $value) {
                    $this->mailItem->AddAddress($value);
                }
            } else $this->mailItem->AddAddress($this->mailAddress);
        }
        
        if ($this->mailCC != "") {
            if (strpos($this->mailCC,",") > 0) {
                $CCArray = explode(",",$this->mailCC);
                foreach ($CCArray AS $value) {
                    $this->mailItem->AddCC($value);
                }
            } else $this->mailItem->AddCC($this->mailCC);
        }
        
        if ($this->mailBCC != "") {
            if (strpos($this->mailBCC,",") > 0) {
                $BCCArray = explode(",",$this->mailBCC);
                foreach ($BCCArray AS $value) {
                    $this->mailItem->AddBCC($value);
                }
            } else $this->mailItem->AddBCC($this->mailBCC);
        }
        if ($this->mailType == 1) {
            $this->mailItem->AltBody = $this->mailAltBody;
            $this->mailItem->ContentType = "text/html";
            $this->mailItem->CharSet = "iso-8859-1";
            $this->mailItem->Body = $this->mailBody;
        } else {
            $this->mailItem->ContentType = "text/plain";
            $this->mailItem->CharSet = "iso-8859-1";
            $this->mailItem->Body = $this->mailBody;
        }
        
        foreach ($this->mailContentData AS $key => $value) {
            if ($this->mailItem->Subject != "") $this->mailItem->Subject = str_replace("[".$key."]",$value,$this->mailItem->Subject);
            if ($this->mailItem->Body != "") $this->mailItem->Body = str_replace("[".$key."]",$value,$this->mailItem->Body);
            if ($this->mailItem->AltBody != "") $this->mailItem->AltBody = str_replace("[".$key."]",$value,$this->mailItem->AltBody);
        }
        
        if (strpos($this->mailItem->Body, 0xc3) != false) { $this->mailItem->Body = utf8_decode($this->mailItem->Body); }
        if (strpos($this->mailItem->Body, 0xc3) != false) { $this->mailItem->Body = utf8_decode($this->mailItem->Body); }
        
        if (strpos($this->mailItem->AltBody, 0xc3) != false) { $this->mailItem->AltBody = utf8_decode($this->mailItem->AltBody); }
        if (strpos($this->mailItem->AltBody, 0xc3) != false) { $this->mailItem->AltBody = utf8_decode($this->mailItem->AltBody); }
        
        $this->mailItem->Send();
	}
}  
?>