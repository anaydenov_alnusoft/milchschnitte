<?php
function fetch($string)
{
 return mysql_fetch_array($string);
}

class DatabaseClass
{

    private $host;
    private $benutzer;
    private $pass;
    private $newLink;
    private $db;
    private $link;
    private $lastid;
    
    private $db_values;
    private $db_names;
    private $db_table;
    private $db_where;
    private $db_order;
    private $db_limit;
    
    private $db_post;
    
    private $stripTags;
    
    
	
	public function __construct($host, $benutzer, $pass, $db, $newLink = NULL)
	{
		$this->host = $host;
		$this->benutzer = $benutzer;
		$this->pass = $pass;
		$this->db = $db;
        $this->newLink = $newLink;
        $this->connectDB();
        
        $this->stripTags = true;
	}
	
	private function connectDB()
	{
        if (!$this->link) {
            $link = mysql_connect($this->host, $this->benutzer, $this->pass, $this->newLink);
            mysql_set_charset('utf8',$link);
            if (!$link)
                die("Keine Verbindung zu MySQL");
            mysql_select_db($this->db, $link)
                or die("Konnte Datenbank nicht öffnen");
            $this->link = $link;
        }
    }
    
    private function closeDB()
    {
        mysql_close($this->link);
        unset($this->link);
    }
    
    private function executeQuery($query)
	{
        $result = mysql_query($query);
        if(mysql_error()) {
            //echo mysql_error()."(SQL: ".$query.")";
            $dbInsert = array();
            $dbInsert["request"] = mysql_real_escape_string(serialize($_REQUEST));
            $dbInsert["queryString"] = mysql_real_escape_string($query);
            $dbInsert["serverData"] = mysql_real_escape_string(serialize($_SERVER));
            $dbInsert["errorText"] = mysql_real_escape_string(mysql_error());
            mysql_query("INSERT INTO system_databaseerrors (request, queryString, serverData, errorText) VALUES ('".$dbInsert["request"]."','".$dbInsert["queryString"]."','".$dbInsert["serverData"]."','".$dbInsert["errorText"]."')");
        } else $this->lastid = mysql_insert_id();
        
		return $result;
	}
	
    private function createValues($query_type)
    {        
        $postArray = array();
        foreach ($this->db_post AS $key => $value) {
            if ($key != "senden") $postArray[] = $key;
        }
        $db_values = "";
        $db_names = "";
        switch ($query_type) {
            case "insert":
                foreach($postArray AS $value) {
                    if (in_array($value,$this->db_values)) {
                        $db_names .= $value.",";
                        $db_values .= "'".$this->avoidInjection($this->db_post[$value])."',";
                    }
                }
                break;
            case "update":
                foreach($postArray AS $value) {
                    if (in_array($value,$this->db_values)) $db_values .= $value."='".$this->avoidInjection($this->db_post[$value])."',";
                }
                break;
        }
        $this->db_names = substr($db_names,0,strlen($db_names) - 1);
        $this->db_values = substr($db_values,0,strlen($db_values) - 1);
    }
    
    private function buildQuery($query_type)
    {
        if ($this->db_where != "") $db_where = " WHERE ".$this->db_where; else $db_where = "";
        if ($this->db_order != "") $db_order = " ORDER BY ".$this->db_order; else $db_order = "";
        if ($this->db_limit != "") $db_limit = " LIMIT ".$this->db_limit; else $db_limit = "";
        
        switch ($query_type) {
            case "select":
                $query = "SELECT ".$this->db_values." FROM ".$this->db_table.$db_where.$db_order.$db_limit;
                break;
            case "insert":
                if ($this->db_post != "") $this->createValues($query_type);
                $query = "INSERT INTO ".$this->db_table." (".$this->db_names.") VALUES(".$this->db_values.");";
                break;
            case "update":
                if ($this->db_post != "") $this->createValues($query_type);
                $query = "UPDATE ".$this->db_table." SET ".$this->db_values.$db_where.";";
                break;
            case "delete":
                $query = "DELETE FROM ".$this->db_table.$db_where.$db_order.";";
                break;
        }
        return $query;
    }
        
    private function setDBValues($db_values, $db_table, $db_where = NULL, $db_order = NULL, $db_limit = NULL)
    {
        
        $this->db_values = $db_values;
        $this->db_table = $db_table;
        $this->db_where = $db_where;
        $this->db_order = $db_order;
        $this->db_limit = $db_limit;
        
        unset($this->db_names);
        unset($this->db_post);
    }
    
    private function setDBPost($db_post)
    {
        $this->db_post = $db_post;
    }
	
	public function dbSelect($db_values, $db_table, $db_where = NULL, $db_order = NULL, $db_join = NULL)
	{
        if ($db_join != "") {
            $selectQuery = mysql_fetch_array($this->executeQuery($db_join));
        } else {
            $this->setDBValues($db_values, $db_table, $db_where, $db_order);
            $query = $this->buildQuery("select");
            $selectQuery = mysql_fetch_array($this->executeQuery($query));
        }
        if (is_array($selectQuery) && count($selectQuery) > 0) {
            foreach ($selectQuery AS $key => $value) {
                $selectQuery[$key] = stripslashes($value);
            }
        }
        return $selectQuery;
	}
	
	public function dbNum($db_values, $db_table, $db_where = NULL, $db_order = NULL, $db_join = NULL)
	{
        if ($db_join != "") {
            return mysql_num_rows($this->executeQuery($db_join));
        } else {
            $this->setDBValues($db_values, $db_table, $db_where, $db_order);
            $query = $this->buildQuery("select");
            return mysql_num_rows($this->executeQuery($query));
        }
        
	}
	
    public function dbRow($db_values, $db_table, $db_where = NULL, $db_order = NULL, $db_limit = NULL, $db_join = NULL)
	{
        if ($db_join != "") {
            $rowQuery = $this->executeQuery($db_join);
        } else {
            $this->setDBValues($db_values, $db_table, $db_where, $db_order, $db_limit);
            $query = $this->buildQuery("select");
            $rowQuery = $this->executeQuery($query);
        }
        if (is_array($rowQuery) && count($rowQuery) > 0) {
            foreach ($rowQuery AS $key => $value) {
                $rowQuery[$key] = stripslashes($value);
            }
        }
        return $rowQuery;
	}
	
	public function dbInsert($db_names, $db_table, $db_post = NULL, $stripTags = NULL)
	{
        if (isset($stripTags)) $this->stripTags = $stripTags;
        $db_names = explode(",",str_replace(" ","",$db_names));
        $this->setDBValues($db_names, $db_table);
        $this->setDBPost($db_post);
		$query = $this->buildQuery("insert");
        
        return $this->executeQuery($query);
        if ($stripTags) $this->stripTags = true;
	}
	
	public function dbUpdate($db_names, $db_table, $db_where = NULL, $db_post = NULL, $stripTags = NULL)
	{
        if (isset($stripTags)) $this->stripTags = $stripTags;
        $db_names = explode(",",str_replace(" ","",$db_names));
        $this->setDBValues($db_names, $db_table, $db_where);
        $this->setDBPost($db_post);
		$query = $this->buildQuery("update");
        return $this->executeQuery($query);
        if ($stripTags) $this->stripTags = true;
	}
	
	public function dbDelete($db_table, $db_where = NULL, $db_join = NULL)
	{
        $db_values = "";
        if ($db_join != "") {
            return $this->executeQuery($db_join);
        } else {
            $this->setDBValues($db_values, $db_table, $db_where);
            $query = $this->buildQuery("delete");
            return $this->executeQuery($query);
        }
	}
    
    public function getFields($db_table)
    {
        $fieldArray = array();
        $result = $this->dbRow("*",$db_table);
        while ($row = mysql_fetch_field($result)) {
            $fieldArray[$row->name] = "";
        }
        return $fieldArray;
    }
    
    public function getLastID() {
        return $this->lastid;
    }
    
    public function avoidInjection($field)
    {
        $field = mysql_real_escape_string($field, $this->link);
        
        $filterArray = array("&lt;" => "<","&gt;" => ">","&#60;" => "<","&#62;" => ">");
        foreach ($filterArray AS $key => $value) {
            $field = str_replace($key,$value,$field);
        }
        if ($this->stripTags) $field = strip_tags($field);
        return $field;
    }
}
?>