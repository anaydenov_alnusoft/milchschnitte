<?php
  
class LoginClass
{
    private $form_message;
    private $connection;
    
    private $username;
    private $password;
    private $login;
    private $memberName;
    private $firstLogin;
    
    
    public function __construct($sessionkey, $login)
	{
		$this->sessionkey = $sessionkey;
		$this->login = $login;
	}
    
    public function isLogin()
    {
        return $this->login;
    }
    
    public function isFirstLogin()
    {
        return $this->firstLogin;
    }
    
    public function getMessage()
    {
        return $this->form_message;
    }
    
    public function setLogin()
    {    
        $_SESSION["sessionkey"] = $this->sessionkey;
        $_SESSION["login"] = $this->login;
        $_SESSION["memberName"] = $this->memberName;
    }
    
    public function setConnection($connection)
    {
        $this->connection = $connection;
    }
    
    public function checkLogin($username, $password, $db_table, $errors)
    {
        if (count($_POST) > 0 && !$this->isLogin()) {
            if ($username == "" && $password == "")
            {
                $errors->insertMessage("Username");
                $errors->insertMessage("Passwort");
            } else {
                $data = $this->connection->dbSelect("*",$db_table,"username='".$this->connection->avoidInjection($username)."'");
                $isLoginError = false;
                if ($db_table == "member") {
                    if ($data["activateStatus"] == 0) {
                        $isLoginError = true;
                        $form_message = "<b>Ihr Account ist noch nicht aktiviert!</b><br>Bitte klicken Sie auf den Aktivierungslink in der Bestätigungsmail, um ihre E-Mail Adresse zu verifizieren.<br><br>";
                    }
                    if ($data["status"] == 0 && !$isLoginError) {
                        $isLoginError = true;
                        $form_message = "<b>Ihre Daten werden zur Zeit überprüft!</b><br>Nach Freischaltung durch den Administrator bekommen Sie eine Benachrichtigung per E-Mail.<br><br>";
                    }
                    if ($data["status"] == 2 && !$isLoginError) {
                        $isLoginError = true;
                        $form_message = "<b>Ihr Account ist gesperrt!</b><br>Bitte wenden Sie sich an den Administrator.<br><br>";
                    }
                }
                if ($db_table == "admin_member") {
                    if ($data["status"] == 0 && !$isLoginError) {
                        $isLoginError = true;
                        $form_message = "<b>Ihr Account ist gesperrt!</b><br>Bitte wenden Sie sich an den Administrator.<br><br>";
                    }
                }
                if ($data["username"] == $username && !$isLoginError) {
                    if ($data["password"] == $password) {
                        $this->connection->dbDelete("sessions","mid='".$data["id"]."'");
                        $this->sessionkey = sha1(time().rand(10000,100000).getenv("REMOTE_ADDR"));
                        $this->login = true;
                        $this->memberName = $data["firstname"] . " " . $data["lastname"];
                        $this->setLogin();
                        $dbInsert["mid"] = $data["id"];
                        $dbInsert["sessionkey"] = $this->sessionkey;
                        $dbInsert["date"] = time();
                        $dbInsert["type"] = $db_table;
                        $this->connection->dbInsert("mid,sessionkey,date,type","sessions",$dbInsert);
                        
                        $check_firstlogin = $this->connection->dbSelect("*","log","mid='".$this->connection->avoidInjection($data["id"])."' && action='login'");
                        if ($check_firstlogin["id"] == "") $this->firstLogin = true; else $this->firstLogin = false;
                        
                        $dbInsert["mid"] = $data["id"];
                        $dbInsert["recordId"] = "";
                        $dbInsert["recordTable"] = "";
                        $dbInsert["oldRecordData"] = "";
                        $dbInsert["ip"] = getenv("REMOTE_ADDR");
                        $dbInsert["headerData"] = $_SERVER["HTTP_USER_AGENT"];
                        $dbInsert["date"] = time();
                        $dbInsert["action"] = "login";
                        $this->connection->dbInsert("mid, recordId, recordTable, oldRecordData, ip, headerData, date, action","log",$dbInsert);
                        
                        //$form_message = "<b>Login erfolgreich!</b><br><br>";
                    } else {
                        $form_message = "<b>Das eingegebene Passwort ist falsch!</b><br><br>";
                    }
                } else {
                    if (!$isLoginError) $form_message = "<b>Das Passwort ist falsch oder der Username existiert nicht!</b><br><br>";
                }
            }
            $this->form_message = $form_message;
        } else {
            $session_data = $this->connection->dbSelect("*","sessions","sessionkey='".$this->connection->avoidInjection($this->sessionkey)."'");
            if ($session_data["id"] == "") $this->resetLogin();
            else {
                $dbUpdate["date"] = time();
                $this->connection->dbUpdate("date","sessions","id='".$this->connection->avoidInjection($session_data["id"])."'",$dbUpdate);
            }
        }
        return $this->isLogin();
    }
    
    public function Logout()
    {
        $this->connection->dbDelete("sessions","sessionkey='".$_SESSION["sessionkey"]."'");
        unset($_SESSION["sessionkey"]);
        unset($_SESSION["login"]);
        $this->login = false;
        $this->sessionkey = "";
        header("location: login.html?action=logout");
        exit;
    }
    
    public function resetLogin()
    {
        $this->login = false;
        unset($_SESSION["login"]);
        unset($_SESSION["sessionkey"]);
    }
}

?>