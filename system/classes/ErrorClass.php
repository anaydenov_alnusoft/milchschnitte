<?php

/**
 Error Klasse zur Ausgabe von Meldungen
 Copyright 2008 Friedemann Ehrke
**/
class ErrorClass
{
	private $error_array = array();
	private $check = false;

	public function insertMessage($kategorie)
	{
		$this->error_array[] = $kategorie;
		$this->check = true;
	}
	
	public function isError()
	{
		return $this->check;
	}
		
	public function displayMessages()
	{
		$error_content = "<ul>";
		for ($i = 0; $i < count($this->error_array); $i++) {
			$error_content .= "<li>".$this->error_array[$i]."</li>";
		}
		$error_content .= "</ul>";
		
		return $error_content;
	}
}  
?>