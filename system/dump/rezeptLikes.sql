-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 13. Apr 2016 um 15:24
-- Server Version: 5.6.24-72.2
-- PHP-Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Datenbank: `zend_ferrero_milchschnitte`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rezeptLikes`
--

DROP TABLE IF EXISTS `rezeptLikes`;
CREATE TABLE IF NOT EXISTS `rezeptLikes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `likes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
