$(function() {
    "use strict";
    
    var currentDay = 14;
    
    $('#calendar').find('i.day').each(function() {
        if( $(this).data('day')>currentDay ) {
            $(this).addClass('x-icon');
        }
        
        if( $(this).data('day')==currentDay ) {
            $(this).addClass('o-icon');
        }
    });
    
    var $window = $(window);
    var scrollPos = $window.scrollTop(); 
    
    function check_if_in_view(e) {
        var window_height = $window.height();
        var window_top_position = $window.scrollTop();
        var window_bottom_position = (window_top_position + window_height);
        
        var $element = $('.floatPosition');
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);

        //check to see if this current container is within viewport
        if (
            (element_bottom_position >= window_top_position) &&
            (element_top_position <= window_bottom_position)
        ) {
            // TODO
        } 
        else {
            // TODO
        }
        
        if( element_top_position < window_bottom_position ) {
            $element.addClass('in-view in-viewport');
        }
        
        if( element_top_position+($element.height()/2)  > window_bottom_position && 
            $element.hasClass('in-viewport') &&
            scrollPos > $window.scrollTop()
        ) {
            $element.removeClass('in-view');
        }
        
        if( $window.scrollTop() === 0 ) {
            $element.addClass('in-view').removeClass('in-viewport');
        }
        
        scrollPos = $window.scrollTop();
    }

    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');


    $('body').on('mousewheel', function(e) {
        if(e.originalEvent.wheelDelta > 0) {
            
        }
        else if(e.originalEvent.wheelDelta < 0) {

        }
    });
    
    var slide = function(e) {
        var self = this;
        var x =  e.pageX - ( $(this).parent().offset().left + ($(this).outerWidth()/2) );
        var position = $(self).position().left;
        position = position<=0 ? 0:position;
        
        if( position>=0 && position<$(self).parent().width() ) {
            $(self).css({"left": Math.abs(x)+"px"});
        }
        else {
            var left = position<5 ? 0:(position<$(self).parent().width()-10 ? '100%':position);
            $('.slider-button').css({left: position}).off('mousemove', slide);
        }
    };
    
    $('.slider-button').on('mousemove dragstart', function(e) {
        e.preventDefault();
        return false;
    });
    
    $('.slider-button').on('mousedown touchstart', function (e) {
        $(this).on('mousemove', slide);
    });
    
    $(document).on('mouseup dragend', function(e) {
        $('.slider-button').off('mousemove', slide);
    });
});